# UNICOM #

Unified communication layer.

original repository: https://y-dev@bitbucket.org/y-dev/unicom.git)

contact: plhak.marek@gmail.com

## Changes log ##
### 2018-04-09, v.1.13.0.###
- new
	- instructions repeat functionality
- fix
	- major processing fixes
- update

### 2020-04-05, v.1.12.17.###
- fix
	unitilised structures init

### 2018-01-19, v.1.12.16.###
- removed
	forgotten invalid thread tracing routine from unicom_event

### 2017-10-07, v.1.12.15.###
- fix
	[unicom_gsm] better processing of long lexems to avoid analyser not working properly

### 2017-10-07, v.1.12.14.###
- fix
	no waiting in case when instruction step continue is applied

### 2017-10-05, v.1.12.13.###
- add
	added some constants
		UNICOM_TIMEOUT_1_H          = UNICOM_TIMEOUT_10_M * 6,
		UNICOM_TIMEOUT_2_H          = UNICOM_TIMEOUT_1_H * 2,
		UNICOM_TIMEOUT_6_H          = UNICOM_TIMEOUT_1_H * 6,
		UNICOM_TIMEOUT_12_H         = UNICOM_TIMEOUT_1_H * 12,
		UNICOM_TIMEOUT_1_D          = UNICOM_TIMEOUT_1_H * 24,

### 2017-09-02, v.1.12.12.###
- fix
	instruction step check instruction present after processing instruction which should stopped thread processing!

### 2017-09-01, v.1.12.11.###
- fix
	response processing in case of processing not omitted and omitted ACK's 

- new
	new events to have an information about response delivery 
		UNICOM_EVENT_LEXEM_PROCESSING       = 25, //!<
		UNICOM_EVENT_LEXEM_PROCESSING_AGAIN = 26, //!<
		UNICOM_EVENT_LEXEM_PROCESSED        = 27, //!<

### 2017-09-01, v.1.12.10.###
- update

### 2017-07-22, v.1.12.9.###
- fix
	1] non interrupted threads response processing
	2] instruction step ERROR recovery from error state which makes thread IDLE before call ( to allow thread be reusable ), so recovery had not been possible

### 2017-06-22, v.1.12.8.###
- fix
	always batch steps together

### 2017-06-16, v.1.12.7.###
- change
	tick timer being restarted by response too ( had been only during process tick )

### 2017-06-09, v.1.12.6.###
- fix
	instruction finalization ( had been called twice when STEP_DONE initiated thread stop, which caused new STEP_DONE/_ERROR call )

### 2017-05-22, v.1.12.5.###
- update
	[unicom_gsm] analyser mdg usage update

### 2017-05-19, v.1.12.4.###
- change
	instruction change always lead to immediate processing of changed step
	to avoid inifite cycle,  _wait call must be called by now on side of instruction itself
- fix
	no event callback processing

### 2017-04-28, v.1.12.3.###
- fix
	[unicom] -thread execution is ready only when processor runs!


### 2017-04-07, unicom v.1.12.2.###
- update & change
	[unicom_event_mdg]
	better tracing - do not prepare trace string when not possible ( may leaded to high cpu usage )
	

### 2017-04-04, unicom v.1.12.1.###
- add
	unicom execution busy event instead of MDG ERROR 
		UNICOM_EVENT_THREAD_EXECUTE_BUSY    = 59, //!< executing already in process
	

### 2017-03-30, unicom v.1.12.0.###
- new
	function to stop whole processing from instruction processing ( not only thread! )
		void unicom_instruction_stop( UNICOM_EXECUTION_CONTEXT ps_context, bool b_error );
	

### 2017-03-30, unicom v.1.11.0.###
- new
	bool unicom_instruction_problem( UNICOM_EXECUTION_CONTEXT ps_context );
		instruction detection of processing ERROR | DONE by function

### 2017-03-10, unicom v.1.10.0.###
- fix
	thread stop must generate THREAD STOP and EXECUTION_DONE event also for PREPARING threads

- new
	possibility to switch CONNECT & SELECT by flag
	UNICOM_FLAG_CONNECT_AFTER_SELECTION    


### 2017-03-09, unicom v.1.9.7.###
- fix
	thread stop issues solved by finishing thread copy in time when thread may be changed by callbacks 

### 2017-03-06, unicom v.1.9.6.###
- fix
	non interruptable threads response only for them other's are temporary unaccessible for response!

### 2017-03-02, unicom v.1.9.5.###
- fix
	interruption issues

### 2017-02-28, unicom v.1.9.4.###
- fix
	many of interruption issues
- fix
	thread stopping with context switching
- fix
	response processing
- new
	events 
	UNICOM_EVENT_THREAD_SELECTION       = 57, //!< selecting next processing thread
	UNICOM_EVENT_THREAD_SELECTED        = 58, //!< selecting next processing thread

### 2017-02-21, unicom v.1.9.3.###
- fix
	execute_stop with failure/success indication is neccessary
- fix
	thread stop processing

### 2017-02-16, unicom v.1.9.2.###
- fix: batching & continue processing

### 2017-02-16, unicom v.1.9.1.###
- fix: steps change stops continue counter
	

### 2017-02-16, unicom v.1.9.0.###
- new
	- better and more clear steps processing
	- instruction step continue to fast processing of step based on time or count

### 2017-02-10, unicom v.1.8.2.###
[unicom]
- new: better steps batching
	- at now all higher steps are batched, or lower too when FORCED\_BATCH flag set
[unicom\_event\_mdg]
- change: changed INTERRUPT/RESUME mdg priority from INFO to DEBUG
- change: LEXEM is propagged as INFO instead DEBUG by now

### 2017-01-31, unicom v.1.8.1.###
- fix: better solution for request response key - automatized
	- when request created, response key attached automatically when request object passed as data
	- otherwise manual call of response\_key\_set needed

### 2017-01-27, unicom v.1.8.0.###
- new: instruction step prev

### 2017-01-16, unicom v.1.7.3.###
- fix: processing debug
- fix: major fix in thread stop call - forcing call EXECUTION event
- change: stop\_thread -> thread\_stop

### 2017-01-11, unicom v.1.7.2.###
- fix: accepting response lexems only on running threads with skipping delay of running thread
- fix: EVENT\_INSTRUCTION\_PROCESS performed immediatelly after step performing

### 2016-12-27, unicom v.1.7.1.###
- fix: thread repeat must restart delay timer
- fix: when instruction finishes, no interrupt timer will be active

### 2016-12-26, unicom v.1.7.0.###
- new: acking unification ( sending to processing or not by flag ACK\_PROCESSING\_OMITTED )
- new: STEPS BATCHING to call more steps in one round
- fix: debugged some failure behaviour

### 2016-12-06, unicom v.1.6.19.###
- fix: MAJOR fixes in interruptable threads

### 2016-11-07, unicom v.1.6.18.###
- fix: better presentation of PROCESS event during performing step - only steps being performed presents EVENT  by now!

### 2016-11-07, unicom v.1.6.17.###
- fix: threads switching
- new: THREAD\_REPEAT event

### 2016-11-07, unicom v.1.6.16.###
- fix: thread delay functionality

### 2016-10-26, unicom v.1.6.15.###
- change: compatibility for unicom\_execute 
	- added unicom\_execute\_2 for new functionality

### 2016-10-19, unicom v.1.6.14.###
- fix : corrected interrupt resume during response processing when thread finishes

### 2016-10-19, unicom v.1.6.13.###
- better processing of interruption
	- at now, instructions are not interruptable by self processing when changing STEP or STATE
	- in such case interruption is possible only externally by response or holding on same state
- fix : when response arrives, it is always delivered even if wait timer for instruction/operation has been set!

### 2016-10-18, unicom v.1.6.12.###
- debug : interruption / resume
- fix: do not interrupt between instructions ( edge of states \_DONE - \_INIT  )
- new : response event for every thread which was target for response

### 2016-10-18, unicom v.1.6.11.###
- debug : interruption / resume
- new: added back THREAD\_FLAG\_ATOMIC and default interruption at operation return when no ATOMIC FLAG given

### 2016-10-14, unicom v.1.6.10.###
- new features debug
- added unicom\_execute object and thread event to be handled and possibility to set response key during start event...
- fix : threads switching

### 2016-10-12, unicom v.1.6.9.###
- new features debug
- changing PARALLEL\_PROCESSING to INTERRUPTABLE flag

### 2016-10-11, unicom v.1.6.8.###
- code finalization - debug + optimization

### 2016-10-11, unicom v.1.6.7.###
- code finalization - debug + optimization

### 2016-10-11, unicom v.1.6.6.###
- for accessing response key from flow added response key to flow event
	- changed callback description name 
- new: possibility to attach response key directly to running context ( sometimes better than to thread )

### 2016-10-10, unicom v.1.6.5.###
- compilation process, not finished yet

### 2016-10-10, unicom v.1.6.4.###
- compilation process, not finished yet


### 2016-10-06, unicom v.1.6.3.###
- fix: syntax
- v.1.6.x. prepared for first compilation

### 2016-10-06, unicom v.1.6.2.###
- fix: context switching and initialization more transparent at now
- new: unicom\_instruction\_thread\_response\_key
	- passing thread response key into execution context

### 2016-10-05, unicom v.1.6.1.###
- fix: UNICOM\_EVENT\_THREAD\_PREPARE not usable
	- new: thread events INIT, DONE to achieve that

### 2016-10-05, unicom v.1.6.0.###
- rework: INTERRUPTABLE flag changes to PARALLEL\_PROCESSING
	- every response is passed to all PARALLEL\_PROCESSING threads if defined
	- every response is checked with thread response key
	- every thread can attach response key during THREAD\_PREPARE phase or during instruction processing
- not compiled yet

### 2016-10-03, unicom v.1.5.2.###
- fix: 1.5.x routines debug
- not compiled yet

### 2016-10-03, unicom v.1.5.1.###
- fix: interrupt /resume during response routine

### 2016-10-03, unicom v.1.5.0.###
- issue : we need to attach object for request which contains key which must be compared with response key for UNICOM\_LEXEM\_ACK responses - to select appropriate THREAD target
	- new : ACK with response and request object pairing possibility 
	- new : settings request\_response\_pair pairing function
- new: interrupt unification only one INTERRUPT and RESUME event
- new: implicitly everything is ATOMIC until INTERRUPTABLE flag is set
- not compiled

### 2016-10-03, unicom v.1.4.0.###
- new : FLAG\_INSTRUCTION\_INTERRUPTION for interrupting instruction during steps processing 
- not compiled

### 2016-09-30, unicom v.1.3.0.###
- new : FLAG\_OPERATION\_INTERRUPTION for interrupting operation between instructions 
- not compiled

### 2016-07-20, unicom v.1.2.3.###
- fix: printout macro fix

### 2016-07-20, unicom v.1.2.2.###
- fix: INSTRUCTION\_ERROR / \_TIMEOUT 
	- needed to restat instruction timeout and ack flag
- fix: unicom\_stop redundant call handling

### 2016-07-01, unicom v.1.2.1.###
- removed pointer debug information from event call

### 2016-07-01, unicom v.1.2.0.###
- new: operation\_stop to stop running operation in thread
- new: execute\_force\_start to start immediatelly when possible without initial delay

### 2016-06-03, unicom v.1.1.2.###
[unicom]
	- fix: STEP\_TIMEOUT recovery has to restart instruction timeout 
[unicom\_gsm]
	- fixed DATA lexem finalization 

### 2016-05-24, unicom v.1.1.1.###
[unicom]
	- fixed unicom instruction timer processing ( not covered \_restart functionality which has to reamin unchanged in not running state when TIMEOUT\_NONE )

### 2016-05-24, unicom v.1.1.0.###
[unicom]
	- added invoke instructions possibility to execute instructions from another instruction
	- added THREAD\_FLAG\_STANDALONE flag to not stop unicom when error occured
	- added THREAD\_FLAG\_ERROR\_REPEAT flag to repeat processing when error occurs ( usable for all-time-self-recoverable running threads )

[unicom\_gsm] 
	- added support to receive data of specified size from incomming stream

### 2016-05-23, unicom v.1.0.15.###
[unicom]
	- added instruction wait timer
	- added specific module events INIT and DONE to maintain some specific structure

[unicom\_gsm] 
	- fix: ACK processing was not correct ( need more testing ( sms and GSM functionality ) )
	- new: position change lexem
	- fix: ACK lexem has higher priority than normal analyse lexem!

### 2016-03-17, unicom v.1.0.14.###
	- new: instruction\_timeout\_stop

### 2016-03-16, unicom v.1.0.13.###
	- issue: using of instruction\_timeout\_restart caused starting of not initialised timer
		- fix: initialising to its value during instruction enter
	- fix: lexem\_equal call shared with lexem\_check
	[unicom\_event\_mdg]
	- change: implicitly all messages are reported as LEVEL\_INFO, with exception of lexems which are reported as LEVEL\_DEBUG
	

### 2016-01-28, unicom v.1.0.12.###
	- issue: after disconnection error, disconnection performed again
		- fix: new solution for recovery with possible return to running cycle ( RUN | CONNECT | DISCONNECT )
		- fix: call OPERATION\_ERROR instead of OPERATION\_DISCONNECT

### 2016-01-26, unicom v.1.0.11.###
	- issue: timings without tick time caused hanging of processes ( because no tick time additions )
		- fix: using of unitimer module for timing of processes

### 2015-12-01, unicom\_event\_mdg v.1.0.0.2. ###
	- change: changed default priority from INFO to DEBUG

### 2015-11-04, unicom v.1.0.10.###
	- issue: thread EVENT READY in which starts some thread which starts unicom\_run which can cycle in same state
		- fix: state switch before event

### 2015-11-04, unicom v.1.0.9. ###
	- issue: instruction timeout is running and fails instruction processing when set to TIMEOUT\_NONE 
		- fix: instruction\_enter & instruction\_timeout\_restart must counts with this

### 2015-11-03, unicom\_event\_mdg v.1.0.0.1. ###
	- new: versioned
	- change: changed order of identifier, which will be printed out immediately after event number

### 2015-10-19, unicom v.1.0.8. ###
	- new: string lexems access routines
		- added: lexem\_content\_( string | double | uint32 )
		- added: lexem\_equal\_string, lexem\_check\_string
	- new: instruction wait routine
	- new: unicom\_event\_mdg module for defined default output by mdg ( usable by unicom\_event\_init )
	- new: unicom identifier to differentiate more unicom instances
	- new: keep lexem after processing instruction ( UNICOM\_FLAG\_KEEP\_LEXEM )

### 2015-10-16, unicom\_gsm v.1.0.3. ###
	- issue: receive too long response leads to error but must be different in according to CMS/E errors
		- fix : differentiate of error state when too long lexem ( CMGL must delete when too long, too long echo during CMGS can't lead to error ) 

### 2015-10-09, unicom v.1.0.7. ###
	- issue: error processing for acked operations before ack arrive
		- new: error is processed everytime even if ack has not yet arrived

### 2015-10-08, unicom v.1.0.6. ###
	- issue: thread instruction execution initialization without event
		- fix: must be processed after operation selection

### 2015-10-08, unicom v.1.0.5. ###
	- new: added support for operations without instructions
	- change: better diagnostic output order

### 2015-10-02, unicom v.1.0.4. ###
	- new: added waiting at operation enter to offer some waiting before operation instructions will be performed

### 2015-10-02, unicom v.1.0.3.1. ###
	- issue: thread stop initiated by stop execution call caused finishing instruction and fall into THREAD\_INTERRUPT state in case of thread stop called by instruction step
		- fix: step processing checks processor state checking 
		- fix: in case of change, instruction processing is cancelled

### 2015-09-22, unicom v.1.0.3. ###
	- issue: code clean-up
		- fix: removed 
	- issue: safe return from event
		- fix: after return if unicom state has changed, automat must stop actual processing and continue with changed one ( to check correct behaviour in this case more tests needed ) ( actually not used in any application )


### 2015-09-21, unicom v.1.0.2.2. ###
	- issue: when execution done event stops unicom and restarts them, there is no expected reinitialisation
		- fix: correct solution is to call select routine, and so forth... as after first init
		       during processing here must be checked actual expected state when calling set state after called event 

### 2015-09-18, unicom\_gsm v.1.0.2. ###
	- removed gsm\_at include ( had been used because of GSMAT\_DEBUG )

### 2015-09-18, unicom v.1.0.2.1. ###
	- valgrind check
	- issue: uninitialised working lexem during start
		- fix: initialised in unicom\_init


### 2015-09-15, unicom v.1.0.2. ###
	- new: fast return from instruction init step ( had been not possible ) 
	- fix: changelog dates 

### 2015-08-21, unicom v.1.0.1., unicom\_gsm v.1.0.1. ###
	- release

### 2015-08-21, unicom v.1.0.0.13. ###
	- issue: operation return event only when finished a thread
		- fix: operation return event even when no next instruction present  

### 2015-08-20, unicom\_gsm v.1.0.0.3. ###
	- change: code refinement ( removed unused enum )

### 2015-08-07, unicom v.1.0.0.12. ###
	- new: added INSTRUCTION\_STEP\_REPEAT when timeout occures and repeat possible 
  - change: instruction processing FSM 

### 2015-08-07, unicom v.1.0.0.11. ###
	- issue: invalid thread number in THREAD... event 
		- fix: must be called before thread deactivation

### 2015-08-07, unicom\_gsm v.1.0.0.2. ###
	- issue: too long lexems  
		- fix: LEXEM\_ERROR generated

### 2015-08-06, unicom v.1.0.0.10. ###
	- issue: hanging analyse lexem when in idle state or instruction idle state 
		- fix: pop routine always
 

### 2015-08-06, unicom v.1.0.0.9. ###
	- issue: stop procedure was not started everytime needed 
		- fix: unified thread stop in unicom\_stop\_thread routine

### 2015-08-05, unicom v.1.0.0.8. ###
	- fix: stop procedure must for every active thread generate error event

### 2015-08-05, unicom v.1.0.0.7. ###
	- fix: diagnostic didn't include instruction number

### 2015-07-23, unicom\_gsm v.1.0.0.1. ###
	- change: LEXEM\_ACK processed separatelly from standard analyse ( all partial lexems are possible to catch - not only LEXEM\_ACK - good for flow )

### 2015-07-21, v.1.0.0.6. ###
	- new: event informing upper layer about instruction acknowledge requirement
	- change: some minor changes

### 2015-07-16, v.1.0.0.5. ###
	- fix: changed syntax to be more pedantic ( compilable by more pedantic compiler )

### 2015-07-16, v.1.0.0.4. ###
	- added reference to git repository and contact here

### 2015-07-16, v.1.0.0.3. ###
	- added reference to git repository to unicom.h

### 2015-07-15, v.1.0.0.2. (debug) ###
	- info: first release to debug functionality in real process 
	- fix: debugged unicom gsm analyser 
	- fix: debugged much of errors
	- fix: reached stabile and working state
	- new: diagnostic events with events data with more details about event origin

### 2015-06-19, v.1.0.0.1. (debug) ###
	- new: implementation of unicom done
	- new: implementation of unicom gsm analyser done
	- next: prepared for more tests
