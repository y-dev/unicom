#include <unicom_gsm.h>

#include <unicom_internal.h>

#include <mdg.h>
#include <ctype.h>
#include <string.h>

  
/*--------------------------------------------------------
                     UNICOM_GSM
----------------------------------------------------------*/
void unicom_gsm_lexem_clear( UNICOM_GSM ps_analyser )
{
	ps_analyser->ui32_lexem_size = 0;
	ps_analyser->b_lexem_found = false;
	unicom_timer_stop( &ps_analyser->s_long_line_timeout );
}

const static char unicom_gsm_text_special_characters[] = { '+','_','-','.' };

bool unicom_gsm_istextspecchar( char c )
{
	const char *pc = unicom_gsm_text_special_characters;
	const char *pc_end = pc + sizeof( unicom_gsm_text_special_characters );	
	while ( pc < pc_end )
	{
		if ( *pc == c )
			return true;
		pc++;
	}

	return false;
}

void unicom_gsm_lexem_finalize( UNICOM_GSM ps_analyser, uint32_t ui32_id )
{

	ps_analyser->auc_lexem_storage[ ps_analyser->ui32_lexem_size ] = '\0';

	// set output structure
	unicom_lexem_set( &ps_analyser->s_lexem, ui32_id, ps_analyser->auc_lexem_storage, ps_analyser->ui32_lexem_size, ps_analyser->ui32_lexem_line_index );

	// set lexem indicator
	ps_analyser->b_lexem_found = true;
}

bool unicom_gsm_lexem_found( UNICOM_GSM ps_analyser )
{
	return ps_analyser->b_lexem_found;
}

void unicom_gsm_lexem_set_data( UNICOM_GSM ps_analyser, const unsigned char *puc_data, uint32_t ui32_size ) 
{
  uint32_t ui32_copy = ui32_size;
  
  if ( ui32_copy > ps_analyser->ui32_lexem_storage_size - 1 )
    ui32_copy = ps_analyser->ui32_lexem_storage_size - 1;
  
  memcpy( ps_analyser->auc_lexem_storage, puc_data, ui32_copy );
}
  

bool unicom_gsm_lexem_putc( UNICOM_GSM ps_analyser, char c )
{
	if ( ps_analyser->ui32_lexem_size == ps_analyser->ui32_lexem_storage_size - 1 ) // -1 because of allocation for finalizing character '\0'
		UNICOM_ERROR_GOTO( UNICOM_ERROR_LEXEM_SPACE );
		
	ps_analyser->auc_lexem_storage[ ps_analyser->ui32_lexem_size ] = c;
	ps_analyser->ui32_lexem_size++;

	return true;

lbl_mdg_error:
	return false;
}


void unicom_gsm_state_set( UNICOM_GSM ps_analyser, UNICOM_GSM_STATE e_state )
{
	if ( ps_analyser->e_state == e_state )
		return;

	ps_analyser->e_state = e_state;

	switch ( e_state )
	{
	default:
		break;

	case UNICOM_GSM_TOO_LONG_WAIT_FOR_EOL:
		unicom_timer_start( &ps_analyser->s_long_line_timeout );
		break;
	}
}

void unicom_gsm_init( UNICOM_GSM ps_analyser, unsigned char *auc_storage, uint32_t ui32_storage_size )
{
	memset( ps_analyser, 0, sizeof( unicom_gsm_t ) );
	ps_analyser->auc_lexem_storage = auc_storage;
	ps_analyser->ui32_lexem_storage_size = ui32_storage_size;
  ps_analyser->b_echo_on = false; // restart must keep echo flag as is
  
	ps_analyser->b_data_mode = false;
  
	unicom_gsm_restart( ps_analyser );	
}

void unicom_gsm_lexem_index_restart( UNICOM_GSM ps_analyser )
{
	ps_analyser->ui32_lexem_line_index = 0;
}

void unicom_gsm_restart( UNICOM_GSM ps_analyser )
{
	ps_analyser->ui32_unget_count = 0;
	ps_analyser->ui32_echo_size = 0;
	ps_analyser->ui32_echo_check_index = 0;
	unicom_gsm_state_set( ps_analyser, UNICOM_GSM_INIT );
	unicom_timer_init( &ps_analyser->s_long_line_timeout, 100 );

	unicom_lexem_set( &ps_analyser->s_lexem, 0, NULL, 0, 0 );
	unicom_gsm_lexem_clear( ps_analyser );
	unicom_gsm_lexem_index_restart( ps_analyser );
}

bool unicom_gsm_unget( UNICOM_GSM ps_analyser, unsigned char c )
{
	if ( ps_analyser->ui32_unget_count == sizeof( ps_analyser->ac8_unget ) )
		UNICOM_ERROR_GOTO( UNICOM_ERROR_LEXEM_UNGET_SPACE );

	ps_analyser->ac8_unget[ ps_analyser->ui32_unget_count ] = c;
	ps_analyser->ui32_unget_count++;

	return true;

lbl_mdg_error:
	return false;
}

void unicom_gsm_lexem_index_next( UNICOM_GSM ps_analyser )
{
	ps_analyser->ui32_lexem_line_index++;
}


void unicom_gsm_lexem_remove_space_at_end( UNICOM_GSM ps_analyser )
{
	uint32_t ui32 = ps_analyser->ui32_lexem_size;

	// remove space at end
	while ( ui32 > 0 && isspace( ps_analyser->auc_lexem_storage[ ui32 - 1 ] ) )
		ui32--;

	ps_analyser->ui32_lexem_size = ui32;
}	


bool unicom_gsm_put_data( UNICOM_GSM ps_analyser, uint8_t **ppui8_data, const uint8_t *pui8_data_end )
{
	uint8_t *pui8_data = *ppui8_data;
	uint32_t ui32_size = (uint32_t)( pui8_data_end - pui8_data );

	uint32_t ui32_total_size = ps_analyser->ui32_data_size + ui32_size;

	// putting more data? put only expected count
	if ( ui32_total_size > ps_analyser->ui32_data_mode_size )
		ui32_size = ps_analyser->ui32_data_mode_size - ps_analyser->ui32_data_size;

	if ( ps_analyser->pui8_data != NULL )
		memcpy( ps_analyser->pui8_data + ps_analyser->ui32_data_size, pui8_data, ui32_size );

	*ppui8_data = pui8_data + ui32_size;

	// set taken bytes count
	ps_analyser->ui32_data_size += ui32_size;

	return ps_analyser->ui32_data_size < ps_analyser->ui32_data_mode_size;
}

UNICOM_LEXEM unicom_gsm_process( UNICOM_GSM ps_analyser, const unsigned char **ppuc_data, uint32_t *pui32_data_size )
{
	UNICOM_LEXEM ps_output_lexem = NULL;
	const unsigned char *puc_begin = *ppuc_data;
	const unsigned char *puc_data = puc_begin;
	const unsigned char *puc_end = puc_data + *pui32_data_size;

	// reset previously processed lexem
	if ( unicom_gsm_lexem_found( ps_analyser ) )
	{
		unicom_gsm_lexem_clear( ps_analyser );
		unicom_gsm_state_set( ps_analyser, UNICOM_GSM_INIT );
	}

	// produce ACK if required
	if ( ps_analyser->b_echo_produce_lexem )
	{
		ps_analyser->b_echo_produce_lexem = false;	
		// produce ACK lexem
		unicom_lexem_set( &ps_analyser->s_tmp_lexem, UNICOM_LEXEM_ACK, ps_analyser->auc16_echo_storage, ps_analyser->ui32_echo_size, 0 );
	
		ps_output_lexem = &ps_analyser->s_tmp_lexem;
		goto lbl_output_lexem;
	}
	

	while ( ( puc_data < puc_end || ps_analyser->ui32_unget_count > 0 ) && !unicom_gsm_lexem_found( ps_analyser ) )
	{
		bool b_putc = true; // implicitly putc OK
		unsigned char c;

		// unget processing
		if ( ps_analyser->ui32_unget_count > 0 )
		{
			ps_analyser->ui32_unget_count--;
			c = ps_analyser->ac8_unget[ ps_analyser->ui32_unget_count ];
		}
		else // if nothing to unget, process input data
		{
			c = *puc_data;
			puc_data++;
			
			// check echo if active
			if ( unicom_gsm_echo( ps_analyser ) && ps_analyser->b_echo_check_active ) 
			{      		    			
				if ( c == '\r' || c == '\n' ) 
				{  
					ps_analyser->ui32_echo_check_index = 0; // restart
					ps_analyser->b_echo_check_wait_for_next_line = false;

					// no more checking for recent request
					if ( ps_analyser->b_echo_ack_finalized )
					{
						ps_analyser->b_echo_produce_lexem = true;
						ps_analyser->b_echo_check_active = false;
					}
					goto lbl_continue_in_normal_analyse;
				}     

				if ( ps_analyser->b_echo_ack_finalized )
					goto lbl_continue_in_normal_analyse;


				if ( ps_analyser->b_echo_check_wait_for_next_line )
					goto lbl_continue_in_normal_analyse;
				
				//GSMAT_DEBUG( "analyse check %c<->%c (%i<-%i)", ps_analyser->auc16_echo_storage[ ps_analyser->ui32_echo_check_index ], c, ps_analyser->ui32_echo_size, ps_analyser->ui32_echo_check_index );
				
				if ( c != ps_analyser->auc16_echo_storage[ ps_analyser->ui32_echo_check_index ] ) 
				{
					ps_analyser->b_echo_check_wait_for_next_line = true;
					goto lbl_continue_in_normal_analyse;
				}

				ps_analyser->ui32_echo_check_index++;
				if ( ps_analyser->ui32_echo_check_index == ps_analyser->ui32_echo_size ) 
				{  
					ps_analyser->b_echo_check_wait_for_next_line = true;
					ps_analyser->b_echo_ack_finalized = true;
				}
			}	
		}

lbl_continue_in_normal_analyse:
	
		switch ( ps_analyser->e_state )
		{
			case UNICOM_GSM_INIT:
				// in DATA mode receive given amount of bytes, then turn back into NORMAL mode
				if ( unicom_gsm_data_mode( ps_analyser ) )
				{	
					puc_data --;
					if ( !unicom_gsm_put_data( ps_analyser, (uint8_t**)&puc_data, puc_end ) )
					{
						unicom_gsm_set_normal_mode( ps_analyser );
						unicom_lexem_set( &ps_analyser->s_tmp_lexem, UNICOM_GSM_LEXEM_DATA, ps_analyser->pui8_data == NULL ? (uint8_t*)"" : ps_analyser->pui8_data, ps_analyser->pui8_data == NULL ? 0 : ps_analyser->ui32_data_size, 0 );
						ps_output_lexem = &ps_analyser->s_tmp_lexem;
						goto lbl_output_lexem;
					}
				}
				else // NORMAL mode
				{
					switch ( c )
					{
						case '=':
						case ':':
						case ',':
							unicom_gsm_lexem_index_next( ps_analyser );
							unicom_gsm_lexem_finalize( ps_analyser, UNICOM_GSM_LEXEM_NEXT_POSITION );
							break;

						case '\n': // CR
						case '\r': // CR
							if ( c == '\r' )
								unicom_gsm_state_set( ps_analyser, UNICOM_GSM_CRLF );
							else 
								unicom_gsm_state_set( ps_analyser, UNICOM_GSM_LFCR );
							// produce fictive lexem
							unicom_lexem_set( &ps_analyser->s_tmp_lexem, UNICOM_GSM_LEXEM_NEW_LINE, NULL, 0, 0 );
							unicom_gsm_lexem_index_restart( ps_analyser );
							ps_output_lexem = &ps_analyser->s_tmp_lexem;
							goto lbl_output_lexem;

						case '"':
						case '\'':
							ps_analyser->c_string_quote = c;
							unicom_gsm_state_set( ps_analyser, UNICOM_GSM_STRING );
							break;

						default:
							if ( c == '+' || isalpha( c ) )
							{
								unicom_gsm_unget( ps_analyser, c );
								unicom_gsm_state_set( ps_analyser, UNICOM_GSM_TEXT );
							}
							else if ( isdigit( c ) )
							{
								unicom_gsm_unget( ps_analyser, c );
								unicom_gsm_state_set( ps_analyser, UNICOM_GSM_NUMBER );
							}
							// else others are ignored
							break;
					}
				}
				break;

			case UNICOM_GSM_STRING:
				if ( c == ps_analyser->c_string_quote )
				{
					unicom_gsm_lexem_finalize( ps_analyser, UNICOM_GSM_LEXEM_STRING );
					break;
				}

				b_putc = unicom_gsm_lexem_putc( ps_analyser, c );
				break;



			case UNICOM_GSM_NUMBER:
				// + [A-Za-z0-9] space is allowed, ending with :
				if ( !isdigit( c ) )
				{
					unicom_gsm_unget( ps_analyser, c );
					if ( isalpha( c ) || unicom_gsm_istextspecchar( c ) ) // some HEXA format begining with DIGITS?
					{
						unicom_gsm_state_set( ps_analyser, UNICOM_GSM_TEXT );
						break;
					}
					
					unicom_gsm_lexem_finalize( ps_analyser, UNICOM_GSM_LEXEM_NUMBER );
					break;
				}	
				
				b_putc = unicom_gsm_lexem_putc( ps_analyser, c );
				break;
			
	
			case UNICOM_GSM_TEXT:
				// + [A-Za-z0-9] space is allowed between allowed characters
				if ( ( !unicom_gsm_istextspecchar( c ) && !isalnum( c ) && !isspace( c ) ) || ( c == '\r' || c == '\n' ) )
				{
					unicom_gsm_lexem_remove_space_at_end( ps_analyser );
					
					unicom_gsm_unget( ps_analyser, c );
					unicom_gsm_lexem_finalize( ps_analyser, UNICOM_GSM_LEXEM_TEXT );
					break;
				}	
				
				b_putc = unicom_gsm_lexem_putc( ps_analyser, c );
				break;
	
			case UNICOM_GSM_LFCR:
				// CR separated ( not CRLF ) - forwarded to further processing
				if ( c != '\r' )
					unicom_gsm_unget( ps_analyser, c );
				
				unicom_gsm_state_set( ps_analyser, UNICOM_GSM_INIT );
				break; 

			case UNICOM_GSM_CRLF:

				// CR separated ( not CRLF ) - forwarded to further processing
				if ( c != '\n' )
					unicom_gsm_unget( ps_analyser, c );
				
				unicom_gsm_state_set( ps_analyser, UNICOM_GSM_INIT );
				break; // because of only '\r' or '\n'

			case UNICOM_GSM_TOO_LONG_WAIT_FOR_EOL:
				// waiting for next line
				if ( c == '\r' || c == '\n' ) 
				{
					unicom_gsm_unget( ps_analyser, c );
					unicom_gsm_lexem_finalize( ps_analyser, UNICOM_GSM_LEXEM_ERROR );
				}
				break;
		}

		if ( !b_putc ) 
		{
			MDG_TRACE( MDG_LEVEL_ERROR, "lexem too long" );
			unicom_gsm_state_set( ps_analyser, UNICOM_GSM_TOO_LONG_WAIT_FOR_EOL ); // skip whole line
		}
	}

	if ( !unicom_gsm_lexem_found( ps_analyser ) )
	{
		if ( unicom_timer_expired( &ps_analyser->s_long_line_timeout ) )
		{
			unicom_gsm_lexem_finalize( ps_analyser, UNICOM_GSM_LEXEM_ERROR );
		}
	}

	// return lexem
	if ( unicom_gsm_lexem_found( ps_analyser ) )
	{
		UNICOM_LEXEM ps_lexem = &ps_analyser->s_lexem;

		// detection of result codes on position 0
		if ( unicom_lexem_position( ps_lexem ) == 0 )
		{
			if ( unicom_gsm_lexem_check( ps_lexem, UNICOM_GSM_LEXEM_TEXT, "OK", 0, false ) )
				unicom_lexem_set_id_predefined( ps_lexem, UNICOM_LEXEM_OK );
			else if ( unicom_gsm_lexem_check( ps_lexem, UNICOM_GSM_LEXEM_TEXT, "+CME ERROR", 0, false ) 
			  || unicom_gsm_lexem_check( ps_lexem, UNICOM_GSM_LEXEM_TEXT, "+CMS ERROR", 0, false ) 
			  || unicom_gsm_lexem_check( ps_lexem, UNICOM_GSM_LEXEM_TEXT, "ERROR", 0, false ) )
				unicom_lexem_set_id_predefined( ps_lexem, UNICOM_LEXEM_ERROR );
		}
			
    ps_output_lexem = ps_lexem;
	}


lbl_output_lexem:
	// update caller's pointers
	if ( puc_data >= puc_end )
	{
		*ppuc_data = NULL;
		*pui32_data_size = 0;
	}
	else
	{
		*ppuc_data = puc_data;
		*pui32_data_size = puc_end - puc_data;
	}
	
	return ps_output_lexem;
}

void unicom_gsm_analyse( UNICOM_GSM ps_gsm, UNICOM ps, const unsigned char *puc_data, uint32_t ui32_data_size )
{
	UNICOM_LEXEM ps_lexem;

	while ( ui32_data_size > 0 )
	{
		ps_lexem = unicom_gsm_process( ps_gsm, &puc_data, &ui32_data_size );

		if ( ps_lexem != NULL )
			unicom_response( ps, ps_lexem );
		
	}
}

bool unicom_gsm_echo( UNICOM_GSM ps_gsm )
{
	return ps_gsm->b_echo_on;
}

void unicom_gsm_echo_on( UNICOM_GSM ps_gsm )
{
	ps_gsm->b_echo_on = true;
}

void unicom_gsm_echo_off( UNICOM_GSM ps_gsm )
{
	ps_gsm->b_echo_on = false;
}

void unicom_gsm_echo_required( UNICOM_GSM ps_gsm )
{
  // echoing must be on
	if ( !unicom_gsm_echo( ps_gsm ) ) 
		return;
    
	ps_gsm->ui32_echo_check_index = 0;
	ps_gsm->ui32_echo_size = 0;
	ps_gsm->b_echo_check_wait_for_next_line = false;
	ps_gsm->b_echo_check_active = true;
	ps_gsm->b_echo_ack_finalized = false;
	
}

void unicom_gsm_echo_add( UNICOM_GSM ps_gsm, char *pc )
{
	uint32_t ui32_size;
	char *pc_end, *pc_i;
	
	if ( !ps_gsm->b_echo_check_active )
	  return;
	
	ui32_size = strlen( pc );
	// put only N first characters
	if ( ui32_size > sizeof( ps_gsm->auc16_echo_storage ) - ps_gsm->ui32_echo_size )
		ui32_size = sizeof( ps_gsm->auc16_echo_storage ) - ps_gsm->ui32_echo_size;
	
	pc_i = pc;
	pc_end = pc_i + ui32_size;
	while ( pc_i < pc_end ) 
	{
	  char c = *pc_i;
	  if ( c == '\r' || c == '\n' ) 
	  {
	    ui32_size = pc_i - pc;
	    break;
	  }
	  pc_i++;
	}
	
	if ( ui32_size == 0 )
		return;
	
	memcpy( ps_gsm->auc16_echo_storage + ps_gsm->ui32_echo_size, pc, ui32_size );
	ps_gsm->ui32_echo_size += ui32_size;
}

bool unicom_gsm_lexem_check( UNICOM_LEXEM ps_lexem, UNICOM_GSM_LEXEM_ID e_id, const char *pc_data, uint32_t ui32_index, bool b_partial )
{
	return unicom_lexem_check( ps_lexem, (uint32_t) e_id, (const unsigned char*) pc_data, pc_data == NULL? 0 : strlen( pc_data ), ui32_index, b_partial );
}

bool unicom_gsm_lexem_equal( UNICOM_LEXEM ps_lexem, const char *pc_data, bool b_partial )
{
	char * pc_content = (char*)unicom_lexem_content( ps_lexem, NULL );

	if ( pc_content == NULL )
		return false;

	while ( *pc_data != '\0' && *pc_content != '\0')
	{
		if ( *pc_data != *pc_content )
			return false;

		pc_data++;
		pc_content++;
	}

	if ( *pc_content != '\0' ) // required same, but may be not complete
	{
		if ( *pc_data != '\0' || !b_partial )
			return false; // complete match required
	} else if ( *pc_data != '\0' ) // required more data
		return false;
	
	return true;
}

char* unicom_gsm_lexem_content( UNICOM_LEXEM ps_lexem )
{
	return (char*) unicom_lexem_content( ps_lexem, NULL );
}

bool unicom_gsm_data_mode( UNICOM_GSM ps_gsm )
{
	return ps_gsm->b_data_mode;
}

void unicom_gsm_set_data_mode( UNICOM_GSM ps_gsm, uint8_t *pui8_buffer, uint32_t ui32_size )
{
	ps_gsm->b_data_mode = true;
	ps_gsm->ui32_data_mode_size = ui32_size;
	ps_gsm->pui8_data = pui8_buffer;
	ps_gsm->ui32_data_size = 0;
}

void unicom_gsm_set_normal_mode( UNICOM_GSM ps_gsm )
{
	ps_gsm->b_data_mode = false;
}


