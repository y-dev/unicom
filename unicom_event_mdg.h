#ifndef __UNICOM_EVENT_MDG_H__
#define __UNICOM_EVENT_MDG_H__

#include <unicom.h>

unicom_event_f unicom_event_mdg();

void unicom_event_mdg_debug_event( void* p, UNICOM_EVENT e_event_id, UNICOM_EVENT_DATA ps_data );

#endif

