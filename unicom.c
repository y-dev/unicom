#define MDG_HEADER_STATIC "UNICOM"

#include "unicom.h"
#include "unicom_internal.h"
#include <stdlib.h>
#include <string.h>

#include <mdg.h>

#define UNICOM_INSTRUCTION_PROCESSING_EVENT_TIME_MS 1000



/*---------------------------------------------------------------------------------------------
				  UNICOM_THREAD
---------------------------------------------------------------------------------------------*/

bool gb_unicom_initialised = false;

const char *unicom_version()
{
	return UNICOM_VERSION;
}


void unicom_thread_state_set( UNICOM_THREAD ps_thread, UNICOM_THREAD_STATE e_state )
{
	if ( ps_thread->e_state == e_state )
		return;

	ps_thread->e_state = e_state;
}

bool unicom_thread_flag_get( UNICOM_THREAD ps_thread, UNICOM_THREAD_FLAG e_flag )
{
	if ( ps_thread->ui32_flags & (uint32_t) e_flag )
		return true;

	return false;
}

bool unicom_thread_tick( UNICOM_THREAD ps_thread )
{
	if ( ps_thread->f_thread_event == NULL )
		return true;

	return ps_thread->f_thread_event( ps_thread->p_app_object, UNICOM_THREAD_EVENT_TICK, ps_thread );
}

void unicom_thread_init( UNICOM ps, UNICOM_THREAD ps_thread )
{
	memset( ps_thread, 0, sizeof( unicom_thread_t ) );
	unicom_timer_init( &ps_thread->s_instruction_processing_event_timer, UNICOM_INSTRUCTION_PROCESSING_EVENT_TIME_MS );
}

bool unicom_execute_force_start( UNICOM ps, uint32_t ui32_thread_id )
{
	UNICOM_THREAD ps_thread;

	if ( unicom_state( ps ) == UNICOM_STATE_IDLE )
		return false;

	ps_thread = unicom_thread_get( ps, ui32_thread_id );
	if ( ps_thread == NULL )
		return false;

	if ( unicom_thread_state( ps_thread ) == UNICOM_THREAD_STATE_IDLE )
		return false;

	unicom_timer_stop( &ps_thread->s_delay_timer );
	return true;
}

bool unicom_thread_start( UNICOM_THREAD ps_thread, const uint32_t *pui32_operations_list, uint32_t ui32_operations_count,  uint32_t ui32_delay_ms, uint32_t ui32_repeat_count, unicom_execution_event_f f_event, void *p_app_object, uint32_t ui32_flags, unicom_thread_event_f f_thread_event )
{
	if ( ui32_operations_count == 0 )
		return false;

	ps_thread->pui32_operations_list = pui32_operations_list;
	ps_thread->ui32_operations_count = ui32_operations_count;

	ps_thread->p_app_object = p_app_object;
	ps_thread->ui32_repeat_count = ui32_repeat_count;
	ps_thread->ui32_repeat_counter = 0;
	ps_thread->f_event = f_event;
	ps_thread->f_thread_event = f_thread_event;

	ps_thread->ui32_flags = ui32_flags;


	unicom_thread_state_set( ps_thread, UNICOM_THREAD_STATE_PREPARING );

	unicom_timer_init( &ps_thread->s_delay_timer, ui32_delay_ms );

	unicom_thread_restart( ps_thread, false );
	return true;
}

void unicom_thread_operation_select( UNICOM_THREAD ps_thread, const uint32_t *pui32_operation )
{
	ps_thread->pui32_operation_selected = pui32_operation;

}

bool unicom_thread_operation_selected( UNICOM_THREAD ps_thread, uint32_t *pui32 )
{
	if ( ps_thread->pui32_operation_selected == NULL )
		return false;

	if ( pui32 != NULL )
		*pui32 = *ps_thread->pui32_operation_selected;

	return true;
}

void unicom_thread_repeat( UNICOM ps, UNICOM_THREAD ps_thread )
{
	ps_thread->ui32_repeat_counter++;

	unicom_thread_restart( ps_thread, false );

	unicom_event( ps, UNICOM_EVENT_THREAD_REPEAT );
}	

void unicom_thread_restart( UNICOM_THREAD ps_thread, bool b_no_delay )
{
	// init delay at init or at end for next init
	if ( unicom_timer_timeout( &ps_thread->s_delay_timer ) != UNICOM_DELAY_NONE )
	{
		if ( b_no_delay == false ) // else finish last timeout but do not start new one
			unicom_timer_restart( &ps_thread->s_delay_timer );
	}

	// select first operation
	unicom_thread_operation_select( ps_thread, &ps_thread->pui32_operations_list[ 0 ] );
}


void unicom_execution_event( UNICOM ps, UNICOM_THREAD ps_thread, UNICOM_EXECUTION_EVENT e_event )
{

	void *p_app_object;

	if ( ps_thread == NULL )
		return;

	if ( ps_thread != NULL && ps_thread->p_app_object != NULL )
		p_app_object = ps_thread->p_app_object;
	else
		p_app_object = ps->p_app_object;

	if ( ps_thread->f_event != NULL )
		ps_thread->f_event( p_app_object, e_event );
}


void unicom_state_set( UNICOM ps, UNICOM_STATE e_state )
{
	if ( e_state != ps->e_state )
		ps->e_state = e_state;
}

void unicom_activity_restore( UNICOM ps, UNICOM_ACTIVITY ps_activity )
{
	ps->ps_active_operation = ps_activity->ps_active_operation;
	ps->ps_active_thread = ps_activity->ps_active_thread;
	ps->ps_active_operation_specific = ps_activity->ps_active_operation_specific;
}

void unicom_activity_clear( UNICOM ps )
{
	ps->ps_active_operation = NULL;
	ps->ps_active_thread = NULL;
	ps->ps_active_operation_specific = NULL;

}

void unicom_activity_store( UNICOM ps, UNICOM_ACTIVITY ps_activity )
{
	ps_activity->ps_active_operation = ps->ps_active_operation;
	ps_activity->ps_active_thread = ps->ps_active_thread;
	ps_activity->ps_active_operation_specific = ps->ps_active_operation_specific;


	unicom_activity_clear( ps );
}




bool unicom_thread_stop( UNICOM ps, UNICOM_THREAD ps_thread, bool b_error ) 
{
	UNICOM_THREAD ps_last_active_thread = ps->ps_active_thread;
	unicom_thread_t s_thread_instance; // we must work with instance because, thread handler can be overriden by this call

	if ( ps_thread == NULL )
		return false; // no thread specified

	if ( unicom_thread_state( ps_thread ) == UNICOM_THREAD_STATE_IDLE )
		return false ;

	// THREAD is immediatelly marked as IDLE
	// only to inform about event
	ps->ps_active_thread = ps_thread;
	unicom_event( ps, UNICOM_EVENT_THREAD_STOP );
	ps->ps_active_thread = ps_last_active_thread;

	// interrupt last running thread
	unicom_thread_interrupt( ps, ps_last_active_thread );

	// put thread into clearing instance, because ps_thread may change during this process
	memcpy( &s_thread_instance, ps_thread, sizeof( unicom_thread_t ) );

	if ( unicom_thread_state( ps_thread ) == UNICOM_THREAD_STATE_PREPARING )
	{
		// perform state change only
		unicom_thread_state_set( ps_thread, UNICOM_THREAD_STATE_IDLE );
	}
	else
	{
		// perform state change immediatelly
		unicom_thread_state_set( ps_thread, UNICOM_THREAD_STATE_IDLE );

		// process instance - with possible internal change of thread, neccessary to work with copy
		// stop thread operation immediatelly
		unicom_operation_stop( ps, b_error, &s_thread_instance );
	}
		
	unicom_execution_event( ps, &s_thread_instance, b_error ? UNICOM_EXECUTION_EVENT_ERROR : UNICOM_EXECUTION_EVENT_DONE );	

	unicom_thread_event( ps, &s_thread_instance, UNICOM_THREAD_EVENT_STOP );

	// set back previous processing thread if other
	if ( ps_last_active_thread != ps_thread && ps_last_active_thread != NULL )
		unicom_thread_resume( ps, ps_last_active_thread );

	return true;

}


bool unicom_thread_runnable( UNICOM_THREAD ps_thread, bool b_forced )
{
	if ( ps_thread->pui32_operations_list == NULL )
		return false;

	if ( unicom_thread_state( ps_thread ) == UNICOM_THREAD_STATE_IDLE )
		return false;


	// not runnable when delay timer running
	if ( unicom_timer_running( &ps_thread->s_delay_timer ) )
	{
		if ( !unicom_timer_expired( &ps_thread->s_delay_timer ) && !b_forced )
			return false;

		// stop timer
		unicom_timer_stop( &ps_thread->s_delay_timer );
	}

	return true;
}


bool unicom_thread_operation_next( UNICOM_THREAD ps_thread )
{
	// is possible to go to next?
	if ( ( ps_thread->pui32_operation_selected + 1 ) < ps_thread->pui32_operations_list + ps_thread->ui32_operations_count )
		unicom_thread_operation_select( ps_thread, ++ps_thread->pui32_operation_selected );		
	else
		unicom_thread_operation_select( ps_thread, NULL );

	if ( !unicom_thread_operation_selected( ps_thread, NULL ) )
		return false;

	return true;

}


bool unicom_timer_running( UNICOM_TIMER ps_timer )
{
	return ps_timer->b_running;
}

UNICOM_THREAD_STATE unicom_thread_state( UNICOM_THREAD ps_thread )
{
	return ps_thread->e_state;
}




/*-----------------------------------------------------------
                        UNICOM
-----------------------------------------------------------*/

bool unicom_execution_active( UNICOM ps )
{
	if ( ps->ps_active_operation == NULL && ps->ps_active_thread == NULL)
		return false;

	return true;
}

bool unicom_threads_registered( UNICOM ps )
{
	return ps->ps_settings->ui32_thread_pool_size != 0;
}

UNICOM_THREAD unicom_thread_last( UNICOM ps )
{
	UNICOM_SETTINGS ps_settings = ps->ps_settings;
	return &( ps_settings->as_thread_pool[ ps_settings->ui32_thread_pool_size - 1 ] );
}

UNICOM_THREAD unicom_thread_first( UNICOM ps )
{
	UNICOM_SETTINGS ps_settings = ps->ps_settings;
	return &( ps_settings->as_thread_pool[ 0 ] );
}

UNICOM_THREAD unicom_thread_next( UNICOM ps, UNICOM_THREAD ps_thread )
{
	UNICOM_SETTINGS ps_settings = ps->ps_settings;
	ps_thread++; // try next one

	if ( ps_settings->as_thread_pool + ps_settings->ui32_thread_pool_size <= ps_thread )
		return NULL;

	return ps_thread;
		 
}


bool unicom_state_check_set( UNICOM ps, UNICOM_STATE e_check_state, UNICOM_STATE e_state )
{
	if ( e_check_state != ps->e_state )
		return false;
	
	unicom_state_set( ps, e_state );

	return true;
}


void unicom_execution_context_init( UNICOM_EXECUTION_CONTEXT ps_execution_context, bool b_repeat )
{
	struct unicom_operation_execution_repeat_context_s s = ps_execution_context->s_operation_repeat_context;
	memset( ps_execution_context, 0, sizeof( unicom_execution_context_t ) );
	if ( b_repeat )
		ps_execution_context->s_operation_repeat_context = s;

}

void unicom_tick_timer_restart( UNICOM ps )
{
	if ( unicom_timer_timeout( &ps->s_tick_timer )!= UNICOM_TIMEOUT_NONE )
		unicom_timer_start( &ps->s_tick_timer );
}

void unicom_tick_timer_stop( UNICOM ps )
{
	unicom_timer_stop( &ps->s_tick_timer );
}

void unicom_tick_timer_set( UNICOM ps, uint32_t ui32_tick_time_ms )
{
	unicom_timer_init( &ps->s_tick_timer, ui32_tick_time_ms );

	unicom_tick_timer_restart( ps );

}


bool unicom_init( UNICOM ps, UNICOM_SETTINGS ps_settings, const unicom_specific_t* ps_generic_base, void *p_app_object )
{
	
	UNICOM_THREAD ps_thread;

	if ( !gb_unicom_initialised )
	{
		MDG_MODULE_REGISTER( "unicom", MDG_TRACING_ALL );
		gb_unicom_initialised = true;
	}

	memset( ps, 0, sizeof( unicom_t ) );	
	ps->ps_settings = ps_settings;
	ps->ps_generic = ps_generic_base;

	unicom_timer_init( &ps->s_instruction_processing_event_timer, UNICOM_INSTRUCTION_PROCESSING_EVENT_TIME_MS );
	unicom_timer_init( &ps->s_thread_interrupt_timer, ps_settings->ui32_thread_switching_time_ms );
	unicom_tick_timer_set( ps, ps_settings->ui32_tick_ms_default );
	
	
	unicom_execution_context_init( &ps->s_execution_context, false );
	ps->p_app_object = p_app_object;

	if ( !unicom_specific_event( ps, ps->ps_generic, UNICOM_SPECIFIC_EVENT_INIT ) )
		return false;

	unicom_state_set( ps, UNICOM_STATE_IDLE );

	unicom_flag_internal_set( ps, UNICOM_FLAG_INTERNAL_INITIALISED );

	ps_thread = unicom_thread_first( ps );
	while ( ps_thread != NULL )
	{
		unicom_thread_init( ps, ps_thread );
		ps_thread = unicom_thread_next( ps, ps_thread  );
	}

	return true;	
}


void unicom_execution_context_clear( UNICOM ps )
{
	memset( &ps->s_execution_context, 0, sizeof( unicom_execution_context_t ) );
}

UNICOM_THREAD unicom_thread_get( UNICOM ps, uint32_t ui32_thread_id )
{
	if ( ui32_thread_id >= ps->ps_settings->ui32_thread_pool_size )
		UNICOM_ERROR_GOTO( UNICOM_ERROR_THREAD_UNKNOWN );

	return &ps->ps_settings->as_thread_pool[ ui32_thread_id ];

lbl_mdg_error:
	return NULL;
}


bool unicom_execute_running( UNICOM ps, uint32_t ui32_thread_id )
{
	return !unicom_execute_ready( ps, ui32_thread_id );
}

bool unicom_execute_ready( UNICOM ps, uint32_t ui32_thread_id )
{
	UNICOM_THREAD ps_thread = unicom_thread_get( ps, ui32_thread_id );
	if ( ps_thread == NULL || !unicom_started( ps ) )
		return false;

	if ( unicom_thread_state( ps_thread ) != UNICOM_THREAD_STATE_IDLE )
		return false;

	return true;
}

bool unicom_event( UNICOM ps, UNICOM_EVENT e )
{
	uint32_t ui32_flags = UNICOM_EVENT_FLAGS_NONE;
  UNICOM_STATE e_original_state = unicom_state( ps );
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	
	unicom_event_data_t s_event_data;

	// nothing to do?
	if ( ps->ps_settings->f_event == NULL )
		return true; 
	
	
	memset( &s_event_data, 0, sizeof( s_event_data ) );
	
	if ( ps->ps_settings->pc_identifier != 0 )
	{
		ui32_flags |= UNICOM_EVENT_FLAG_IDENTIFIER;
		s_event_data.pc_identifier = ps->ps_settings->pc_identifier;
	}

	switch ( e )
	{
		default:
			break;
		case UNICOM_EVENT_INSTRUCTION_NONE:
		case UNICOM_EVENT_INSTRUCTION_DONE:
		case UNICOM_EVENT_INSTRUCTION_PROCESS:
		case UNICOM_EVENT_INSTRUCTION_TIMEOUT:
		case UNICOM_EVENT_INSTRUCTION_ERROR:
		case UNICOM_EVENT_INSTRUCTION_ENTER:
		case UNICOM_EVENT_INSTRUCTION_ENTER_ERROR:
		case UNICOM_EVENT_LEXEM_PROCESSING:
		case UNICOM_EVENT_LEXEM_PROCESSED:
		case UNICOM_EVENT_LEXEM_PROCESSING_AGAIN:
		case UNICOM_EVENT_INTERRUPT:
		case UNICOM_EVENT_DEBUG:
		case UNICOM_EVENT_RESUME:
		
			if ( ps->ps_active_operation != NULL )
			{
				s_event_data.ui32_active_operation = ps->ps_active_operation->ui32_id;
				ui32_flags |= UNICOM_EVENT_FLAG_OPERATION;

				if ( ps_context->ps_instruction != NULL )
				{
					s_event_data.ui32_active_instruction = ps_context->ps_instruction->ui32_id;
					s_event_data.ui32_active_instruction_step = unicom_instruction_step_get( ps_context );
					ui32_flags |= UNICOM_EVENT_FLAG_INSTRUCTION;
					ui32_flags |= UNICOM_EVENT_FLAG_INSTRUCTION_STEP;
				}
				
			}

			if ( ps_context->ps_instruction_specific != NULL )
			{
				s_event_data.pc_target_specific_name = ps_context->ps_instruction_specific->pc_id;
				ui32_flags |= UNICOM_EVENT_FLAG_SPECIFIC;	 
			}

			if ( ps->ps_active_thread != NULL )
			{
				s_event_data.ui32_active_thread = ps->ps_active_thread - ps->ps_settings->as_thread_pool;
				ui32_flags |= UNICOM_EVENT_FLAG_THREAD;
			}
			break;

		case UNICOM_EVENT_OPERATION_ENTER:
		case UNICOM_EVENT_OPERATION_REPEAT:
		case UNICOM_EVENT_OPERATION_ERROR:
		case UNICOM_EVENT_OPERATION_RETURN:
		case UNICOM_EVENT_OPERATION_ENTER_ERROR:

			if ( ps->ps_active_operation != NULL )
			{
				s_event_data.ui32_active_operation = ps->ps_active_operation->ui32_id;
				ui32_flags |= UNICOM_EVENT_FLAG_OPERATION;
			}

			if ( ps->ps_active_operation_specific != NULL )
			{
				s_event_data.pc_target_specific_name = ps->ps_active_operation_specific->pc_id;
				ui32_flags |= UNICOM_EVENT_FLAG_SPECIFIC;	 
			}


			if ( ps->ps_active_thread != NULL )
			{
				s_event_data.ui32_active_thread = ps->ps_active_thread - ps->ps_settings->as_thread_pool;
				ui32_flags |= UNICOM_EVENT_FLAG_THREAD;
			}
			break;

		case UNICOM_EVENT_SELECTED:
			s_event_data.pc_target_specific_name = ps->ps_specific->pc_id;
			ui32_flags |= UNICOM_EVENT_FLAG_SPECIFIC;
			break;

		case UNICOM_EVENT_RESPONSE_LEXEM:
		case UNICOM_EVENT_RESPONSE_LEXEM_PROCESSING:
		  s_event_data.ps_lexem = unicom_response_top( ps );
		  if ( s_event_data.ps_lexem != NULL )
		    ui32_flags |= UNICOM_EVENT_FLAG_LEXEM;

		  if ( ps->ps_active_thread != NULL )
		  {
			  s_event_data.ui32_active_thread = ps->ps_active_thread - ps->ps_settings->as_thread_pool;
			  ui32_flags |= UNICOM_EVENT_FLAG_THREAD;
		  }
		  break;


		case UNICOM_EVENT_THREAD_SELECTION:
		case UNICOM_EVENT_THREAD_SELECTED:
		case UNICOM_EVENT_THREAD_START:
		case UNICOM_EVENT_THREAD_EXECUTE:
		case UNICOM_EVENT_THREAD_ERROR:
		case UNICOM_EVENT_THREAD_EXECUTE_BUSY:
		case UNICOM_EVENT_THREAD_RETURN:
		case UNICOM_EVENT_THREAD_STOP:
			s_event_data.ui32_active_thread = unicom_thread_id( ps, ps->ps_active_thread );
			ui32_flags |= UNICOM_EVENT_FLAG_THREAD;
			break;
	}
	
	s_event_data.ui32_flags = ui32_flags;

	ps->ps_settings->f_event( ps->p_app_object, e, &s_event_data );

	return unicom_state( ps ) == e_original_state;
}

bool unicom_event_flag_get( UNICOM_EVENT_DATA ps_data, UNICOM_EVENT_FLAG e_flag ) 
{
	return ps_data->ui32_flags & (uint32_t)e_flag;
}


UNICOM_LEXEM unicom_event_lexem( UNICOM_EVENT_DATA ps_data )
{
	if ( !unicom_event_flag_get( ps_data, UNICOM_EVENT_FLAG_LEXEM ) )
		return false;
	
	return ps_data->ps_lexem;
}

bool unicom_event_instruction_step( UNICOM_EVENT_DATA ps_data, uint32_t *pui_id )
{
	if ( !unicom_event_flag_get( ps_data, UNICOM_EVENT_FLAG_INSTRUCTION_STEP ) )
		return false;


	if ( pui_id )
		*pui_id = ps_data->ui32_active_instruction_step;

	return true;
}

bool unicom_event_instruction( UNICOM_EVENT_DATA ps_data, uint32_t *pui_id )
{
	if ( !unicom_event_flag_get( ps_data, UNICOM_EVENT_FLAG_INSTRUCTION ) )
		return false;


	if ( pui_id )
		*pui_id = ps_data->ui32_active_instruction;

	return true;
}

bool unicom_event_operation( UNICOM_EVENT_DATA ps_data, uint32_t *pui_id )
{
	if ( !unicom_event_flag_get( ps_data, UNICOM_EVENT_FLAG_OPERATION ) )
		return false;


	if ( pui_id )
		*pui_id = ps_data->ui32_active_operation;

	return true;
}

bool unicom_event_thread( UNICOM_EVENT_DATA ps_data, uint32_t *pui_id )
{
	if ( !unicom_event_flag_get( ps_data, UNICOM_EVENT_FLAG_THREAD ) )
		return false;


	if ( pui_id )
		*pui_id = ps_data->ui32_active_thread;

	return true;
}

bool unicom_event_identifier( UNICOM_EVENT_DATA ps_data, const char **ppc_id )
{
	if ( !unicom_event_flag_get( ps_data, UNICOM_EVENT_FLAG_IDENTIFIER ) )
		return false;

	if ( ppc_id != NULL )
		*ppc_id = ps_data->pc_identifier;

	return true;
}

bool unicom_event_specific_name( UNICOM_EVENT_DATA ps_data, const char **ppc_id )
{
	if ( !unicom_event_flag_get( ps_data, UNICOM_EVENT_FLAG_SPECIFIC ) )
		return false;

	if ( ppc_id != NULL )
		*ppc_id = ps_data->pc_target_specific_name;

	return true;
}


bool unicom_execute( UNICOM ps, uint32_t ui32_thread_id, const uint32_t *pui32_operations, uint32_t ui32_operations_count, uint32_t ui32_delay_ms, uint32_t ui32_repeat_count, unicom_execution_event_f f_event, uint32_t ui32_flags )
{
	return unicom_execute_2( ps, ui32_thread_id, pui32_operations, ui32_operations_count, ui32_delay_ms, ui32_repeat_count, f_event, NULL, ui32_flags, NULL );
}

bool unicom_execute_simple( UNICOM ps, uint32_t ui32_thread_id, uint32_t ui32_operation, unicom_execution_event_f f_event, void *p_app_object, uint32_t ui32_flags )
{
	UNICOM_THREAD ps_thread;

	MDG_ASSERT( unicom_state( ps ) == UNICOM_STATE_IDLE );
	
	if ( !unicom_execute_ready( ps, ui32_thread_id ) )
		return false;

	ps_thread = unicom_thread_get( ps, ui32_thread_id );
	
	ps_thread->ui32_simple_execution_operation = ui32_operation;

	return unicom_execute_2( ps, ui32_thread_id, &ps_thread->ui32_simple_execution_operation, 1, UNICOM_DELAY_NONE, UNICOM_REPEAT_NONE, f_event, p_app_object, ui32_flags, NULL );

lbl_mdg_assert:
	return false;

}


bool unicom_execute_2( UNICOM ps, uint32_t ui32_thread_id, const uint32_t *pui32_operations, uint32_t ui32_operations_count, uint32_t ui32_delay_ms, uint32_t ui32_repeat_count, unicom_execution_event_f f_event, void *p_app_object, uint32_t ui32_flags, unicom_thread_event_f f_thread_event )
{
	UNICOM_THREAD ps_thread = NULL;
	unicom_activity_t s_activity;

	MDG_ASSERT( unicom_state( ps ) == UNICOM_STATE_IDLE );

	ps_thread = unicom_thread_get( ps, ui32_thread_id );
	MDG_ASSERT( ps_thread == NULL );

	if ( unicom_thread_state( ps_thread ) != UNICOM_THREAD_STATE_IDLE )	
	{
		unicom_activity_store( ps, &s_activity ); 
		ps->ps_active_thread = ps_thread;
		unicom_event( ps, UNICOM_EVENT_THREAD_EXECUTE_BUSY );
		unicom_activity_restore( ps, &s_activity ); 
		goto lbl_nok;
	}

	MDG_ASSERT( false == unicom_thread_start( ps_thread, pui32_operations, ui32_operations_count, ui32_delay_ms, ui32_repeat_count, f_event, p_app_object, ui32_flags, f_thread_event ) );

	// send event with actual thread and after done restore back original working thread

	unicom_activity_store( ps, &s_activity ); 

	ps->ps_active_thread = ps_thread;

	if ( false == unicom_event( ps, UNICOM_EVENT_THREAD_EXECUTE ) )
	{
		unicom_activity_restore( ps, &s_activity );
		MDG_ERROR();
	}

	unicom_activity_restore( ps, &s_activity ); 

	unicom_run( ps );

	return true;
lbl_nok:
lbl_mdg_error:
lbl_mdg_assert:
	return false;
}

const unicom_operation_t* unicom_specific_operation_get( const unicom_specific_t *ps_specific, uint32_t ui32_id )
{
	const unicom_operation_t *ps = ps_specific->as_operations;
	const unicom_operation_t *ps_end = ps_specific->as_operations + ps_specific->ui32_operations_count;

	while ( ps != ps_end )
	{
		if ( ps->ui32_id == ui32_id )
			break;
		ps++;
	} 

	if ( ps == ps_end )
		return NULL;

	return ps;
}

const unicom_instruction_t *unicom_instructions_register_get( uint32_t ui32_id, const unicom_instruction_t* ps_register, uint32_t ui_count )
{
	const unicom_instruction_t *ps_iterator;
	const unicom_instruction_t *ps_end;

	if ( ps_register == NULL )
		return NULL;

	ps_iterator = ps_register;
	ps_end  = ps_register + ui_count; 

   while ( ps_iterator < ps_end )
	{ 
		if ( ps_iterator->ui32_id == ui32_id ) 
      	return ps_iterator; 

		ps_iterator++;
	}

	// not found
	return NULL;
}


const unicom_instruction_t* unicom_specific_instruction_get( const unicom_specific_t *ps_specific, uint32_t ui32_id )
{	
	return unicom_instructions_register_get( ui32_id, ps_specific->as_instructions, ps_specific->ui32_instructions_count );
}

UNICOM_INSTRUCTION_STATE unicom_instruction_state_get( struct unicom_execution_context_s *ps_context )
{
	return ps_context->e_instruction_state;
}

void unicom_instruction_state_set( struct unicom_execution_context_s *ps_context, UNICOM_INSTRUCTION_STATE e_state )
{
	ps_context->e_instruction_state = e_state;
}

bool unicom_instruction_repeat_flag( struct unicom_instruction_repeat_s *ps_repeat, UNICOM_INSTRUCTION_REPEAT_FLAG e )
{
	return ps_repeat->ui32_flags & e;
}

bool unicom_instruction_next( UNICOM ps  )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	struct unicom_instruction_repeat_s *ps_repeat = &ps_context->s_instruction_return_repeat;

	if ( ps_repeat->ui32_limit != 0 )
	{
		bool b_allowed = true;
		if ( unicom_instruction_repeat_flag( ps_repeat, UNICOM_INSTRUCTION_REPEAT_INVOKED_ONLY ) && ps_context->ui8_invoke_instructions_count > 0 )
			b_allowed = true;

		if ( ps_repeat->ui32_counter < ps_repeat->ui32_limit && b_allowed )
		{
			if ( unicom_instruction_repeat_flag( ps_repeat, UNICOM_INSTRUCTION_REPEAT_INVOKE_PARENT ) && ps_context->ui8_invoke_instructions_count > 0 )
				ps_context->ui8_invoke_instructions_count = 0; // do not invoke

			ps_repeat->ui32_counter++;
			ps_repeat->ui32_limit = 0; // limit must be set again in next run
			return true;
		}
	}
	// clean return repeat counters if continues
	unicom_instruction_return_repeat_clean( ps_context );

	// try to execute invokable instructions if initialised ( or process operation instructions... )
	if ( ps_context->ui8_invoke_instructions_count > 0 )
	{
		// if instruction iterator change stopped, return with no next instruction selected
		if ( ps_context->ui8_invoke_instructions_iterator == UNICOM_INSTRUCTION_ITERATOR_NEXT_STOP )
			return false;

		if ( ps_context->ui8_invoke_instructions_iterator == UNICOM_INSTRUCTION_ITERATOR_INIT )
			ps_context->ui8_invoke_instructions_iterator = 0;
		else
			ps_context->ui8_invoke_instructions_iterator++;
	
		if ( ps_context->ui8_invoke_instructions_iterator >= ps_context->ui8_invoke_instructions_count )
			return false;


		return true; // instruction ready
	}

	// if instruction iterator change stopped, return with no next instruction selected
	if ( ps_context->ui8_operation_instructions_iterator == UNICOM_INSTRUCTION_ITERATOR_NEXT_STOP )
		return false;

	// not checked for init because it is initialised by operation instruction selection to 0
	ps_context->ui8_operation_instructions_iterator++;

	// operation selection
	if ( ps_context->ui8_operation_instructions_iterator >= ps->ps_active_operation->ui8_instructions_count )
		return false;

	return true;
}

void unicom_instruction_init( UNICOM_INSTRUCTION ps, uint32_t ui32_id )
{
	memset( ps, 0, sizeof( unicom_instruction_t ) );
	ps->ui32_id = ui32_id;

	
}

bool unicom_instruction_selection( UNICOM ps )
{
	const unicom_instruction_t* ps_instruction = NULL;
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	const unicom_operation_t* ps_operation = ps->ps_active_operation;
	uint32_t ui32_id;

	// check invokable instructions at first, then operation instructions
	if ( ps_context->ui8_invoke_instructions_count > 0 )
	{
		ui32_id = ps_context->aui8_invoke_instructions[ ps_context->ui8_invoke_instructions_iterator ];
	}
	// operation instructions list
	else
	{
		// if no instruction, goto immediatelly to instruction done step
		if ( ps_operation->ui8_instructions_count == 0 || ps_operation->aui32_instructions == NULL ) 
		{
			ps_context->ps_instruction = NULL;
			if ( !unicom_event( ps, UNICOM_EVENT_INSTRUCTION_NONE ) )
				return false;

			unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_DONE );		
			return true;
		}

		ui32_id = ps->ps_active_operation->aui32_instructions[ ps_context->ui8_operation_instructions_iterator ];
	}

	// try to get from specific
	if ( ps->ps_specific != NULL )
	{
		ps_instruction = unicom_specific_instruction_get( ps->ps_specific, ui32_id );
		if ( ps_instruction != NULL )
			ps_context->ps_instruction_specific = ps->ps_specific;
			
	}

	// try to get from generic if not selected yet
	if ( ps_instruction == NULL )
	{
		ps_instruction = unicom_specific_instruction_get( ps->ps_generic, ui32_id );

		if ( ps_instruction == NULL )
		{
			unicom_instruction_t s_tmp_instruction;
			unicom_instruction_init( &s_tmp_instruction, ui32_id );
			ps_context->ps_instruction = &s_tmp_instruction;

			if ( !unicom_event( ps, UNICOM_EVENT_INSTRUCTION_ENTER_ERROR ) )
				return false;

			UNICOM_ERROR_GOTO( UNICOM_ERROR_INSTRUCTION_SELECTION );
		}

		ps_context->ps_instruction_specific = ps->ps_generic;
	}

	ps_context->ps_instruction = ps_instruction;

	// default response key for instruction is not set
	unicom_instruction_response_key_clear( ps_context );

	unicom_instruction_step_set( ps_context, UNICOM_INSTRUCTION_STEP_INIT );	
	unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_INIT );
	if ( !unicom_event( ps, UNICOM_EVENT_INSTRUCTION_ENTER ) )
		return false;

	unicom_timer_init( &ps_context->s_instruction_wait_timer, 0 );
	
	return true;

lbl_mdg_error:
	ps_context->ps_instruction = NULL;
	return false;
}

void unicom_operation_init( UNICOM_OPERATION ps, uint32_t ui32_id )
{
	memset( ps, 0, sizeof( unicom_operation_t ) );
	ps->ui32_id = ui32_id;
}

void unicom_instruction_wait_stop( UNICOM_EXECUTION_CONTEXT ps_context )
{
	unicom_timer_stop( &ps_context->s_instruction_wait_timer );
}


void unicom_instruction_wait_set( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_wait_ms ) 
{

	unicom_timer_reset( &ps_context->s_instruction_wait_timer, ui32_wait_ms );
	unicom_timer_start( &ps_context->s_instruction_wait_timer );
	
	// prolong instruction timeout if wait would led to instruction timeout
	if ( unicom_timer_running( &ps_context->s_instruction_timer ) )
	{
		uint32_t ui32_time_to_expire;

		const unicom_instruction_t* ps_instruction = ps_context->ps_instruction;

		// if not yet waited and waiting could lead to timeout, prolong them once
		if ( unicom_timer_timeout( &ps_context->s_instruction_timer ) == ps_instruction->ui32_timeout_ms )
		{
			ui32_time_to_expire = unicom_timer_time_to_expire_ms( &ps_context->s_instruction_timer );
			if ( ui32_time_to_expire < ui32_wait_ms )
				unicom_instruction_timeout_add( ps_context, ui32_wait_ms );
		}
	}

}

void unicom_operation_wait_set( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_wait_ms ) 
{
	unicom_timer_reset( &ps_context->s_operation_wait_timer, ui32_wait_ms );
	unicom_timer_start( &ps_context->s_operation_wait_timer );
}
	

bool unicom_operation_start( UNICOM ps, uint32_t ui32_id, unicom_execution_event_f f_event, UNICOM_THREAD ps_thread, bool b_operation_repeat )
{
	const unicom_operation_t *ps_operation = NULL;

	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;

	ps->ps_active_operation = NULL;
	ps->ps_active_thread = ps_thread;
	ps->ps_active_operation_specific = NULL;

	// try to find it in selected specific
	if ( ps->ps_specific != NULL )
	{
		ps_operation = unicom_specific_operation_get( ps->ps_specific, ui32_id );
		if ( ps_operation != NULL )
			ps->ps_active_operation_specific = ps->ps_specific;
	}

	if ( ps_operation == NULL ) // or specific not selected
	{
		ps_operation = unicom_specific_operation_get( ps->ps_generic, ui32_id );
		if ( ps_operation != NULL )
			ps->ps_active_operation_specific = ps->ps_generic;
	}
	
	
	// not selected in specific nor generic ? => error
	if ( ps_operation == NULL )
	{
		unicom_operation_t s_tmp_operation;
		unicom_operation_init( &s_tmp_operation, ui32_id );

		ps->ps_active_operation = &s_tmp_operation;

		ps->ps_active_operation = NULL;

		if ( unicom_ready( ps ) )
			unicom_event( ps, UNICOM_EVENT_OPERATION_ENTER_ERROR );
		else
			unicom_event( ps, UNICOM_EVENT_OPERATION_ENTER_ERROR_ALLOWED );


		return false;
	}

	ps->ps_active_operation = ps_operation;

	if ( ps_thread == NULL || unicom_thread_state( ps_thread ) != UNICOM_THREAD_STATE_INTERRUPT )
	{
		// reinitialize execution context
		unicom_execution_context_init( ps_context, b_operation_repeat );

		if ( !unicom_event( ps, UNICOM_EVENT_OPERATION_ENTER ) )
			return false;

		if ( !unicom_instruction_selection( ps ) )
			MDG_ERROR();

		// start wait at enter timeout	
		unicom_operation_wait_set( ps_context, ps->ps_active_operation->ui32_operation_wait_at_enter_ms );

					
	}
	else
	{
		// resume interrupted execution context
		memcpy( ps_context, &ps_thread->s_context_interrupt.s_context, sizeof( ps_thread->s_context_interrupt.s_context ) );

		if ( !unicom_event( ps, UNICOM_EVENT_RESUME ) )
			return false;
	}

	if ( !b_operation_repeat ) // no operation repeat means, clear repeat counters
	{
		unicom_repeat_init( &ps_context->s_operation_repeat_context.s_repeats_ok, ps_operation->ui32_repeats_ok );
		unicom_repeat_init( &ps_context->s_operation_repeat_context.s_repeats_error, ps_operation->ui32_repeats_error );
	}


	return true;

lbl_mdg_error:
	return false;
}



void unicom_repeat_reset( UNICOM_REPEAT_COUNTER ps_repeat )
{
	ps_repeat->ui32_counter = 0;
}

void unicom_repeat_init( UNICOM_REPEAT_COUNTER ps_repeat, uint32_t ui32_limit )
{
	ps_repeat->ui32_limit = ui32_limit;
	unicom_repeat_reset( ps_repeat );
}



bool unicom_repeat_next( UNICOM_REPEAT_COUNTER ps_repeat )
{
	if ( ps_repeat->ui32_counter == ps_repeat->ui32_limit )
	{
		unicom_repeat_reset( ps_repeat );
		return false;
	}

	ps_repeat->ui32_counter++;
	return true;
}

UNICOM_PROCESSING_RESULT unicom_operation_repeat( UNICOM ps, bool b_error )
{
	UNICOM_EXECUTION_CONTEXT ps_context = &ps->s_execution_context;
	UNICOM_THREAD ps_thread = ps->ps_active_thread;
	const unicom_operation_t *ps_operation = ps->ps_active_operation;

	if ( b_error )
	{
		if ( !unicom_repeat_next( &ps_context->s_operation_repeat_context.s_repeats_error ) )
			return UNICOM_PROCESSING_ERROR;
	}
	else
	{
		// clear error repeats
		unicom_repeat_reset( &ps_context->s_operation_repeat_context.s_repeats_error );

		// perform repeat next
		if ( !unicom_repeat_next( &ps_context->s_operation_repeat_context.s_repeats_ok ) )
			return UNICOM_PROCESSING_NONE;
	}

	unicom_event( ps, UNICOM_EVENT_OPERATION_REPEAT );

	if ( ps_thread != NULL )
	{
		// select thread next operation ( with instruction )
		if ( !unicom_thread_operation_selection( ps, ps_thread, true ) )
			MDG_ERROR();
	}
	else if ( ps_operation != NULL ) // if no thread, repeat last operation
	{
		// attach it to processor
		if ( !unicom_operation_start( ps, ps_operation->ui32_id, NULL, NULL, true ) )
			MDG_ERROR();
	}
	else
		MDG_ERROR();


	if ( unicom_interrupt( ps, true, false ) ) // no thread repeat, only operation repeat
		return UNICOM_PROCESSING_DONE; // interrupted

	return UNICOM_PROCESSING_RUN;

lbl_mdg_error:
	return UNICOM_PROCESSING_ERROR;
}


bool unicom_thread_operation_selection( UNICOM ps, UNICOM_THREAD ps_thread, bool b_operation_repeat )
{
	uint32_t ui32_id = UNICOM_OPERATIONS_END;

	// get thread operation
	if ( !unicom_thread_operation_selected( ps_thread, &ui32_id  ) )
		return false;

	// attach it to processor
	if ( !unicom_operation_start( ps, ui32_id, ps_thread->f_event, ps_thread, b_operation_repeat ) )
		return false;

	return true;
}

uint32_t unicom_thread_id( UNICOM ps, UNICOM_THREAD ps_thread )
{
	return (uint32_t)( ( (intptr_t)ps_thread - (intptr_t)ps->ps_settings->as_thread_pool ) / (intptr_t)sizeof( unicom_thread_t ) );
}

bool unicom_thread_event( UNICOM ps, UNICOM_THREAD ps_thread, UNICOM_THREAD_EVENT e_event )
{
	void *p_app_object;

	if ( ps_thread->f_thread_event == NULL )
		return true; // nothing to process

	if ( ps_thread != NULL && ps_thread->p_app_object != NULL )
		p_app_object = ps_thread->p_app_object;
	else
		p_app_object = ps->p_app_object;
	
	return ps_thread->f_thread_event( p_app_object, e_event, ps_thread );
}


UNICOM_PROCESSING_RESULT unicom_thread_try_to_start( UNICOM ps, UNICOM_THREAD ps_thread )
{

//	UNICOM_PROCESSING_RESULT e_result = UNICOM_PROCESSING_NONE;
	if ( unicom_thread_runnable( ps_thread, false ) )
	{
		// INTERRUPT state is solved in operation_selection	
		if ( unicom_thread_state( ps_thread ) == UNICOM_THREAD_STATE_INTERRUPT )
		{
			if ( !unicom_thread_resume( ps, ps_thread ) )
				return UNICOM_PROCESSING_NONE;

			return UNICOM_PROCESSING_DONE; // resume complete
		}
		else
		{
			unicom_thread_restart( ps_thread, true );

			// select active thread for events
			ps->ps_active_thread = ps_thread;
			MDG_ASSERT( false == unicom_event( ps, UNICOM_EVENT_THREAD_START ) );

			MDG_ASSERT( false == unicom_thread_event( ps, ps_thread, UNICOM_THREAD_EVENT_START ) );


			// operation is prepared to be selected ?
			MDG_ASSERT( false == unicom_thread_operation_selection( ps, ps_thread, false ) );

			unicom_execution_event( ps, ps_thread, UNICOM_EXECUTION_EVENT_INIT );

			// execution may changed thread state, check it
			if ( unicom_thread_runnable( ps_thread, false ) )
			{
/*				if ( unicom_thread_interruptable( ps_thread ) )
					unicom_thread_interrupt_timer_start( ps );*/
	
				unicom_thread_state_set( ps_thread, UNICOM_THREAD_STATE_RUNNING );

				return UNICOM_PROCESSING_DONE;
			}
		}
	}

	return UNICOM_PROCESSING_NONE;

lbl_mdg_assert:
	unicom_activity_clear( ps );
	return UNICOM_PROCESSING_ERROR;
	
}

UNICOM_STATE unicom_state( UNICOM ps )
{
	return ps->e_state;
}

void unicom_instruction_request_send( UNICOM ps )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;

	if ( unicom_data_present( &ps_context->s_request ) )
	{
		bool b_ack_off = ps_context->b_acknowledgement_off;
		bool b_defs = ps_context->b_request_use_defaults;
		unicom_data_t s_request;

		// because of possible cyclic call during error / stop processing it is needed to clear them all before processing
		memcpy( &s_request, &ps_context->s_request, sizeof( ps_context->s_request ) );
		unicom_data_clear( &ps_context->s_request ); // clear source

		if ( b_ack_off )
			ps_context->b_acknowledgement_off = false;

		if ( b_defs )
			ps_context->b_request_use_defaults = false;

		

		if ( !b_ack_off )
		{
			if ( unicom_flag_get( ps, UNICOM_FLAG_ACK ) )
			{
				if ( !unicom_event( ps, UNICOM_EVENT_INSTRUCTION_ACKNOWLEDGE_REQUIRED ) )
					return;

				unicom_context_flag_set( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE );
			}
			else
				unicom_context_flag_clear( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE );

		}
		else
			unicom_context_flag_clear( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE );
		
		if ( b_defs )
			unicom_request_begin( ps, &ps_context->ps_instruction_specific->s_request_begin ) ;
		else
			unicom_request_begin( ps, NULL ) ;

		if ( unicom_flag_get( ps, UNICOM_FLAG_REQUEST_OBJECT_RESPONSE_KEY ) )
			unicom_instruction_response_key_set( ps_context, unicom_data_object( &s_request ) );

		unicom_request_body( ps, &s_request );

		if ( b_defs )
			unicom_request_end( ps, &ps_context->ps_instruction_specific->s_request_end );
		else
			unicom_request_end( ps, NULL ) ;

	}
}

UNICOM_INSTRUCTION_STEP unicom_instruction_step_get( struct unicom_execution_context_s *ps_context )
{
	return ps_context->e_instruction_step;
}

void unicom_flag_change( UNICOM_EXECUTION_CONTEXT ps_context, UNICOM_FLAG e_flag , bool b_value )
{
	if ( b_value ) 
		ps_context->ui32_step_flags |= (uint32_t)e_flag;
	else
		ps_context->ui32_step_flags &= ~( (uint32_t)e_flag );
}

void unicom_instruction_process_event( UNICOM ps, bool b_forced )
{
	UNICOM_THREAD ps_thread = ps->ps_active_thread;

	UNICOM_TIMER ps_timer;

	if ( ps_thread == NULL )
		ps_timer = &ps->s_instruction_processing_event_timer;
	else
		ps_timer = &ps_thread->s_instruction_processing_event_timer;

	if ( !b_forced )
	{
		if ( unicom_timer_running( ps_timer ) && !unicom_timer_expired( ps_timer ) )
			return;

	}
	unicom_timer_restart( ps_timer );

	unicom_event( ps, UNICOM_EVENT_INSTRUCTION_PROCESS );
}

bool unicom_instruction_step_internal( UNICOM ps, UNICOM_THREAD ps_thread, struct unicom_execution_context_s *ps_context, void *p_app_object, UNICOM_INSTRUCTION_STEP e_step, UNICOM_LEXEM ps_lexem )
{
	unicom_instruction_step_set( ps_context, e_step );

	if ( ps_thread != NULL && ps_thread->p_app_object != NULL )
		p_app_object = ps_thread->p_app_object;
	else
		p_app_object = ps->p_app_object;
	
	switch ( e_step )
	{
		default:
			break;

		case UNICOM_INSTRUCTION_STEP_DONE:
			if ( unicom_instruction_problem( ps_context ) )
				unicom_context_flag_set( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_PROBLEM_RECOVERED );
			break;

		case UNICOM_INSTRUCTION_STEP_ERROR:
			unicom_context_flag_set( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ERROR_OCCURED );
			break;

		case UNICOM_INSTRUCTION_STEP_TIMEOUT:
			unicom_context_flag_set( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_TIMEOUT_OCCURED );
			break;
	}

	MDG_ASSERT( NULL == ps_context->ps_instruction );

	MDG_ASSERT( false == ps_context->ps_instruction->f_step( ps_context, p_app_object, unicom_instruction_step_get( ps_context ), ps_lexem ) );


	return true;

lbl_mdg_assert:
	return false;
}
	

UNICOM_INSTRUCTION_STEP_RESULT unicom_instruction_step( UNICOM ps, struct unicom_lexem_s *ps_lexem )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;

	UNICOM_INSTRUCTION_STEP e_step_original, e_step_next;
	UNICOM_INSTRUCTION_STATE e_state;
	UNICOM_STATE e_unicom_state;
	UNICOM_THREAD ps_thread = ps->ps_active_thread;
	void *p_app_object = NULL;
	bool b_result;

	// instruction callback, step performed by default
	if ( ps_context->ps_instruction->f_step == NULL )
		return UNICOM_INSTRUCTION_STEP_RESULT_NONE;

	e_step_original = unicom_instruction_step_get( ps_context );

	if ( ps_context->e_instruction_step_last_performed != e_step_original )
	{
		unicom_instruction_process_event( ps, true );
		ps_context->e_instruction_step_last_performed = e_step_original;
	}
	
	// prepare some attributes from CORE
	ps_context->ui32_step_flags = ps->ui32_flags;

	e_unicom_state = unicom_state( ps );

	b_result = unicom_instruction_step_internal( ps, ps_thread, ps_context, p_app_object, e_step_original, ps_lexem );

	if ( unicom_context_flag_get( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_STOP ) )
	{
		unicom_context_flag_clear( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_STOP );

		unicom_event( ps, UNICOM_EVENT_INSTRUCTION_STOP );

		unicom_thread_stop( ps, ps_thread, false ); // do not finish with error

		// call unicom stop
		unicom_stop( ps, false );
		return UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN;
	}

	if ( unicom_context_flag_get( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_STOP_ERROR ) )
	{
		// immediatelly turn into error processing
		unicom_context_flag_clear( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_STOP_ERROR );
		
		unicom_event( ps, UNICOM_EVENT_INSTRUCTION_STOP_ERROR );

		unicom_thread_stop( ps, ps_thread, true ); // do not finish with error
		return false; // error processing
	}
	

	if ( ps_context->e_instruction_step_last_performed != unicom_instruction_step_get( ps_context ) )
	{
		unicom_instruction_process_event( ps, true );
		ps_context->e_instruction_step_last_performed = unicom_instruction_step_get( ps_context );
	}
	
	// if processor state changes instruction processing stops without further processing
	if ( e_unicom_state != unicom_state( ps ) )
		return UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN; // unicom state changed, means some ERROR during processing
		
	// set back attributes to CORE
	ps->ui32_flags = ps_context->ui32_step_flags;


	// send if requested
	unicom_instruction_request_send( ps );

	if ( e_unicom_state != unicom_state( ps ) )
		return UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN; // unicom state changed, means some ERROR during processing

	if ( b_result == false )
	{
		UNICOM_INSTRUCTION_STATE e_state;
		
		unicom_error_set( UNICOM_ERROR_INSTRUCTION_PROCESSING );
		e_state = unicom_instruction_state_get( ps_context );

		// in ending states we don't switch back to running ( error ) state

		if ( e_state != UNICOM_INSTRUCTION_STATE_DONE 
		  && e_state != UNICOM_INSTRUCTION_STATE_ERROR 
		  && e_state != UNICOM_INSTRUCTION_STATE_IDLE )
			unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_ERROR );

		if ( e_unicom_state != unicom_state( ps ) )
			return UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN; // unicom state changed, means some ERROR during processing

		return UNICOM_INSTRUCTION_STEP_RESULT_ERROR;
	}

	e_step_next = unicom_instruction_step_get( ps_context );
	e_state = unicom_instruction_state_get( ps_context );


	// step changed! check state change and new step
	if ( e_step_next != e_step_original )
	{
		bool b_going_up = e_step_original < e_step_next;

		switch ( e_state )
		{
			case UNICOM_INSTRUCTION_STATE_INIT:
				unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_PROCESSING );
				break;


			case UNICOM_INSTRUCTION_STATE_TIMEOUT:
			case UNICOM_INSTRUCTION_STATE_IDLE: // resurection in case of instruction done processing
			case UNICOM_INSTRUCTION_STATE_ERROR:
				unicom_instruction_timeout_restart( ps_context ); // restart timeout when exit from timeout

				unicom_instruction_wait_stop( ps_context ); // restart timeout when exit from timeout


				// out of order -> reset ACK response flag
				unicom_context_flag_clear( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE );
				unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_PROCESSING );
				break;

			default:
				break;
		}

		// next step ERROR only out of ERROR state
		if ( e_step_next == UNICOM_INSTRUCTION_STEP_ERROR && e_state != UNICOM_INSTRUCTION_STATE_ERROR )
			unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_ERROR );
		// next step DONE only out of DONE state
		else if ( e_step_next == UNICOM_INSTRUCTION_STEP_DONE && e_state != UNICOM_INSTRUCTION_STATE_DONE )
			unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_DONE );

		if ( b_going_up ) 
			return UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_UP;
			
		return UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_DOWN;
	} 
	
	return UNICOM_INSTRUCTION_STEP_RESULT_OK;
}

void unicom_instruction_step_set( struct unicom_execution_context_s *ps_context, UNICOM_INSTRUCTION_STEP e_step )
{
	// not possible to make step again
	if ( ps_context->e_instruction_step == e_step )
		return;


	ps_context->e_instruction_step = e_step;
}


UNICOM_PROCESSING_RESULT unicom_instruction_done( UNICOM ps, bool b_error )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	UNICOM_PROCESSING_RESULT e_result = UNICOM_PROCESSING_NONE;

	if ( ps_context->ps_instruction == NULL )
		return UNICOM_PROCESSING_NONE;
	
	if ( unicom_instruction_state_get( ps_context ) == UNICOM_INSTRUCTION_STATE_IDLE )
		return UNICOM_PROCESSING_NONE;
	
	unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_IDLE );

	unicom_instruction_step_set( ps_context, b_error ? UNICOM_INSTRUCTION_STEP_ERROR : UNICOM_INSTRUCTION_STEP_DONE );

	// call instruction processing for error state
	switch ( unicom_instruction_step( ps, NULL ) )
	{
		case UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN:
			e_result = UNICOM_PROCESSING_NONE;
			break;

		case UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_UP:
		case UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_DOWN:
			e_result = UNICOM_PROCESSING_RUN;
			break;
		default:
			e_result = UNICOM_PROCESSING_DONE;
			break;
		break;
	}
	unicom_instruction_response_key_clear( ps_context  );

	unicom_event( ps, b_error ? UNICOM_EVENT_INSTRUCTION_ERROR : UNICOM_EVENT_INSTRUCTION_DONE );
	
	return e_result;
}

UNICOM_LEXEM unicom_response_top( UNICOM ps )
{
	UNICOM_LEXEM ps_lexem = ps->ps_response_lexem;
	return ps_lexem;
}

void unicom_instruction_response_key_clear( UNICOM_EXECUTION_CONTEXT ps )
{
	ps->p_response_key = NULL;
}

void unicom_instruction_response_key_set( UNICOM_EXECUTION_CONTEXT ps, void *p_response_key )
{
	ps->p_response_key = p_response_key;
}

void* unicom_instruction_response_key( UNICOM_EXECUTION_CONTEXT ps )
{
	return ps->p_response_key;
}





void unicom_response_pop( UNICOM ps, bool b_explicit )
{

	bool b_keep_flag = unicom_flag_get( ps, UNICOM_FLAG_KEEP_LEXEM );

	if ( ps->ps_response_lexem == NULL )
	{
		unicom_flag_clear( ps, UNICOM_FLAG_KEEP_LEXEM );
		return;	
	}
	
	
	// clear flag
	if ( b_keep_flag )
		unicom_flag_clear( ps, UNICOM_FLAG_KEEP_LEXEM );

	// keep flag ? return
	if ( b_keep_flag && !b_explicit )
	{
		unicom_event( ps, UNICOM_EVENT_LEXEM_PROCESSING_AGAIN );
		return;
	}

	// no keep flag || explicit pop
	ps->ps_response_lexem = NULL;

	unicom_event( ps, UNICOM_EVENT_LEXEM_PROCESSED );

}

void unicom_response_set( UNICOM ps, UNICOM_LEXEM ps_lexem )
{
	ps->ps_response_lexem = ps_lexem;

	if ( ps_lexem != NULL )
		unicom_event( ps, UNICOM_EVENT_LEXEM_PROCESSING );
}

bool unicom_specific_event( UNICOM ps, const unicom_specific_t *ps_specific, UNICOM_SPECIFIC_EVENT e_event )
{
	if ( ps_specific == NULL )
		return true; // processed ok

	if ( ps_specific->f_specific_event == NULL )
		return true; // ok

	return ps_specific->f_specific_event( ps->p_app_object, e_event );
}

bool unicom_flow_process( UNICOM ps, const unicom_specific_t *ps_specific, UNICOM_LEXEM ps_lexem )
{
	if ( ps_specific == NULL )
		return true; // processed ok

	if ( ps_specific->f_flow == NULL )
		return true; // ok

	return ps_specific->f_flow( ps->p_app_object, ps_lexem );
}

void unicom_error( UNICOM ps )
{
	UNICOM_STATE e_state = unicom_state( ps );

	// retrieve error and clear them
	ps->e_error_last = unicom_error_get();
	unicom_error_clear();

	// processor not ready
	if ( e_state == UNICOM_STATE_IDLE )
		return;

	if ( unicom_flag_internal_get( ps, UNICOM_FLAG_INTERNAL_ERROR ) )
		return;
	
	unicom_flag_internal_set( ps, UNICOM_FLAG_INTERNAL_ERROR );

	if ( !unicom_event( ps, UNICOM_EVENT_ERROR ) )
		return;

	unicom_state_set( ps, UNICOM_STATE_ERROR );
}


bool unicom_instruction_response_accept( UNICOM ps, UNICOM_EXECUTION_CONTEXT ps_context, UNICOM_LEXEM ps_lexem )
{
	void *p_response_object = unicom_lexem_object( ps_lexem );

	if ( unicom_instruction_response_key( ps_context ) != NULL )
	{
		if ( !ps->ps_settings->f_response_key_pass( unicom_instruction_response_key( ps_context ), p_response_object ) )
			return false;

		// response object acceptable now!
		unicom_instruction_response_key_clear( ps_context );

	}

	// inform about response passing into thread
	unicom_response_set( ps, ps_lexem );

	return true;
}

bool unicom_response_accept( UNICOM ps, UNICOM_LEXEM ps_lexem )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;

	return unicom_instruction_response_accept( ps, ps_context, ps_lexem );
}


bool unicom_state_running( UNICOM ps )
{
	UNICOM_STATE e = unicom_state( ps );
	if ( e == UNICOM_STATE_RUN_PROCESSING || e == UNICOM_STATE_RUN )
		return true;

	return false;
	
}

bool unicom_thread_interruptable( UNICOM_THREAD ps )
{
	// no thread, no interruption
	if ( ps == NULL )
		return false;

	return unicom_thread_flag_get( ps, UNICOM_THREAD_FLAG_INTERRUPTABLE );
}

void unicom_response( UNICOM ps, struct unicom_lexem_s *ps_lexem )
{
	UNICOM_THREAD ps_thread_check;
	bool b_some_thread_interruptable = false;
	bool b_active_thread_non_interruptable = false;
	unicom_activity_t s_activity;

	bool b_result;

	// nothing to process?
	if ( ps_lexem == NULL )
		return;
	
	// check running processor
	if ( unicom_state( ps ) == UNICOM_STATE_IDLE )
		return;



	unicom_activity_store( ps, &s_activity ); 
	
	// inform about passing lexem into flows	
	unicom_response_set( ps, ps_lexem );
	b_result = unicom_event( ps, UNICOM_EVENT_RESPONSE_LEXEM );
	unicom_activity_restore( ps, &s_activity ); 

	if ( !b_result )
		goto lbl_finish;
		
	// processor running, put it to flow mechanisms ( to generic and selected specific )
	// at first to specific because generic can cause some specific context switch after procesing lexem
	if ( !unicom_flow_process( ps, ps->ps_specific, ps_lexem ) )
		UNICOM_ERROR_GOTO( UNICOM_ERROR_FLOW_SPECIFIC );
	//
	if ( !unicom_flow_process( ps, ps->ps_generic, ps_lexem ) )
		UNICOM_ERROR_GOTO( UNICOM_ERROR_FLOW_GENERIC );

	ps_thread_check = unicom_thread_first( ps );
	while ( ps_thread_check != NULL )
	{
		if ( unicom_thread_interruptable( ps_thread_check ) )
		{
			b_some_thread_interruptable = true;
			break;
		}
		ps_thread_check = unicom_thread_next( ps, ps_thread_check );
	}

	b_active_thread_non_interruptable = ps->ps_active_thread != NULL && !unicom_thread_flag_get( ps->ps_active_thread, UNICOM_THREAD_FLAG_INTERRUPTABLE );


	// process CONNECT / SELECT / DISCONNECT or not PARALLEL PROCESSING THREAD which can accept this response
	// if nothing interruptable perform only this one, if no active thread
	if ( !b_some_thread_interruptable || !unicom_ready( ps ) || b_active_thread_non_interruptable )
	{
		if ( !unicom_response_accept( ps, ps_lexem ) )
			goto lbl_finish;


		if ( !unicom_event( ps, UNICOM_EVENT_RESPONSE_LEXEM_PROCESSING ) )
			goto lbl_finish;

		unicom_run( ps );
		
	}

	else
	{

		UNICOM_THREAD ps_thread_original = ps->ps_active_thread;

		ps_thread_check = unicom_thread_first( ps );
		while ( ps_thread_check != NULL )
		{
			UNICOM_THREAD ps_interrupt_thread = ps->ps_active_thread;

			if ( unicom_thread_flag_get( ps_thread_check, UNICOM_THREAD_FLAG_INTERRUPTABLE ) )
			{
				// interrupt active thread
				if ( ps_interrupt_thread == NULL || unicom_thread_interrupt( ps, ps_interrupt_thread ) )
				{
					// activate check thread
					if ( unicom_thread_resume( ps, ps_thread_check ) )
					{

						if ( unicom_response_accept( ps, ps_lexem ) )
						{
							if ( !unicom_event( ps, UNICOM_EVENT_RESPONSE_LEXEM_PROCESSING ) )
								goto lbl_finish;

							unicom_run( ps );
						}
					}
				}
			}
			ps_thread_check = unicom_thread_next( ps, ps_thread_check );
		}

		// resume back to previous active thread ( to threads_try_to_start )
		if ( ps_thread_original != ps->ps_active_thread )
		{
			// interrupt active thread
			unicom_thread_interrupt( ps, ps->ps_active_thread );

			// interrupt possible original thread
			unicom_thread_resume( ps, ps_thread_original );
		}
	}

lbl_finish:

	unicom_response_set( ps, NULL ); // set to none
	return;

lbl_mdg_error:
	unicom_error( ps );
	goto lbl_finish;
}

void unicom_process( UNICOM ps )
{
	if ( unicom_state( ps ) == UNICOM_STATE_IDLE )
		return;

	// can be stopped by instruction continue feature
	if ( unicom_timer_running( &ps->s_tick_timer ) )
	{
		if ( !unicom_timer_expired( &ps->s_tick_timer ) )
			return; // waiting for TICK


	}

	// processor event
	unicom_run( ps );
}

UNICOM_PROCESSING_RESULT unicom_threads_try_to_start( UNICOM ps )
{
	UNICOM_PROCESSING_RESULT e_thread_run = UNICOM_PROCESSING_NONE;

	UNICOM_THREAD ps_thread = NULL, ps_thread_previous = NULL;

	if ( !unicom_threads_registered( ps ) )
		return e_thread_run;


	// show possibilities	
	ps_thread = unicom_thread_first( ps );
	while ( ps_thread != NULL )
	{
		if ( unicom_thread_runnable( ps_thread, false ) )
		{
			bool b_result;
			ps->ps_active_thread = ps_thread;
			b_result = unicom_event( ps, UNICOM_EVENT_THREAD_SELECTION );
			ps->ps_active_thread = NULL;

			MDG_ASSERT( false == b_result );
		}
		
		ps_thread = unicom_thread_next( ps, ps_thread );
	}


	// select next thread
	ps_thread_previous = ps->ps_last_started_thread;

	// select previous thread as first for iteration
	if ( ps_thread_previous == NULL )
		ps_thread_previous = unicom_thread_last( ps );

	ps_thread = ps_thread_previous;

	// iterate through all threads assume previous too if no other started
	do 
	{

		// try next one
		ps_thread = unicom_thread_next( ps, ps_thread );

		if ( ps_thread == NULL )
			ps_thread = unicom_thread_first( ps );
		
		e_thread_run = unicom_thread_try_to_start( ps, ps_thread );
		if ( e_thread_run == UNICOM_PROCESSING_ERROR )
			break;

		if ( e_thread_run == UNICOM_PROCESSING_DONE )
		{
			MDG_ASSERT( false == unicom_event( ps, UNICOM_EVENT_THREAD_SELECTED ) );
			ps->ps_last_started_thread = ps_thread;
			break;
		}

	} while ( ps_thread != ps_thread_previous );
	
lbl_mdg_assert:
	return e_thread_run;

}

void unicom_selection_complete( UNICOM ps )
{
	
}

void unicom_flag_clear( UNICOM ps, UNICOM_FLAG e_flag )
{
	ps->ui32_flags &= ~( (uint32_t) e_flag );
}

void unicom_flags_clear( UNICOM ps, uint32_t ui32_flags )
{
	ps->ui32_flags &= ~ui32_flags;
}

void unicom_flags_set( UNICOM ps, uint32_t ui32_flags )
{
	ps->ui32_flags |= ui32_flags;
}

bool unicom_flag_get( UNICOM ps, UNICOM_FLAG e_flag )
{
	return ps->ui32_flags & (uint32_t) e_flag;
}

bool unicom_context_flag_get( UNICOM_EXECUTION_CONTEXT ps, UNICOM_EXECUTION_CONTEXT_FLAG e_flag )
{
	return ps->ui32_flags & (uint32_t) e_flag;
}

void unicom_context_flag_set( UNICOM_EXECUTION_CONTEXT ps, UNICOM_EXECUTION_CONTEXT_FLAG e_flag )
{
	ps->ui32_flags |= (uint32_t) e_flag;
}

void unicom_context_flag_clear( UNICOM_EXECUTION_CONTEXT ps, UNICOM_FLAG_INTERNAL e_flag )
{
	ps->ui32_flags &= ~( (uint32_t) e_flag );
}


bool unicom_flag_internal_get( UNICOM ps, UNICOM_FLAG_INTERNAL e_flag )
{
	return ps->ui32_flags_internal & (uint32_t) e_flag;
}

void unicom_flag_internal_set( UNICOM ps, UNICOM_FLAG_INTERNAL e_flag )
{
	ps->ui32_flags_internal |= (uint32_t) e_flag;
}

void unicom_flag_internal_clear( UNICOM ps, UNICOM_FLAG_INTERNAL e_flag )
{
	ps->ui32_flags_internal &= ~( (uint32_t) e_flag );
}

void unicom_flag_set( UNICOM ps, UNICOM_FLAG e_flag )
{
	ps->ui32_flags |= (uint32_t) e_flag;
}

bool unicom_request_echo( UNICOM ps )
{
	return unicom_flag_get( ps, UNICOM_FLAG_ACK );
}

void unicom_request_echo_set( UNICOM ps, bool b_set )
{
	unicom_flag_set( ps, UNICOM_FLAG_ACK );
}


void unicom_instruction_request( UNICOM_EXECUTION_CONTEXT ps_context, const struct unicom_data_s s_data, bool b_use_defaults, bool b_no_ack )
{
	memcpy( &ps_context->s_request, &s_data, sizeof( unicom_data_t ) );
	ps_context->b_request_use_defaults = b_use_defaults;
	ps_context->b_acknowledgement_off = b_no_ack;
	
}

bool unicom_instruction_flag_get( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_flags_mask )
{
	if ( ( ps_context->ui32_step_flags & ui32_flags_mask ) != ui32_flags_mask )
		return false;

	return true;
}

void unicom_instruction_request_simple( UNICOM_EXECUTION_CONTEXT ps_context, const struct unicom_data_s s_data )
{
	if ( unicom_instruction_flag_get( ps_context, UNICOM_FLAG_ACK ) )
		ps_context->b_acknowledgement_off = false;
	else
		ps_context->b_acknowledgement_off = true;

	if ( unicom_data( &ps_context->ps_instruction_specific->s_request_begin, NULL ) != NULL
	  || unicom_data( &ps_context->ps_instruction_specific->s_request_end, NULL ) != NULL )
		ps_context->b_request_use_defaults = true;
	else
		ps_context->b_request_use_defaults = false;
	
	memcpy( &ps_context->s_request, &s_data, sizeof( unicom_data_t ) );
}


void unicom_instruction_timeout_stop( UNICOM_EXECUTION_CONTEXT ps_context )
{
	if ( unicom_timer_running( &ps_context->s_instruction_timer ) )
		unicom_timer_stop( &ps_context->s_instruction_timer );
}

void unicom_instruction_timeout_add( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_timeout_ms )
{
	unicom_timer_add( &ps_context->s_instruction_timer, ui32_timeout_ms );
}


void unicom_instruction_timeout_reset( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_timeout_ms )
{
	unicom_timer_reset( &ps_context->s_instruction_timer, ui32_timeout_ms );

	if ( ui32_timeout_ms == UNICOM_TIMEOUT_NONE )
		unicom_timer_stop( &ps_context->s_instruction_timer ); // ensure stop
	else
		unicom_timer_start( &ps_context->s_instruction_timer ); // ensure start
}

void unicom_instruction_timeout_restart( UNICOM_EXECUTION_CONTEXT ps_context )
{
	if ( unicom_timer_timeout( &ps_context->s_instruction_timer ) != UNICOM_TIMEOUT_NONE )
	{
		const unicom_instruction_t* ps_instruction = ps_context->ps_instruction;

		// when timeout has changed by wait calls
		unicom_timer_reset( &ps_context->s_instruction_timer, ps_instruction->ui32_timeout_ms );
	}
}

void unicom_instructions_invoke( UNICOM_EXECUTION_CONTEXT ps_context, const uint8_t *aui8_invoke, uint8_t ui8_invoke_items_count )
{
	// mark instruction as done
	unicom_instruction_return( ps_context );

	// but invoke instructions 
	ps_context->aui8_invoke_instructions = aui8_invoke;
	ps_context->ui8_invoke_instructions_count = ui8_invoke_items_count;
	ps_context->ui8_invoke_instructions_iterator = UNICOM_INSTRUCTION_ITERATOR_INIT;
}

void unicom_instruction_return( struct unicom_execution_context_s *ps_context )
{
	if ( ps_context->s_instruction_return_repeat.ui32_limit != 0 )
		unicom_instruction_wait_set( ps_context, ps_context->s_instruction_return_repeat.ui32_wait_at_return_ms );

	unicom_instruction_step_set( ps_context, UNICOM_INSTRUCTION_STEP_DONE );
}

void unicom_operation_return( UNICOM_EXECUTION_CONTEXT ps_context )
{
	unicom_instruction_return( ps_context );
	
	ps_context->ui8_operation_instructions_iterator = UNICOM_INSTRUCTION_ITERATOR_NEXT_STOP;
	ps_context->ui8_invoke_instructions_iterator = UNICOM_INSTRUCTION_ITERATOR_NEXT_STOP;
}

void unicom_instruction_repeat_setup( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_repeats_limit,  uint32_t ui32_wait_at_return_ms, uint32_t ui32_instruction_repeat_flags )
{
	ps_context->s_instruction_return_repeat.ui32_limit = ui32_repeats_limit;
	ps_context->s_instruction_return_repeat.ui32_wait_at_return_ms = ui32_wait_at_return_ms;
	ps_context->s_instruction_return_repeat.ui32_flags = ui32_instruction_repeat_flags;
}

void unicom_instruction_return_repeat( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_repeats_limit, uint32_t ui32_wait_before_return_ms )
{
	unicom_instruction_repeat_setup( ps_context, ui32_repeats_limit, ui32_wait_before_return_ms, UNICOM_INSTRUCTION_REPEAT_NORMAL );
	unicom_instruction_return( ps_context );
}

void unicom_instruction_return_repeat_clean( struct unicom_execution_context_s *ps_context  )
{
	memset( &ps_context->s_instruction_return_repeat, 0, sizeof( ps_context->s_instruction_return_repeat ) );
}

bool unicom_instruction_enter( UNICOM ps )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	const unicom_instruction_t* ps_instruction = ps_context->ps_instruction;

	// initiate timer
	unicom_timer_init( &ps_context->s_instruction_timer, ps_instruction->ui32_timeout_ms );

	// start timer if timeout not NONE
	if ( ps_instruction->ui32_timeout_ms != UNICOM_TIMEOUT_NONE )
		unicom_timer_start( &ps_context->s_instruction_timer );

	unicom_context_flag_clear( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE );
		// initialize instruction
	unicom_flag_set( ps, UNICOM_FLAG_FINALIZE );

	// associated request to instruction ? send it
	if ( unicom_data_present( &ps_instruction->s_request ) )
	{
		unicom_instruction_request( ps_context, ps_instruction->s_request, true, false );
		unicom_instruction_request_send( ps );
	}

	return true;
		
}

bool unicom_instruction_try_to_finalize( UNICOM ps, const unicom_instruction_t* ps_instruction )
{
	UNICOM_LEXEM ps_lexem = unicom_response_top( ps );

	if ( ps_lexem == NULL )
		return false;

	if ( unicom_flag_get( ps, UNICOM_FLAG_FINALIZE ) == false )
		return false;

	// ignore non finalizing lexems
	if ( ps_instruction->ui32_finalizing_lexem_id == UNICOM_LEXEM_NONE 
	  || ( ps_instruction->ui32_finalizing_lexem_id != unicom_lexem_id( ps_lexem ) ) )
		return false;

	unicom_response_pop( ps, true );
	return true;
}

void unicom_instruction_step_continue_stop( UNICOM_EXECUTION_CONTEXT ps_context )
{
	unicom_timer_stop( &ps_context->s_continue_timer );
	

	if ( ps_context->ui32_continue_count > 0 )
		ps_context->ui32_continue_count = 0;
}

bool unicom_instruction_step_continue_count( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_count )
{
	if ( ps_context->ui32_continue_count > 0 )
		return false;

	ps_context->ui32_continue_count = ui32_count;
	ps_context->ui32_continue_counter = 0;
	return true;
}

bool unicom_instruction_step_continue( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_timeout_ms )
{
	if ( unicom_timer_running( &ps_context->s_continue_timer ) )
		return false;

	unicom_timer_init( &ps_context->s_continue_timer, ui32_timeout_ms );
	unicom_timer_start( &ps_context->s_continue_timer );

	return true;
}


void unicom_instruction_step_next_wait( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_wait_ms  )
{
	unicom_instruction_step_next( ps_context );

	unicom_instruction_wait_set( ps_context, ui32_wait_ms );
}

void unicom_instruction_step_prev( struct unicom_execution_context_s *ps_context )
{
	uint8_t ui8_step = (uint8_t)unicom_instruction_step_get( ps_context );

	switch ( ui8_step )
	{
		case UNICOM_INSTRUCTION_STEP_REPEAT:
		case UNICOM_INSTRUCTION_STEP_INIT:
		case UNICOM_INSTRUCTION_STEP_DONE:
		case UNICOM_INSTRUCTION_STEP_TIMEOUT:
		case UNICOM_INSTRUCTION_STEP_ERROR:
		case UNICOM_INSTRUCTION_STEP_1:
			return;
	}

	ui8_step--; // go to prev step

	unicom_instruction_step_set( ps_context, (UNICOM_INSTRUCTION_STEP) ui8_step );
}

void unicom_instruction_step_next( struct unicom_execution_context_s *ps_context )
{
	uint8_t ui8_step = (uint8_t)unicom_instruction_step_get( ps_context );

	if ( ui8_step == UNICOM_INSTRUCTION_STEP_DONE )
		return;
	
	// REPEAT | INIT => STEP_1
	if ( ui8_step == UNICOM_INSTRUCTION_STEP_REPEAT || ui8_step == UNICOM_INSTRUCTION_STEP_INIT ) 
		ui8_step = UNICOM_INSTRUCTION_STEP_1;
	// ERROR -> DONE
	else if ( ui8_step == (uint8_t) UNICOM_INSTRUCTION_STEP_ERROR 
	  || ui8_step == (uint8_t) UNICOM_INSTRUCTION_STEP_TIMEOUT )
		ui8_step = UNICOM_INSTRUCTION_STEP_DONE; 
	else
		ui8_step++; // go to next step

	unicom_instruction_step_set( ps_context, (UNICOM_INSTRUCTION_STEP) ui8_step );
}

void unicom_operation_stop( UNICOM ps, bool b_error, UNICOM_THREAD ps_thread )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	void *p_app_object = NULL;

	if ( ps_thread == NULL )
	{
		// if running instruction stop it
		if ( ps->ps_active_operation == NULL )
			return;

		ps->ps_active_operation = NULL;
	}
	else
	{
		// only for running threads
		if ( unicom_thread_state( ps_thread ) == UNICOM_THREAD_STATE_PREPARING || unicom_thread_state( ps_thread ) == UNICOM_THREAD_STATE_IDLE )
			return; 

		// work directly with thread interrupted context	
		// interrupt thread if not yet, to ensure context is valid
		if ( unicom_thread_interrupt( ps, ps_thread ) )
			ps_context = &ps_thread->s_context_interrupt.s_context;
	
	}

	unicom_instruction_wait_stop( ps_context );
	unicom_timer_stop( &ps_context->s_operation_wait_timer );

	if ( unicom_instruction_state_get( ps_context ) != UNICOM_INSTRUCTION_STATE_IDLE )
	{

		// perform STEP done
		unicom_instruction_step_internal( ps, ps_thread, ps_context, p_app_object, b_error ? UNICOM_INSTRUCTION_STEP_ERROR : UNICOM_INSTRUCTION_STEP_DONE, NULL );

		// forced STATE_IDLE after performing step
		unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_IDLE );
	}
}


bool unicom_instruction_acking( UNICOM ps, UNICOM_EXECUTION_CONTEXT ps_context, bool *pb_skip_lexem )
{
	bool b_acking = false;
	*pb_skip_lexem = false;

	// check waiting for instruction acknowledgement 
	if ( unicom_context_flag_get( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE ) )
	{
		switch ( unicom_lexem_id( unicom_response_top( ps ) ) )
		{
			case UNICOM_LEXEM_ACK:
				if ( !unicom_event( ps, UNICOM_EVENT_ACK_ARRIVED ) )
				{
					*pb_skip_lexem = true;
					break;
				}

				unicom_context_flag_clear( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE );

				if ( unicom_flag_get( ps, UNICOM_FLAG_ACK_PROCESSING_OMITTED ) )
				{
					*pb_skip_lexem = true;
					break;
				}
				
				// perform STEP immediatelly to react on ACK
				break;

			// errors are forwarded to step even if ACK required ( ##2: case when ERROR recognising off by operation ) ( see ##1 )
			case UNICOM_LEXEM_ERROR:
				// ERROR is always processed ! even in ACK required
				break;

			default:
				b_acking = true; // acking in process
				*pb_skip_lexem = true; // this lexem will be not processed
				break;
		}
	}
	else
	{
		// skip ACK if ACK is incomming and not passing to this instance
		if ( unicom_lexem_id( unicom_response_top( ps ) ) == UNICOM_LEXEM_ACK && unicom_flag_get( ps, UNICOM_FLAG_ACK ) && unicom_flag_get( ps, UNICOM_FLAG_ACK_PROCESSING_OMITTED ) )
			*pb_skip_lexem = true;
	
	}

	if ( *pb_skip_lexem )
		unicom_response_pop( ps, true );

	return b_acking;
	
}


UNICOM_PROCESSING_RESULT unicom_instruction_run( UNICOM ps )
{
	bool b_skip;
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	const unicom_instruction_t* ps_instruction = ps_context->ps_instruction;
	bool b_skip_waiting = false;

	UNICOM_PROCESSING_RESULT e_result = UNICOM_PROCESSING_RUN; // it runs at default

	if ( ps_instruction == NULL )
	{
		// STATE_DONE can be performed without known instruction ( has been finished from outside)	
		if ( ps_context->e_instruction_state == UNICOM_INSTRUCTION_STATE_DONE ) 
			return UNICOM_PROCESSING_DONE;

		return UNICOM_PROCESSING_NONE;
	}

lbl_process_again:

	unicom_instruction_process_event( ps, false );
	b_skip_waiting = false;

	if ( unicom_timer_running( &ps_context->s_continue_timer ) )
		b_skip_waiting = true;
	else if ( ps_context->ui32_continue_count != 0 )
		b_skip_waiting = true;
	else if ( unicom_response_top( ps ) != NULL )
		b_skip_waiting = true;

	if ( !b_skip_waiting && unicom_timer_running( &ps_context->s_instruction_wait_timer ) )
	{
		// if some response, no wait timer checking
		if ( !unicom_timer_expired( &ps_context->s_instruction_wait_timer ) )
		{
//			MDG_TRACE( MDG_LEVEL_DEBUG, "instruction timeout %i", unicom_timer_time_to_expire_ms( &ps_context->s_instruction_wait_timer ) );
			return UNICOM_PROCESSING_RUN;		// is running but waiting
		}

		// add difference of waiting to instruction timeout

		unicom_instruction_wait_stop( ps_context );
	}

	// if waiting, add difference of waiting to instruction timeout

	switch ( ps_context->e_instruction_state )
	{
		case UNICOM_INSTRUCTION_STATE_INIT:
			// initiate repeat counter	
			ps_context->ui8_instruction_repeat = 0;

			if ( !unicom_instruction_enter( ps ) )
			{
				unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_ERROR );
				goto lbl_process_again;
			}
			else
			{
				UNICOM_INSTRUCTION_STEP_RESULT e_ires;
				bool b_do_not_process_again = false;

				unicom_instruction_step_set( ps_context, UNICOM_INSTRUCTION_STEP_INIT );	
				e_ires = unicom_instruction_step( ps, NULL );
				switch ( e_ires )
				{
					case UNICOM_INSTRUCTION_STEP_RESULT_ERROR:
						break;

					case UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN:
						e_result = UNICOM_PROCESSING_NONE;
						b_do_not_process_again	= true;
						break;

					case UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_DOWN:
					case UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_UP:
						break;

					case UNICOM_INSTRUCTION_STEP_RESULT_NONE:
					case UNICOM_INSTRUCTION_STEP_RESULT_OK:
						unicom_instruction_step_set( ps_context, UNICOM_INSTRUCTION_STEP_1 );
						unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_PROCESSING );
						break;
				}

				if ( b_do_not_process_again )
					break;

				// otherwise process next STEP immediatelly
				goto lbl_process_again;
				
			}
			break;


		case UNICOM_INSTRUCTION_STATE_TIMEOUT:

			if ( !unicom_event( ps, UNICOM_EVENT_INSTRUCTION_TIMEOUT ) )
				break;

			if ( ps_context->ui8_instruction_repeat == ps_instruction->ui32_error_repeats )
			{
				unicom_instruction_step_set( ps_context, UNICOM_INSTRUCTION_STEP_TIMEOUT );

				switch ( unicom_instruction_step( ps, NULL ) )
				{
					case UNICOM_INSTRUCTION_STEP_RESULT_ERROR:
						goto lbl_process_again;

					case UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN:
						e_result = UNICOM_PROCESSING_NONE;
						break;


					default:
	
						// instruction_step could change error state
						if ( ps_context->e_instruction_state != UNICOM_INSTRUCTION_STATE_TIMEOUT )
						{
							ps_context->ui8_instruction_repeat = 0; // reset repeat counter
							goto lbl_process_again; // process next state
						}
				
						unicom_error_set( UNICOM_ERROR_INSTRUCTION_REPEATS_MAX );
						unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_ERROR );
						goto lbl_process_again;
				}
			}

			// repeat processing
			ps_context->ui8_instruction_repeat++;
			
			// repeat step
			unicom_instruction_step_set( ps_context, UNICOM_INSTRUCTION_STEP_REPEAT );	

			switch ( unicom_instruction_step( ps, NULL ) )
			{
				case UNICOM_INSTRUCTION_STEP_RESULT_ERROR:
					goto lbl_process_again;

				case UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN:
					e_result = UNICOM_PROCESSING_NONE;
					break;

				default:
					// enter again
					if ( !unicom_instruction_enter( ps ) )
					{
						unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_ERROR );
						goto lbl_process_again;
					}

					
					// if not changed go to step_1
					if ( unicom_instruction_step_get( ps_context ) == UNICOM_INSTRUCTION_STEP_REPEAT )
						unicom_instruction_step_set( ps_context, UNICOM_INSTRUCTION_STEP_1 );


					unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_PROCESSING );
					goto lbl_process_again; // after timeout, restart immediatelly
			}
			break;
					
			

		case UNICOM_INSTRUCTION_STATE_ERROR:

			switch ( unicom_instruction_done( ps, true ) )
			{
				case UNICOM_PROCESSING_RUN:
					goto lbl_process_again;
				default:
					break;
			}

			UNICOM_ERROR_GOTO( UNICOM_ERROR_INSTRUCTION_ERROR );
			break;


		case UNICOM_INSTRUCTION_STATE_PROCESSING:
			// processing timer expired?
			if ( unicom_timer_expired( &ps_context->s_instruction_timer ) ) // if instruction timer runs check them
			{
				unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_TIMEOUT );
				goto lbl_process_again; // process error state
			}
			// ERROR lexem ? => error ( ##1: case when ERROR recognising on by operation )
			if ( ps_instruction->ui32_error_lexem_id != UNICOM_LEXEM_NONE && unicom_lexem_id( unicom_response_top( ps ) ) == ps_instruction->ui32_error_lexem_id )
			{
				unicom_response_pop( ps, true );
				unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_ERROR );
				goto lbl_process_again;
			}

			if ( unicom_instruction_acking( ps, ps_context, &b_skip ) )
				goto lbl_awaiting_ack;
	
			if ( b_skip )
				goto lbl_response_skipped;


			if ( unicom_instruction_try_to_finalize( ps, ps_instruction ) )
			{
				unicom_instruction_state_set( ps_context, UNICOM_INSTRUCTION_STATE_DONE );
				goto lbl_process_again;
			}
			else
			{
				UNICOM_INSTRUCTION_STEP_RESULT e;

				// call instruction processing ( for tick event and so )
				// if step changes, keep on mind KEEP LEXEM
				e = unicom_instruction_step( ps, unicom_response_top( ps ) );
				unicom_response_pop( ps, false );

				
				switch ( e )
				{
					case UNICOM_INSTRUCTION_STEP_RESULT_ERROR:
						goto lbl_process_again;

					case UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_DOWN:
					case UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_UP:
						unicom_instruction_step_continue_stop( ps_context ); // restart steps continue processing
						unicom_tick_timer_restart( ps );			
	
						// if tick timer running do not perform batched
						//if ( unicom_timer_running( &ps->s_tick_timer ) )
						//	break;

/*						// change DOWN is batched only when forced
						if ( e == UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_DOWN
						  && !unicom_flag_get( ps, UNICOM_FLAG_FORCED_STEPS_BATCHING ) )
							break;*/

						// awaiting ACK stops batching too
						if ( unicom_context_flag_get( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE ) )
							break;
							
						goto lbl_process_again; // when step changes, always process again

					case UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN:
						e_result = UNICOM_PROCESSING_NONE;
						break;

					case UNICOM_INSTRUCTION_STEP_RESULT_NONE:
					case UNICOM_INSTRUCTION_STEP_RESULT_OK:
					  {
						bool b_step_continues = false;

						// check continue timer & continue count
						if ( unicom_timer_running( &ps_context->s_continue_timer ) && !unicom_timer_expired( &ps_context->s_continue_timer ) )
							b_step_continues = true;

						// check continue count
						if ( ps_context->ui32_continue_counter < ps_context->ui32_continue_count  )
						{
							ps_context->ui32_continue_counter++;
							b_step_continues = true;
						}
			
						if ( b_step_continues )
						{
							// tick is temporarily stopped during continue
							unicom_tick_timer_stop( ps );

							// IF not set ACK_RESPONSE perform immediatelly
							if ( !unicom_context_flag_get( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE ) )
								goto lbl_process_again;
							// otherwise break and process immediatelly when possible
							// + stop tick to perform immediatelly
							break;
						}

						// stop if not yet stopped
						unicom_instruction_step_continue_stop( ps_context );
						unicom_tick_timer_restart( ps );			
						break;
					  }
				}
			}	
			break;

		case UNICOM_INSTRUCTION_STATE_DONE:
			// wait if ACK had been requested
			if ( unicom_instruction_acking( ps, ps_context, &b_skip ) )
				break;
			
			// instruction finished - send status DONE to caller
			
			unicom_instruction_done( ps, false );
			e_result = UNICOM_PROCESSING_DONE;
			break;

		case UNICOM_INSTRUCTION_STATE_IDLE: 
			break;
	}

	
lbl_response_skipped:
lbl_awaiting_ack:

	return e_result;

lbl_mdg_error:
	return UNICOM_PROCESSING_ERROR;
}

bool unicom_thread_resume( UNICOM ps, UNICOM_THREAD ps_thread )
{
	if ( ps_thread == NULL )
		return false;

	// not possible when not interrupted
	if ( unicom_thread_state( ps_thread ) != UNICOM_THREAD_STATE_INTERRUPT )
		return false;

	// deactivate possible active thread
	unicom_thread_interrupt( ps, ps->ps_active_thread );

	// select thread and turn it to RUNNING state
	MDG_ASSERT( false == unicom_thread_operation_selection( ps, ps_thread, false ) );

	MDG_ASSERT( ps_thread != ps->ps_active_thread ); // must be active thread

	unicom_thread_state_set( ps_thread, UNICOM_THREAD_STATE_RUNNING );

	unicom_state_check_set( ps, UNICOM_STATE_RUN, UNICOM_STATE_RUN_PROCESSING );
	return true;

lbl_mdg_assert:
	return false;
}


bool unicom_thread_interrupt( UNICOM ps, UNICOM_THREAD ps_thread )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;

	if ( ps_thread == NULL )
		return false;

	switch ( unicom_thread_state( ps_thread ) )
	{
		case UNICOM_THREAD_STATE_INTERRUPT:
			return true;

		case UNICOM_THREAD_STATE_PREPARING:
		case UNICOM_THREAD_STATE_IDLE:
			return false;
		default:
			break; // interrupt
	}

	// must be active thread!
	MDG_ASSERT( ps_thread != ps->ps_active_thread );

	memcpy( &ps_thread->s_context_interrupt.s_context, ps_context, sizeof( unicom_execution_context_t ) );

	// thread interrupt
	unicom_thread_state_set( ps_thread, UNICOM_THREAD_STATE_INTERRUPT );

	unicom_event( ps, UNICOM_EVENT_INTERRUPT );

	unicom_activity_clear( ps );
	unicom_state_check_set( ps, UNICOM_STATE_RUN_PROCESSING, UNICOM_STATE_RUN );
	return true;

lbl_mdg_assert:
	return false;

}


void unicom_thread_interrupt_timer_start( UNICOM ps )
{
	unicom_timer_start( &ps->s_thread_interrupt_timer );
}

bool unicom_interrupt( UNICOM ps, bool b_forced_interrupt, bool b_thread_repeat )
{


	UNICOM_THREAD ps_thread = ps->ps_active_thread;

	if ( !unicom_state_running( ps ) )
		return false;

	if ( ps_thread == NULL )
		return false;

	// ATOMIC threads are not interruptable
	if ( unicom_thread_flag_get( ps_thread, UNICOM_THREAD_FLAG_ATOMIC ) && !b_thread_repeat )
		return false;

	if ( !b_forced_interrupt && !unicom_thread_flag_get( ps_thread, UNICOM_THREAD_FLAG_INTERRUPTABLE ) )
		return false;

/*	if ( !b_thread_last_operation )
	{
		if ( unicom_timer_running( &ps->s_thread_interrupt_timer ) && !unicom_timer_expired( &ps->s_thread_interrupt_timer ) )
			return false;
	}*/

	unicom_timer_stop( &ps->s_thread_interrupt_timer );

	// interruption possible
	return unicom_thread_interrupt( ps, ps_thread );
}



UNICOM_PROCESSING_RESULT unicom_operation_run( UNICOM ps )
{
	struct unicom_execution_context_s *ps_context = &ps->s_execution_context;
	UNICOM_THREAD ps_thread = ps->ps_active_thread;
	UNICOM_PROCESSING_RESULT e_result;

lbl_process_again:

	e_result = UNICOM_PROCESSING_NONE;

	if ( ps->ps_active_operation == NULL )
		return UNICOM_PROCESSING_NONE;
	
	// no response and no wait => process
	if ( unicom_response_top( ps ) == NULL && unicom_timer_running( &ps_context->s_operation_wait_timer ) )
	{
		if ( !unicom_timer_expired( &ps_context->s_operation_wait_timer ) )
			return UNICOM_PROCESSING_RUN;		// is running but waiting

		unicom_timer_stop( &ps_context->s_operation_wait_timer );
	}

	switch ( unicom_instruction_run( ps ) )
	{
		case UNICOM_PROCESSING_NONE:
			break;

		case UNICOM_PROCESSING_RUN:
			// operation interrupt at now?
			if ( unicom_interrupt( ps, false, false ) )
			{
				e_result = UNICOM_PROCESSING_DONE;
				break;
			}
			e_result = UNICOM_PROCESSING_RUN;
			break;

		case UNICOM_PROCESSING_ERROR:
			e_result = unicom_operation_repeat( ps, true );

			switch ( e_result )
			{
			case UNICOM_PROCESSING_ERROR:
			case UNICOM_PROCESSING_NONE:
				MDG_ERROR();

			case UNICOM_PROCESSING_RUN:
			case UNICOM_PROCESSING_DONE:
				break;
			}
			break;

		case UNICOM_PROCESSING_DONE:
			
			// no next instruction ? try to get next operation
			if ( !unicom_instruction_next( ps ) )
			{
				bool b_thread_repeat = false;

				// no next instruction, operation returns
				if ( !unicom_event( ps, UNICOM_EVENT_OPERATION_RETURN ) )
					break;

				e_result = unicom_operation_repeat( ps, false );

				switch ( e_result )
				{
				case UNICOM_PROCESSING_ERROR:
					MDG_ERROR();

				case UNICOM_PROCESSING_NONE: // no repeat
					// if no repeat, go to next thread operation if thread defined
					if ( ps_thread == NULL )
						e_result = UNICOM_PROCESSING_DONE;
					break; // nothing to do

				case UNICOM_PROCESSING_RUN:
					// next operation selected
					break;

				case UNICOM_PROCESSING_DONE:
					// next operation selected and interrupt performed
					break;
				}

				// repeated operation executed
				if ( e_result == UNICOM_PROCESSING_RUN || e_result == UNICOM_PROCESSING_DONE )
					break;

				// try to go to next operation if no repeat and no next
				if ( !unicom_thread_operation_next( ps_thread ) )
				{
					// no next operation
					// is possible to repeat ?

					// can be repeated ? if yes then don't stop them
					if ( ps_thread->ui32_repeat_count != UNICOM_REPEAT_INFINITE && ps_thread->ui32_repeat_counter == ps_thread->ui32_repeat_count )
					{
						if ( !unicom_event( ps, UNICOM_EVENT_THREAD_RETURN ) )
							break;

						// thread done
						unicom_thread_stop( ps, ps_thread, false );
						
						e_result = UNICOM_PROCESSING_DONE;
						break;
					}

					unicom_thread_repeat( ps, ps_thread );

					b_thread_repeat	= true;
					// repeat, but last operation!
				}
				
				// select thread next operation ( with instruction )
				if ( !unicom_thread_operation_selection( ps, ps_thread, false ) )
					MDG_ERROR();
				
				// if not atomic run or delay timer is running and not interrupt time running, interrupt thread 
				// FORCED INTERRUPT is used when operation finishes and is possibility to switch context to another thread ( and old interrupt time not expired yet )
				if ( unicom_interrupt( ps, true, b_thread_repeat ) )
				{
					e_result = UNICOM_PROCESSING_DONE;
					break;
				}
		
				goto lbl_process_again;	
			}
			// select operation instruction
			if ( !unicom_instruction_selection( ps ) )
				MDG_ERROR();

			// instruction processing immediatelly after selection
			goto lbl_process_again; // process again - init instruction
	}

	if ( e_result == UNICOM_PROCESSING_DONE )
		unicom_activity_clear( ps );

	return e_result;

lbl_mdg_error:

	unicom_event( ps, UNICOM_EVENT_OPERATION_ERROR );
	return UNICOM_PROCESSING_ERROR;
}


void unicom_stop( UNICOM ps, bool b_immediatelly )
{
	UNICOM_THREAD ps_thread;
	if ( unicom_state( ps ) == UNICOM_STATE_IDLE )
		return;
	
	if ( !b_immediatelly )
	{
		switch ( unicom_state( ps ) )
		{
			case UNICOM_STATE_DISCONNECT:
			case UNICOM_STATE_DISCONNECT_PROCESSING:
			case UNICOM_STATE_ERROR:
			case UNICOM_STATE_ERROR_PROCESSING:
				// do not process again in these states 
				return;
			default:
				break;
		}
	}

	// stop non thread operation ( out of running state )
	if ( !unicom_state_running( ps ) )
		unicom_operation_stop( ps, true, NULL ); // imm == error, do not lead to ERROR state
	// stop all threads
	ps_thread = unicom_thread_first( ps );
	while ( ps_thread != NULL )
	{
		unicom_thread_stop( ps, ps_thread, true );

		ps_thread = unicom_thread_next( ps, ps_thread  );
	}
	

	// immediatelly - stop IT!
	if ( b_immediatelly )
	{
		// destroy specifics
		unicom_specific_event( ps, ps->ps_specific, UNICOM_SPECIFIC_EVENT_DONE );

		unicom_specific_event( ps, ps->ps_generic, UNICOM_SPECIFIC_EVENT_DONE );

		unicom_state_set( ps, UNICOM_STATE_IDLE );
		unicom_event( ps, UNICOM_EVENT_STOPPED );
		return;
	}

	unicom_state_set( ps, UNICOM_STATE_DISCONNECT );
}

bool unicom_execute_stop( UNICOM ps, uint32_t ui32_thread_id, bool b_error )
{
	UNICOM_THREAD ps_thread;

	if ( !unicom_started( ps ) )
		return false;

	ps_thread = unicom_thread_get( ps, ui32_thread_id );	

	if ( ps_thread == NULL )
		return false;

	if ( !unicom_thread_stop( ps, ps_thread, b_error ) ) 
		return true;

	return true; 	
}

bool unicom_specific_select_pointer( UNICOM ps )
{
	UNICOM_SETTINGS ps_settings = ps->ps_settings;
	const struct unicom_specific_s** pps_specific = ps->pps_specific_pointer;
	
	if ( ps->pps_specific_pointer == NULL )
		goto lbl_deselect;

	if ( *ps->pps_specific_pointer == ps->ps_specific )
		return false;

	if ( pps_specific >= ps_settings->aps_specifics + ps_settings->ui32_specifics_count )
		ps->pps_specific_pointer = NULL;

	if ( ps->pps_specific_pointer == NULL )
		goto lbl_deselect;

	if ( ps->ps_specific != NULL )
		unicom_specific_event( ps, ps->ps_specific, UNICOM_SPECIFIC_EVENT_DONE );

	// try to init specific
	if ( !unicom_specific_event( ps, *ps->pps_specific_pointer, UNICOM_SPECIFIC_EVENT_INIT ) )
		return false;

	ps->ps_specific = *ps->pps_specific_pointer;
	unicom_event( ps, UNICOM_EVENT_SPECIFIC_CHANGED );
		

	return true;

lbl_deselect:
	ps->ps_specific = NULL;
	return false;
	
}


bool unicom_specific_select_first( UNICOM ps )
{
	UNICOM_SETTINGS ps_settings = ps->ps_settings;

	const struct unicom_specific_s** aps_specifics = ps_settings->aps_specifics;

	// no specifics
	if ( aps_specifics == NULL )
		return false;
	
	ps->pps_specific_pointer = aps_specifics;

	return unicom_specific_select_pointer( ps );
}

bool unicom_specific_select_next( UNICOM ps )
{
	ps->pps_specific_pointer ++;

	return unicom_specific_select_pointer( ps );

}


bool unicom_connection_selection_switch( UNICOM ps )
{
	UNICOM_STATE e_next_state = UNICOM_STATE_IDLE;

	bool b_connect_after_selection = unicom_flag_get( ps, UNICOM_FLAG_CONNECT_AFTER_SELECTION );

	switch ( unicom_state( ps ) )
	{
		case UNICOM_STATE_INIT:
			if ( b_connect_after_selection )
				e_next_state = UNICOM_STATE_SELECT_PREPARE;
			else
				e_next_state = UNICOM_STATE_CONNECT;

			break;

		case UNICOM_STATE_CONNECT:
		case UNICOM_STATE_CONNECT_PROCESSING:
			if ( b_connect_after_selection )
				e_next_state = UNICOM_STATE_RUN;
			else
				e_next_state = UNICOM_STATE_SELECT_PREPARE;
			
			break;

		case UNICOM_STATE_SELECT_PREPARE:
		case UNICOM_STATE_SELECT_PROCESSING:
			if ( b_connect_after_selection )
				e_next_state = UNICOM_STATE_CONNECT;
			else
				e_next_state = UNICOM_STATE_RUN;
			break;

		default:
			break;

	}


	if ( e_next_state == UNICOM_STATE_IDLE )
		return false;


	switch ( e_next_state )
	{
		default:
			return false;

		case UNICOM_STATE_CONNECT:
		case UNICOM_STATE_SELECT_PREPARE:
			break;

		case UNICOM_STATE_RUN:
			if ( !unicom_event( ps, UNICOM_EVENT_READY ) )
				break;
			break;
	}


	unicom_state_set( ps, e_next_state );
	return true;
}

void unicom_run( UNICOM ps )
{
	UNICOM_PROCESSING_RESULT e_result;

	unicom_timer_restart( &ps->s_tick_timer ); // restart tick timer for unicom_process call

	if ( ps->b_lock ) // not runnable hierarchically, already running
		return;


	ps->b_lock = true;

	if ( ps->e_state != UNICOM_STATE_IDLE )
	{
		// perform specific ticks
		if ( !unicom_specific_event( ps, ps->ps_specific, UNICOM_SPECIFIC_EVENT_TICK )) 
			MDG_ERROR();
			
		// perform generic ticks
		if ( !unicom_specific_event( ps, ps->ps_generic, UNICOM_SPECIFIC_EVENT_TICK ) )
			MDG_ERROR();

	}
	

lbl_process_again:

	e_result = UNICOM_PROCESSING_NONE;

	switch ( ps->e_state )
	{

		case UNICOM_STATE_IDLE:
			break;

		case UNICOM_STATE_INIT:
			if ( !unicom_connection_selection_switch( ps ) )
				break;

			goto lbl_process_again;


		case UNICOM_STATE_CONNECT:
			if ( !unicom_operation_start( ps, UNICOM_OPERATION_CONNECT, NULL, NULL, false ) )
			{
				if ( !unicom_event( ps, UNICOM_EVENT_CONNECT_SKIPPED ) )
					break;
			
				if ( !unicom_connection_selection_switch( ps ) )
					break;

				goto lbl_process_again;
			}

			unicom_state_check_set( ps, UNICOM_STATE_CONNECT, UNICOM_STATE_CONNECT_PROCESSING );
			goto lbl_process_again;


		case UNICOM_STATE_CONNECT_PROCESSING:
			e_result = unicom_operation_run( ps );
			if ( e_result == UNICOM_PROCESSING_RUN || e_result == UNICOM_PROCESSING_NONE )
				break;

			if ( e_result == UNICOM_PROCESSING_ERROR )
				break;

			if ( !unicom_event( ps, UNICOM_EVENT_CONNECTED ) )
				break;
		
			unicom_flag_internal_set( ps, UNICOM_FLAG_INTERNAL_CONNECTED );

			if ( !unicom_connection_selection_switch( ps ) )
				break;
			

			goto lbl_process_again;

		case UNICOM_STATE_SELECT_PREPARE:
			if ( !unicom_specific_select_first( ps ) )
			{
				if ( !unicom_event( ps, UNICOM_EVENT_SELECT_SKIPPED ) )
					break;

				if ( !unicom_connection_selection_switch( ps ) )
					break;

				goto lbl_process_again;
			}
			
			unicom_state_check_set( ps, UNICOM_STATE_SELECT_PREPARE, UNICOM_STATE_SELECT );
			goto lbl_process_again;

		case UNICOM_STATE_SELECT:
			if ( !unicom_operation_start( ps, UNICOM_OPERATION_SELECT, NULL, NULL, false ) )
			{
				e_result = UNICOM_PROCESSING_ERROR;
				break;
			}
			unicom_state_check_set( ps, UNICOM_STATE_SELECT, UNICOM_STATE_SELECT_PROCESSING );
			goto lbl_process_again;


		case UNICOM_STATE_SELECT_PROCESSING:
			e_result = unicom_operation_run( ps );
			if ( e_result == UNICOM_PROCESSING_RUN || e_result == UNICOM_PROCESSING_NONE )
				break; // process further

			if ( e_result == UNICOM_PROCESSING_ERROR )
			{
				unicom_state_check_set( ps, UNICOM_STATE_SELECT_PROCESSING, UNICOM_STATE_SELECT_NEXT );
				goto lbl_process_again;
			}
		

			if ( !unicom_event( ps, UNICOM_EVENT_SELECTED ) )
				break;
			
			unicom_flag_internal_set( ps, UNICOM_FLAG_INTERNAL_CONNECTED );

			if ( !unicom_connection_selection_switch( ps ) )
				break;


			goto lbl_process_again; // DONE - process again - _RUN or whatever else 

		case UNICOM_STATE_SELECT_NEXT:
			if ( !unicom_specific_select_next( ps ) )
			{
				if ( !unicom_event( ps, UNICOM_EVENT_SELECT_FAILED ) )
					break;

				e_result = UNICOM_PROCESSING_ERROR;
				break;
			}

			unicom_state_check_set( ps, UNICOM_STATE_SELECT_NEXT, UNICOM_STATE_SELECT );
			goto lbl_process_again;

		case UNICOM_STATE_RUN:
			e_result = unicom_threads_try_to_start( ps );
			if ( e_result == UNICOM_PROCESSING_DONE )
			{
				unicom_state_check_set( ps, UNICOM_STATE_RUN, UNICOM_STATE_RUN_PROCESSING );
				goto lbl_process_again;
			}
			break;


		case UNICOM_STATE_RUN_PROCESSING:
			// thread tick
			MDG_ASSERT( false == unicom_thread_tick( ps->ps_active_thread ) );

			e_result = unicom_operation_run( ps );
			switch ( e_result )
			{
				case UNICOM_PROCESSING_ERROR:
				 {
					UNICOM_THREAD ps_thread = ps->ps_active_thread;

					// thread operation ERROR, can be ommited ( resolved as thread restart - by repeat call )
					if ( unicom_thread_flag_get( ps_thread, UNICOM_THREAD_FLAG_ERROR_REPEAT ) )
					{
						unicom_thread_repeat( ps, ps_thread );
						e_result = UNICOM_PROCESSING_RUN;
						unicom_state_check_set( ps, UNICOM_STATE_RUN_PROCESSING, UNICOM_STATE_RUN );
					}
					break;
				 }


				case UNICOM_PROCESSING_DONE:
					unicom_state_check_set( ps, UNICOM_STATE_RUN_PROCESSING, UNICOM_STATE_RUN );
					break;

				default:
					break;
			}
			break;

		
		case UNICOM_STATE_DISCONNECT:
			// not connected - exit
			if ( unicom_flag_internal_get( ps, UNICOM_FLAG_INTERNAL_CONNECTED ) == false )
			{
				unicom_state_check_set( ps, UNICOM_STATE_DISCONNECT, UNICOM_STATE_STOP );
				goto lbl_process_again;
			}
			
			if ( !unicom_operation_start( ps, UNICOM_OPERATION_DISCONNECT, NULL, NULL, false ) )
			{
				unicom_state_check_set( ps, UNICOM_STATE_DISCONNECT, UNICOM_STATE_STOP );
				unicom_flag_internal_set( ps, UNICOM_FLAG_INTERNAL_DISCONNECTION_TRIED );	
				goto lbl_process_again;
			}
			else
				unicom_state_check_set( ps, UNICOM_STATE_DISCONNECT, UNICOM_STATE_DISCONNECT_PROCESSING );

			goto lbl_process_again;


		case UNICOM_STATE_DISCONNECT_PROCESSING:
			e_result = unicom_operation_run( ps );
			if ( e_result == UNICOM_PROCESSING_RUN || e_result == UNICOM_PROCESSING_NONE )
				break;

			
			if ( e_result == UNICOM_PROCESSING_ERROR )
			{
				// already tried ? stop!
				if ( unicom_flag_internal_get( ps, UNICOM_FLAG_INTERNAL_DISCONNECTION_TRIED ) )
				{
					unicom_state_check_set( ps, UNICOM_STATE_DISCONNECT_PROCESSING, UNICOM_STATE_STOP );
					MDG_ERROR();
				}

				unicom_flag_internal_set( ps, UNICOM_FLAG_INTERNAL_DISCONNECTION_TRIED );	
				
				unicom_event( ps, UNICOM_EVENT_DISCONNECTION_FAILED );

				MDG_ERROR(); // try to resolve disconnection problem
			}	
			else
			{
				// already not connected
				unicom_flag_internal_clear( ps, UNICOM_FLAG_INTERNAL_CONNECTED );
				unicom_flag_internal_clear( ps, UNICOM_FLAG_INTERNAL_DISCONNECTION_TRIED );
				
				unicom_event( ps, UNICOM_EVENT_DISCONNECTED );
			}
	
			unicom_state_check_set( ps, UNICOM_STATE_DISCONNECT_PROCESSING, UNICOM_STATE_STOP );
			goto lbl_process_again;


		case UNICOM_STATE_ERROR:
			if ( !unicom_operation_start( ps, UNICOM_OPERATION_ERROR, NULL, NULL, false ) )
			{
				if ( unicom_flag_internal_get( ps, UNICOM_FLAG_INTERNAL_CONNECTED ) )
				{
					if ( !unicom_flag_internal_get( ps, UNICOM_FLAG_INTERNAL_DISCONNECTION_TRIED ) )
					{
						unicom_state_check_set( ps, UNICOM_STATE_ERROR, UNICOM_STATE_DISCONNECT );
						goto lbl_process_again;
					}
				}
				unicom_state_check_set( ps, UNICOM_STATE_ERROR, UNICOM_STATE_STOP );
				goto lbl_process_again;
			}
			unicom_state_check_set( ps, UNICOM_STATE_ERROR, UNICOM_STATE_ERROR_PROCESSING );
			goto lbl_process_again;

		case UNICOM_STATE_ERROR_PROCESSING:
			e_result = unicom_operation_run( ps );
			if ( e_result == UNICOM_PROCESSING_RUN )
				break;

			if ( e_result == UNICOM_PROCESSING_ERROR )
			{
				unicom_event( ps, UNICOM_EVENT_ERROR_NOT_RESOLVED );
				unicom_state_check_set( ps, UNICOM_STATE_ERROR_PROCESSING, UNICOM_STATE_STOP );
				goto lbl_process_again;
			}

			unicom_event( ps, UNICOM_EVENT_ERROR_RESOLVED );
			unicom_flag_internal_clear( ps, UNICOM_FLAG_INTERNAL_ERROR );

			// if error after disconnection try, try it again
			if ( unicom_flag_internal_get( ps, UNICOM_FLAG_INTERNAL_DISCONNECTION_TRIED ) )
				unicom_state_check_set( ps, UNICOM_STATE_ERROR_PROCESSING, UNICOM_STATE_DISCONNECT );
			else if ( unicom_flag_internal_get( ps, UNICOM_FLAG_INTERNAL_CONNECTED ) )
				unicom_state_check_set( ps, UNICOM_STATE_ERROR_PROCESSING, UNICOM_STATE_RUN );
			else
				unicom_state_check_set( ps, UNICOM_STATE_ERROR_PROCESSING, UNICOM_STATE_CONNECT );
				
			goto lbl_process_again;

		case UNICOM_STATE_STOP:
			unicom_stop( ps, true );
			break;

	}

	if ( e_result == UNICOM_PROCESSING_ERROR )
		MDG_ERROR();

lbl_end:
	ps->b_lock = false;
	return;

lbl_mdg_assert:
lbl_mdg_error:
	if ( ps->ps_active_thread != NULL )
	{
		UNICOM_THREAD ps_thread = ps->ps_active_thread;

		if ( !unicom_event( ps, UNICOM_EVENT_THREAD_ERROR ) )
			goto lbl_end;
		
		unicom_thread_stop( ps, ps_thread, true );


		// if standalone, stop only this thread
		if ( unicom_thread_flag_get( ps_thread, UNICOM_THREAD_FLAG_STANDALONE ) )
			goto lbl_end;
	}
	
	unicom_error( ps );
	goto lbl_process_again;
}

bool unicom_start( UNICOM ps )
{
	if ( unicom_state( ps ) != UNICOM_STATE_IDLE )
		return false;
	
	// initialise internal flags and process structures
	ps->ps_specific = NULL;
	ps->ps_response_lexem = NULL;
	ps->ui32_flags_internal = UNICOM_FLAGS_NONE; 
	ps->ui32_flags = ps->ps_settings->ui32_flags_default;
	ps->ps_last_started_thread = NULL;
	unicom_activity_clear( ps );
		
	unicom_state_set( ps, UNICOM_STATE_INIT );

	unicom_event( ps, UNICOM_EVENT_STARTED );
	
	unicom_run( ps );

	return true;
}




bool unicom_error_present( UNICOM ps )
{
	if ( unicom_state( ps ) != UNICOM_STATE_ERROR 
	 &&  unicom_state( ps ) != UNICOM_STATE_ERROR_PROCESSING )
		return false;

	return true;	
}


/*---------------------------------------------------------------------------------------------
                                  UNICOM_ERROR
---------------------------------------------------------------------------------------------*/
 


static UNICOM_ERROR_ID ge_unicom_error;

void unicom_error_clear()
{
	ge_unicom_error = UNICOM_ERROR_NONE;
}

UNICOM_ERROR_ID unicom_error_get()
{
	return ge_unicom_error;
}

void unicom_error_set(  UNICOM_ERROR_ID e )
{
	ge_unicom_error = e;
}


/*---------------------------------------------------------------------------------------------
				  UNICOM_LEXEM
---------------------------------------------------------------------------------------------*/
bool unicom_lexem_check_string( UNICOM_LEXEM ps_lexem, uint32_t ui32_lexem_id, const char *pc_data, uint32_t ui32_position, bool b_partial )
{
	return unicom_lexem_check( ps_lexem, ui32_lexem_id, (const unsigned char*) pc_data, pc_data == NULL? 0 : strlen( pc_data ), ui32_position, b_partial );
}

bool unicom_lexem_equal_string( UNICOM_LEXEM ps_lexem, const char *pc_data, bool b_partial )
{
	// id and position match lexem
	return unicom_lexem_check( ps_lexem, ps_lexem->ui32_id, (unsigned char*)pc_data, strlen( pc_data ), ps_lexem->ui32_position, b_partial );
}


char* unicom_lexem_content_string( UNICOM_LEXEM ps_lexem )
{
	return (char*) unicom_lexem_content( ps_lexem, NULL );
}

double unicom_lexem_content_double( UNICOM_LEXEM ps_lexem )
{
	char* pc;

	pc = unicom_lexem_content_string( ps_lexem );
	if ( pc == NULL )
		return 0.0f;

	return atof( pc );
}

uint32_t unicom_lexem_content_uint32( UNICOM_LEXEM ps_lexem )
{
	char* pc;

	pc = unicom_lexem_content_string( ps_lexem );
	if ( pc == NULL )
		return 0;

	return atoi( pc );
}

bool unicom_lexem_check( struct unicom_lexem_s *ps_lexem, uint32_t ui32_lexem_id, const unsigned char *puc_data, uint32_t ui32_content_size, uint32_t ui32_position, bool b_partial )
{
	if ( ps_lexem == NULL )
		return false;

	if ( ps_lexem->ui32_id != ui32_lexem_id )
		return false;

	if ( ps_lexem->ui32_position != ui32_position )
		return false;
	if ( puc_data != NULL )
	{
		if ( b_partial )
		{
			if ( ui32_content_size > ps_lexem->ui32_size ) 	
				return false;
		}
		else if ( ui32_content_size != ps_lexem->ui32_size )
			return false;

		if ( memcmp( puc_data, ps_lexem->puc_data, ui32_content_size ) != 0 )
			return false;
	}

	return true;
}

void unicom_lexem_set_id_predefined( struct unicom_lexem_s *ps_lexem, UNICOM_LEXEM_ID e_id )
{
	ps_lexem->ui32_id = (uint32_t) e_id;
}

void unicom_lexem_set( struct unicom_lexem_s *ps_lexem, uint32_t ui32_id, unsigned char *puc_data, uint32_t ui32_size, uint32_t ui32_position )
{
	ps_lexem->puc_data = puc_data;
	ps_lexem->ui32_size = ui32_size;
	ps_lexem->ui32_position = ui32_position;
	ps_lexem->ui32_id = ui32_id;
}

uint32_t unicom_lexem_id( struct unicom_lexem_s *ps_lexem )
{
	if ( ps_lexem == NULL )
		return UNICOM_LEXEM_NONE;

	return ps_lexem->ui32_id;
}

unsigned char* unicom_lexem_content( struct unicom_lexem_s *ps_lexem, uint32_t *pui32_size )
{
	if ( ps_lexem == NULL )
		return NULL;

	if ( pui32_size != NULL )
		*pui32_size = ps_lexem->ui32_size;

	return ps_lexem->puc_data;
}

uint32_t unicom_lexem_length( struct unicom_lexem_s *ps_lexem )
{
	if ( ps_lexem == NULL )
		return 0;

	return ps_lexem->ui32_size;
}

uint32_t unicom_lexem_position( struct unicom_lexem_s *ps_lexem )
{
	if ( ps_lexem == NULL )
		return (uint32_t) -1;

	return ps_lexem->ui32_position;
}

void *unicom_lexem_object( UNICOM_LEXEM ps_lexem )
{
	if ( ps_lexem == NULL )
		return NULL;

	if ( ps_lexem->ui32_size != 0 )
		return NULL;

	return (void*)ps_lexem->puc_data;
}

void unicom_lexem_set_object( UNICOM_LEXEM ps_lexem, uint32_t ui32_id, void *p_object )
{
	unicom_lexem_set( ps_lexem, ui32_id, (unsigned char*) p_object, 0, 0 );
}

UNICOM_LEXEM unicom_lexem_object_set( UNICOM_LEXEM ps_lexem, void *p_object )
{
	unicom_lexem_set_object( ps_lexem, UNICOM_LEXEM_OBJECT, p_object );
	return ps_lexem;
}

UNICOM_LEXEM unicom_lexem_ack_set( UNICOM_LEXEM ps_lexem, void *p_object )
{
	unicom_lexem_set_object( ps_lexem, UNICOM_LEXEM_ACK, p_object );
	return ps_lexem;
}


/*---------------------------------------------------------------------------------------------
				  UNICOM_TIMER
---------------------------------------------------------------------------------------------*/

void unicom_timer_init( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_ms )
{
	ps_timer->b_running = false;
	ps_timer->b_expired = false;
	ps_timer->ui32_timeout_ms = ui32_timeout_ms;
}

void unicom_timer_init_s( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_s )
{
	unicom_timer_init( ps_timer, ui32_timeout_s * 1000 );
}


void unicom_timer_start( UNICOM_TIMER ps_timer )
{
	if ( ps_timer->ui32_timeout_ms == UNICOM_TIMEOUT_NONE )
		ps_timer->b_running = false;
	else
		ps_timer->b_running = true;

	unicom_timer_reset( ps_timer, ps_timer->ui32_timeout_ms );
}

void unicom_timer_stop( UNICOM_TIMER ps_timer )
{
	if ( ps_timer->b_running )
		ps_timer->b_running = false;
}

uint32_t unicom_timer_timeout( UNICOM_TIMER ps_timer )
{
	return ps_timer->ui32_timeout_ms;
}

uint32_t unicom_timer_time_to_expire_ms( UNICOM_TIMER ps_timer )
{
	unitimer_t s_now;
	uint32_t ui32;

	unitimer_now( &s_now );
	unitimer_diff_ms( &s_now, &ps_timer->s_systimer, &ui32 );

	return ui32;
}

bool unicom_timer_expired( UNICOM_TIMER ps_timer )
{
	if ( !ps_timer->b_running )
		return false;
	
	if ( ps_timer->b_expired )
		return true;

		
	ps_timer->b_expired = unitimer_expired_ms( &ps_timer->s_systimer, ps_timer->ui32_timeout_ms );
	return ps_timer->b_expired;
}

void unicom_timer_restart( UNICOM_TIMER ps_timer )
{
	ps_timer->b_running = true;
	unicom_timer_reset( ps_timer, ps_timer->ui32_timeout_ms );
}


bool unicom_timer_expire( UNICOM_TIMER ps_timer )
{
	if ( !ps_timer->b_running )
		return false;

	if ( !ps_timer->b_expired )
		ps_timer->b_expired = true;

	return true;
}

void unicom_timer_add( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_ms )
{
	ps_timer->ui32_timeout_ms += ui32_timeout_ms;

	if ( !unicom_timer_running( ps_timer ) )
		unicom_timer_start( ps_timer );
}

void unicom_timer_sub( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_ms )
{
	if ( ui32_timeout_ms <= ps_timer->ui32_timeout_ms )
		ps_timer->ui32_timeout_ms -= ui32_timeout_ms;
	else
		ps_timer->ui32_timeout_ms = 0;

	if ( !unicom_timer_running( ps_timer ) )
		unicom_timer_start( ps_timer );
}



void unicom_timer_reset( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_ms )
{
	bool b_started = ps_timer->b_running;

	memset( ps_timer, 0, sizeof( unicom_timer_t ) );
	ps_timer->ui32_timeout_ms = ui32_timeout_ms;

	unitimer_now( &ps_timer->s_systimer );

	if ( ui32_timeout_ms == UNICOM_TIMER_TIMEOUT_IMMEDIATELLY )
		ps_timer->b_expired = true;

	if ( b_started )
		ps_timer->b_running = true;

}

/*---------------------------------------------------------------------------------------------
				 UNICOM_REQUEST 
---------------------------------------------------------------------------------------------*/

void unicom_request( UNICOM ps, UNICOM_EVENT e_event, const struct unicom_data_s *ps_data )
{

	if ( !unicom_event( ps, e_event ) )
		return;

	if (  ps_data == NULL )
		return;

	if ( unicom_data_size( ps_data ) > 0 || unicom_data_object( ps_data ) != NULL )
	{
		ps->ps_settings->f_request( ps->p_app_object, ps_data );
	}
}


void unicom_request_begin( UNICOM ps, const struct unicom_data_s *ps_data )
{
	unicom_request( ps, UNICOM_EVENT_REQUEST_BEGIN, ps_data );
}

void unicom_request_body( UNICOM ps, const struct unicom_data_s *ps_data )
{
	unicom_request( ps, UNICOM_EVENT_REQUEST_BODY, ps_data );
}

void unicom_request_end( UNICOM ps, const struct unicom_data_s *ps_data )
{
	unicom_request( ps, UNICOM_EVENT_REQUEST_END, ps_data );
}

/*---------------------------------------------------------------------------------------------
				  UNICOM_DATA
---------------------------------------------------------------------------------------------*/

void unicom_data_set( UNICOM_DATA ps, const unsigned char *puc_data, uint32_t ui32_size )
{
	if ( ps == NULL )
		return;

	ps->puc_data = puc_data;
	ps->ui32_size = ui32_size;
}

const unsigned char *unicom_data( const unicom_data_t *ps, uint32_t *pui32_size )
{
	if ( ps == NULL )
		return NULL;

	if ( pui32_size != NULL )
		*pui32_size = ps->ui32_size;

	return ps->puc_data;
}

bool unicom_data_present( const struct unicom_data_s *ps )
{
	if ( ps == NULL )
		return false;
	return ps->puc_data != NULL;
}

uint32_t unicom_data_size( const struct unicom_data_s *ps )
{
	if ( ps == NULL )
		return 0;

	return ps->ui32_size;
}

void unicom_data_clear( UNICOM_DATA ps )
{
	ps->ui32_size = 0;
	ps->puc_data = NULL;
}

void* unicom_data_object( const struct unicom_data_s *ps )
{
	if ( ps->ui32_size != 0 )
		return NULL;

	return (void*)ps->puc_data;
}


unicom_data_t unicom_data_object_set( void *p_object )
{
	unicom_data_t s;
	s.puc_data = p_object;
	s.ui32_size = 0;
	return s;
}

unicom_data_t unicom_data_pchar( const char *pc_string, const char *pc_string_end )
{
	unicom_data_t s;
	s.puc_data = (const unsigned char*) pc_string;
	s.ui32_size = (uint32_t) ( pc_string_end - pc_string );
	return s;
}

unicom_data_t unicom_data_string( const char *pc_string )
{
	unicom_data_t s;
	s.puc_data = (const unsigned char*)pc_string;
	if ( pc_string == NULL )
		s.ui32_size = 0;
	else
		s.ui32_size = strlen( pc_string );
	return s;
}

unicom_data_t unicom_data_block( const unsigned char *puc_block, uint32_t ui32_size )
{
	unicom_data_t s;
	s.puc_data = puc_block;
	s.ui32_size = ui32_size;

	return s;
}

const char *unicom_identifier( UNICOM ps )
{
	if ( ps->ps_settings == NULL )
		return NULL;

	return ps->ps_settings->pc_identifier;
}

bool unicom_started( UNICOM ps )
{
	return unicom_state( ps ) != UNICOM_STATE_IDLE;
}

bool unicom_ready( UNICOM ps )
{
	return unicom_state( ps ) == UNICOM_STATE_RUN || unicom_state( ps ) == UNICOM_STATE_RUN_PROCESSING;
}



void unicom_instruction_object_clear( UNICOM_EXECUTION_CONTEXT ps )
{
	ps->p_instruction_object = NULL;
}

void unicom_instruction_object_set( UNICOM_EXECUTION_CONTEXT ps, void *p_object )
{
	ps->p_instruction_object = p_object;
}

void* unicom_instruction_object( UNICOM_EXECUTION_CONTEXT ps )
{
	return ps->p_instruction_object;
}


void unicom_instructions_invoke_simple( UNICOM_EXECUTION_CONTEXT ps_context, uint8_t ui8_instruction_id )
{
	ps_context->ui8_simple_instruction_invoke = ui8_instruction_id;

	unicom_instructions_invoke( ps_context, &ps_context->ui8_simple_instruction_invoke, 1 );
}

bool unicom_instruction_problem( UNICOM_EXECUTION_CONTEXT ps_context )
{
	return unicom_context_flag_get( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_ERROR_OCCURED ) || unicom_context_flag_get( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_TIMEOUT_OCCURED );
}

void unicom_instruction_stop( UNICOM_EXECUTION_CONTEXT ps_context, bool b_error )
{
	if ( b_error )
		unicom_context_flag_set( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_STOP_ERROR );
	else
		unicom_context_flag_set( ps_context, UNICOM_EXECUTION_CONTEXT_FLAG_STOP );
}

void unicom_instruction_flag_set( UNICOM_EXECUTION_CONTEXT ps_context, UNICOM_FLAG e_flag )
{
	unicom_flag_change( ps_context, e_flag, true );
}

