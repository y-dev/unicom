#ifndef __UNICOM_H__
#define __UNICOM_H__

#define UNICOM_VERSION "1.13.0."

/**
 * @brief Unified communication processor.
 * 
 * @remarks
 * Common software processor designed to support more specific communications for different targets.
 *
 * This library encapsulates those functionalities:
 * - connection to target module
 * - selection of specific target solution ( different hardwares )
 * - specifying instructions and operations for specific solutions 
 * - specifying generic instructions and operations used when no specific exists or not implemented
 * - offers error recovery
 * - easy control of what processor does by callback events
 * - simple defined entry points forwarding time and lexical units into processor
 *     1] syntax analyser entry point( analyser offering UNICOM_LEXEM/s )
 *     2] tick entry point ( giving information about time differentials to processor )
 * - easy implementation of instructions with predefined or own defined behaviour
 * - easy implementation of operations ( array of instructions used in operation )
 * - optionally possible to  control instruction flow completelly by instruction control callback ( ICCB ) 
 * - very useful is serialization of more requests
 *   - very easy usable threads to perform operations simultaneously without need of caller to control finishing last to start new one
 *   - simply when thread finishes/interrupts his job, next one enters and performs its own job
 *   - threads can perform operations atomically, but evenso interruptably ( thread settings during execution )
 *   - for interruptably threads, smallest unit which will be performed atomically is operation
 *   - it looks like EXECUTE: THREAD [ OPERATION1[ INSTRUCTION1, INSTRUCTION2, ...], OPERATION2[ INSTRUCTION5, INSTRUCTION1, INSTRUCTION15 ] ]
 * - possibility to specific processing of target behaviour flow sepparatelly from instructions flow ( FLOW vs. INSTRUCTIONS )
 *
 * Actual version you find at: https://y-dev@bitbucket.org/y-dev/unicom.git
 *
 * For more details and examples about how things are working please see very usefull UNICOM documentation. (unicom.odt)
 * 
 * @author Marek Plhak 2015, plhak.marek@gmail.com
 **/

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unitimer.h>


/**
 * @brief Auxiliary macro for registering operation into operations list
 **/
#define UNICOM_SPECIFIC_OPERATION_REGISTER( i, wait_at_enter_ms ) { i, i##_INSTRUCTIONS, sizeof( i##_INSTRUCTIONS ) / sizeof( uint32_t ), wait_at_enter_ms }
#define UNICOM_SPECIFIC_OPERATION_REGISTER_EMPTY( i ) { i, NULL, 0, 0  }
#define UNICOM_DATA_CARRAY( str ) { (unsigned char*)(str), str ? sizeof( str ) : 0 }
#define UNICOM_DATA_CSTRING( str ) { (const unsigned char*)(str), str ? sizeof( str ) - 1 : 0 }
#define UNICOM_DATA_STRING( str ) { (const unsigned char*)(str), str ? strlen( str ) : 0 }
#define UNICOM_DATA_NONE( ) { NULL, 0 }

/*---------------------------------------------------------

UNICOM enumerations

---------------------------------------------------------*/


typedef enum
{
	UNICOM_LEXEM_OBJECT = ( (uint32_t) -5 ),
	UNICOM_LEXEM_ACK    = ( (uint32_t) -4 ),
	UNICOM_LEXEM_ERROR  = ( (uint32_t) -3 ),
	UNICOM_LEXEM_NONE   = ( (uint32_t) -2 ),
	UNICOM_LEXEM_OK     = ( (uint32_t) -1 ),
	UNICOM_LEXEMS_END   = 0,
} UNICOM_LEXEM_ID;

typedef enum
{
	UNICOM_ERROR_NONE                       = 0,
	UNICOM_ERROR_NOT_ENOUGHT_SPACE          = 1,
	UNICOM_ERROR_LEXEM_UNGET_SPACE          = 2,
	UNICOM_ERROR_LEXEM_SPACE                = 3,
	UNICOM_ERROR_OPERATION_NOT_FOUND        = 4,
	UNICOM_ERROR_THREAD_ALREADY_RUNNING     = 5,
	UNICOM_ERROR_THREAD_NO_OPERATION        = 6,
	UNICOM_ERROR_OPERATION_SELECTION        = 7,
	UNICOM_ERROR_OPERATION_NOT_IMPLEMENTED  = 8,
	UNICOM_ERROR_OPERATION_NO_INSTRUCTIONS  = 9,
	UNICOM_ERROR_INSTRUCTION_SELECTION      = 10,
	UNICOM_ERROR_INSTRUCTION_INITIALISATION = 11,
	UNICOM_ERROR_INSTRUCTION_REPEATS_MAX    = 12,
	UNICOM_ERROR_INSTRUCTION_NOT_FOUND      = 13,
	UNICOM_ERROR_SENDING_REQUEST            = 14,
	UNICOM_ERROR_THREAD_UNKNOWN             = 15,
	UNICOM_ERROR_INSTRUCTION_PROCESSING     = 16,
	UNICOM_ERROR_INSTRUCTION_ERROR          = 17,
	UNICOM_ERROR_INSTRUCTION_RESTART        = 18,
	UNICOM_ERROR_FLOW_SPECIFIC              = 19,
	UNICOM_ERROR_FLOW_GENERIC               = 20,
} unicom_error_e, UNICOM_ERROR_ID;

typedef enum 
{
	UNICOM_THREAD_STATE_IDLE       = 0,
	UNICOM_THREAD_STATE_PREPARING  = 1,
	UNICOM_THREAD_STATE_RUNNING    = 2,
	UNICOM_THREAD_STATE_INTERRUPT  = 3,
} unicom_thread_state_e, UNICOM_THREAD_STATE;

typedef enum
{
	UNICOM_REPEAT_INFINITE   = -1,
	UNICOM_REPEAT_NONE       = 0,
	UNICOM_REPEAT_1          = 1,
	UNICOM_REPEAT_2          = 2,
	UNICOM_REPEAT_3          = 3,
	UNICOM_REPEAT_5          = 5,
	UNICOM_REPEAT_10         = 10,
	UNICOM_REPEAT_20         = 20,
	UNICOM_REPEAT_30         = 30,
} unicom_repeat_e, UNICOM_REPEAT;

typedef enum
{
	UNICOM_TIMEOUT_NONE         = 0,
	UNICOM_TIMEOUT_100_MS       = 100,
	UNICOM_TIMEOUT_200_MS       = 200,
	UNICOM_TIMEOUT_500_MS       = 500,
	UNICOM_TIMEOUT_1_S          = 1000,
	UNICOM_TIMEOUT_2_S          = 2000,
	UNICOM_TIMEOUT_3_S          = 3000,
	UNICOM_TIMEOUT_5_S          = 5000,
	UNICOM_TIMEOUT_10_S         = 10000,
	UNICOM_TIMEOUT_20_S         = UNICOM_TIMEOUT_10_S * 2,
	UNICOM_TIMEOUT_30_S         = UNICOM_TIMEOUT_10_S * 3,
	UNICOM_TIMEOUT_1_M          = UNICOM_TIMEOUT_10_S * 6,
	UNICOM_TIMEOUT_2_M          = UNICOM_TIMEOUT_1_M * 2,
	UNICOM_TIMEOUT_3_M          = UNICOM_TIMEOUT_1_M * 3,
	UNICOM_TIMEOUT_5_M          = UNICOM_TIMEOUT_1_M * 5,
	UNICOM_TIMEOUT_10_M         = UNICOM_TIMEOUT_1_M * 10,
	UNICOM_TIMEOUT_1_H          = UNICOM_TIMEOUT_10_M * 6,
	UNICOM_TIMEOUT_2_H          = UNICOM_TIMEOUT_1_H * 2,
	UNICOM_TIMEOUT_6_H          = UNICOM_TIMEOUT_1_H * 6,
	UNICOM_TIMEOUT_12_H         = UNICOM_TIMEOUT_1_H * 12,
	UNICOM_TIMEOUT_1_D          = UNICOM_TIMEOUT_1_H * 24,
	UNICOM_TIMEOUT_IMMEDIATELLY = ((uint32_t)-1),
} unicom_timeout_e, UNICOM_TIMEOUT;

typedef enum
{
	UNICOM_DELAY_NONE        = 0,
	UNICOM_DELAY_50_MS       = 50,
	UNICOM_DELAY_100_MS      = 100,
	UNICOM_DELAY_250_MS      = 250,
	UNICOM_DELAY_500_MS      = 500,
	UNICOM_DELAY_1_S         = 1000,
	UNICOM_DELAY_2_S         = 2000,
	UNICOM_DELAY_3_S         = 3000,
	UNICOM_DELAY_4_S         = 4000,
	UNICOM_DELAY_5_S         = 5000,
	UNICOM_DELAY_7_S         = 7000,
	UNICOM_DELAY_10_S        = 10000,
	UNICOM_DELAY_20_S        = 20000,
} unicom_delay_e, UNICOM_DELAY;

typedef enum
{
	UNICOM_WAIT_NONE        = 0,
	UNICOM_WAIT_50_MS       = 50,
	UNICOM_WAIT_100_MS      = 100,
	UNICOM_WAIT_250_MS      = 250,
	UNICOM_WAIT_500_MS      = 500,
	UNICOM_WAIT_1_S         = 1000,
	UNICOM_WAIT_2_S         = 2000,
	UNICOM_WAIT_3_S         = 3000,
	UNICOM_WAIT_4_S         = 4000,
	UNICOM_WAIT_5_S         = 5000,
	UNICOM_WAIT_7_S         = 7000,
	UNICOM_WAIT_10_S        = 10000,
	UNICOM_WAIT_20_S        = 20000,
} unicom_wait_e, UNICOM_WAIT;



typedef enum
{
	UNICOM_INSTRUCTION_STEP_INIT,
	UNICOM_INSTRUCTION_STEP_1,
	UNICOM_INSTRUCTION_STEP_2,
	UNICOM_INSTRUCTION_STEP_3,
	UNICOM_INSTRUCTION_STEP_4,
	UNICOM_INSTRUCTION_STEP_5,
	UNICOM_INSTRUCTION_STEP_6,
	UNICOM_INSTRUCTION_STEP_7,
	UNICOM_INSTRUCTION_STEP_8,
	UNICOM_INSTRUCTION_STEP_9,
	UNICOM_INSTRUCTION_STEP_10,
	UNICOM_INSTRUCTION_STEP_11,
	UNICOM_INSTRUCTION_STEP_12,
	UNICOM_INSTRUCTION_STEP_13,
	UNICOM_INSTRUCTION_STEP_14,
	UNICOM_INSTRUCTION_STEP_15,
	UNICOM_INSTRUCTION_STEP_16,
	UNICOM_INSTRUCTION_STEP_17,
	UNICOM_INSTRUCTION_STEP_18,
	UNICOM_INSTRUCTION_STEP_19,
	UNICOM_INSTRUCTION_STEP_20,
	UNICOM_INSTRUCTION_STEP_DONE,
	UNICOM_INSTRUCTION_STEP_TIMEOUT,
	UNICOM_INSTRUCTION_STEP_ERROR,
	UNICOM_INSTRUCTION_STEP_REPEAT,
} unicom_instruction_step_e, UNICOM_INSTRUCTION_STEP;

typedef enum
{ 
	UNICOM_FLAG_ACK                           = 0x0001, //!< indicates expectance of acknowledge lexem after request ( default is off )
	UNICOM_FLAG_FINALIZE                      = 0x0002, //!< indicates instruction finalizing by specified item in definition ( default is on )
	UNICOM_FLAG_KEEP_LEXEM                    = 0x0004, //!< keep lexem for further processing ( be aware of all consequences when using when steps are not changing )
	UNICOM_FLAG_ACK_PROCESSING_OMITTED        = 0x0008, //!< omit processing of ACK in STEPS ( do not send ACK into instruction step processing )
	UNICOM_FLAG_STEPS_BATCHING                = 0x0010, //!< batching steps when changing otherwise process alone during one unicom_process
	UNICOM_FLAG_REQUEST_OBJECT_RESPONSE_KEY   = 0x0020, //!< use request object as response key
	UNICOM_FLAG_FORCED_STEPS_BATCHING         = 0x0040, //!< allways batch steps when changes without waiting for process time ( means do not wait for tick / interrupt time when steps are changing) , otherwise batched are only steps in up-going sequence 
	UNICOM_FLAG_CONNECT_AFTER_SELECTION       = 0x0080, //!< connection is performed after selection or vice versa
	UNICOM_FLAGS_NONE                         = 0x0000,
} unicom_flag_e, UNICOM_FLAG;

typedef enum
{
	UNICOM_OPERATION_STATE_PROCESS,
	UNICOM_OPERATION_STATE_PROCESSED,
} unicom_operation_e, UNICOM_OPERATION_STATE;

typedef enum
{
	UNICOM_SPECIFIC_EVENT_INIT,
	UNICOM_SPECIFIC_EVENT_TICK,
	UNICOM_SPECIFIC_EVENT_DONE
} unicom_specific_event_e, UNICOM_SPECIFIC_EVENT;

typedef enum
{
	UNICOM_OPERATION_CONNECT    = (uint32_t)-4,
	UNICOM_OPERATION_SELECT     = (uint32_t)-3,
	UNICOM_OPERATION_DISCONNECT = (uint32_t)-2,
	UNICOM_OPERATION_ERROR      = (uint32_t)-1,
	// reserved 4..9
	UNICOM_OPERATIONS_END       = 0,
} unicom_operation_id_e, UNICOM_OPERATION_ID;

typedef enum
{
	UNICOM_EVENT_STARTED                = 1, //!< processor started
	UNICOM_EVENT_ERROR                  = 2, //!< some runtime error occured ( unicom_error_get and unicom_flag_get for details )
	UNICOM_EVENT_ERROR_NOT_RESOLVED     = 3, //!< error not resolved
	UNICOM_EVENT_ERROR_RESOLVED         = 4, //!< error resolved, prepared for disconnect / stop
	UNICOM_EVENT_CONNECTED              = 5, //!< connection estabilished
	UNICOM_EVENT_SELECTED               = 6, //!< specific selection performed successfully
	UNICOM_EVENT_DISCONNECTED           = 7, //!< disconnection performed
	UNICOM_EVENT_DISCONNECTION_FAILED   = 8, //!< disconnection failed ( if not in error state, it tries to resolve error state
	UNICOM_EVENT_RUN                    = 8,

	UNICOM_EVENT_STOPPED                = 10, //!< processor stopped
	UNICOM_EVENT_READY                  = 11, //!< connected and selected

	UNICOM_EVENT_REQUEST_BEGIN          = 20, //!< 
	UNICOM_EVENT_REQUEST_BODY           = 21, //!< 
	UNICOM_EVENT_REQUEST_END            = 22, //!< 
	UNICOM_EVENT_ACK_ARRIVED            = 23, //!<
	UNICOM_EVENT_RESPONSE_LEXEM         = 24, //!< lexem arrived
	UNICOM_EVENT_RESPONSE_LEXEM_PROCESSING = 25, //!< lexem processing
	UNICOM_EVENT_LEXEM_PROCESSING       = 26, //!<
	UNICOM_EVENT_LEXEM_PROCESSING_AGAIN = 27, //!<
	UNICOM_EVENT_LEXEM_PROCESSED        = 28, //!<

	UNICOM_EVENT_INSTRUCTION_DONE         = 30, //!<
	UNICOM_EVENT_INSTRUCTION_ENTER        = 31, //!<
	UNICOM_EVENT_INSTRUCTION_TIMEOUT      = 32, //!<
	UNICOM_EVENT_INSTRUCTION_ERROR        = 33, //!<
	UNICOM_EVENT_INSTRUCTION_ENTER_ERROR  = 34, //!<
	UNICOM_EVENT_INSTRUCTION_ACKNOWLEDGE_REQUIRED = 35, //!<
	UNICOM_EVENT_INSTRUCTION_NONE         = 36, //!< no instruction
	UNICOM_EVENT_INSTRUCTION_PROCESS      = 37,
	UNICOM_EVENT_INSTRUCTION_STOP         = 38,
	UNICOM_EVENT_INSTRUCTION_STOP_ERROR   = 39,


	UNICOM_EVENT_OPERATION_ENTER            = 40, //!<
	UNICOM_EVENT_OPERATION_ERROR            = 41, //!<
	UNICOM_EVENT_OPERATION_RETURN           = 42, //!<
	UNICOM_EVENT_OPERATION_ENTER_ERROR      = 43, //!<
	UNICOM_EVENT_OPERATION_ENTER_ERROR_ALLOWED  = 44, //!< not enterable, but may be missing
	UNICOM_EVENT_OPERATION_REPEAT           = 45,

	UNICOM_EVENT_THREAD_EXECUTE         = 50, //!< executing thread - allocating them
	UNICOM_EVENT_THREAD_START           = 51, //!< thread start - here starts processing of thread operations
	UNICOM_EVENT_THREAD_ERROR           = 52, //!< unexpected error
	UNICOM_EVENT_THREAD_RETURN          = 53, //!< successfull finish and return from thread flow
	UNICOM_EVENT_THREAD_STOP            = 54, //!< unexpected stop
	UNICOM_EVENT_THREAD_PREPARE         = 55, //!< thread preparation during initialisation 
	UNICOM_EVENT_THREAD_REPEAT          = 56, //!< thread repeat

	UNICOM_EVENT_THREAD_SELECTION       = 57, //!< selecting next processing thread
	UNICOM_EVENT_THREAD_SELECTED        = 58, //!< selecting next processing thread

	UNICOM_EVENT_THREAD_EXECUTE_BUSY    = 59, //!< executing already in process


	UNICOM_EVENT_INTERRUPT              = 60,
	UNICOM_EVENT_RESUME                 = 61,

	UNICOM_EVENT_SELECT_SKIPPED         = 100, //!< no specifics defined
	UNICOM_EVENT_SELECT_FAILED          = 101, //!< selection failed - no appropriate specific found
	UNICOM_EVENT_SPECIFIC_CHANGED       = 102, //!< no specifics
	UNICOM_EVENT_CONNECT_SKIPPED        = 103, //!< connection skippe

	UNICOM_EVENT_DEBUG                  = 200,
} unicom_event_e, UNICOM_EVENT;

typedef enum
{
	UNICOM_STATE_IDLE                  = 0,
	UNICOM_STATE_INIT                  = 1,

	UNICOM_STATE_CONNECT               = 5,
	UNICOM_STATE_CONNECT_PROCESSING    = 6,

	UNICOM_STATE_SELECT_PREPARE        = 10,
	UNICOM_STATE_SELECT                = 11,
	UNICOM_STATE_SELECT_PROCESSING     = 12,
	UNICOM_STATE_SELECT_NEXT           = 13,

	UNICOM_STATE_RUN                   = 15,
	UNICOM_STATE_RUN_PROCESSING        = 16,

	UNICOM_STATE_DISCONNECT            = 20,
	UNICOM_STATE_DISCONNECT_PROCESSING = 21,

	UNICOM_STATE_ERROR                 = 25,
	UNICOM_STATE_ERROR_PROCESSING      = 26,

	UNICOM_STATE_STOP                  = 30,
} UNICOM_STATE;

typedef enum
{
	UNICOM_EXECUTION_CONTEXT_FLAG_ACK_RESPONSE         = 0x0001,
	UNICOM_EXECUTION_CONTEXT_FLAG_ERROR_OCCURED        = 0x0002,
	UNICOM_EXECUTION_CONTEXT_FLAG_TIMEOUT_OCCURED      = 0x0004,
	UNICOM_EXECUTION_CONTEXT_FLAG_PROBLEM_RECOVERED    = 0x0008,
	UNICOM_EXECUTION_CONTEXT_FLAG_STOP                 = 0x0010,
	UNICOM_EXECUTION_CONTEXT_FLAG_STOP_ERROR           = 0x0020,

	UNICOM_EXECUTION_CONTEXT_FLAGS_NONE                = 0x0000,
} UNICOM_EXECUTION_CONTEXT_FLAG;

typedef enum 
{ 
	UNICOM_FLAG_INTERNAL_INITIALISED          = 0x0001, //!< initialised flag
	UNICOM_FLAG_INTERNAL_CONNECTED            = 0x0002, //!< connection instruction passed
	UNICOM_FLAG_INTERNAL_ERROR                = 0x0004, //!< error occured
	UNICOM_FLAG_INTERNAL_DISCONNECTION_TRIED  = 0x0008, //!< 
	UNICOM_FLAG_INTERNAL_SELECTED             = 0x0010, //!< selection instruction passed

	UNICOM_FLAG_INTERNALS_NONE                = 0x0000, 
} unicom_flag_internal_e, UNICOM_FLAG_INTERNAL;

typedef enum
{
	UNICOM_INSTRUCTION_STATE_IDLE       = 0,
	UNICOM_INSTRUCTION_STATE_INIT       = 1,
	UNICOM_INSTRUCTION_STATE_PROCESSING = 2,
	UNICOM_INSTRUCTION_STATE_ERROR      = 3,
	UNICOM_INSTRUCTION_STATE_TIMEOUT    = 4,
	UNICOM_INSTRUCTION_STATE_DONE       = 5,
} unicom_instruction_state_e, UNICOM_INSTRUCTION_STATE;

typedef enum
{
	UNICOM_PROCESSING_NONE   = 0, //!< none operation processed
	UNICOM_PROCESSING_RUN    = 1,  //!< some operation processed
	UNICOM_PROCESSING_DONE   = 2, //!< operation done
	UNICOM_PROCESSING_ERROR  = 3, //!< error occured
} unicom_processing_result_e, UNICOM_PROCESSING_RESULT;
 
typedef enum
{
	UNICOM_EXECUTION_EVENT_INIT   = 0, //!< execution was initiated
	UNICOM_EXECUTION_EVENT_DONE   = 1, //!< execution successfully done
	UNICOM_EXECUTION_EVENT_ERROR  = 2, //!< execution failed
} unicom_execution_event_e, UNICOM_EXECUTION_EVENT;


typedef enum
{
	UNICOM_THREAD_EVENT_START,
	UNICOM_THREAD_EVENT_TICK,
	UNICOM_THREAD_EVENT_STOP,
} UNICOM_THREAD_EVENT;

typedef enum
{
	UNICOM_INSTRUCTION_REPEAT_NORMAL          = 0x00,
	UNICOM_INSTRUCTION_REPEAT_INVOKED_ONLY    = 0x01, //!< invoked only repeats allowed
	UNICOM_INSTRUCTION_REPEAT_INVOKE_PARENT   = 0x02, //!< repeat invoke parent
} UNICOM_INSTRUCTION_REPEAT_FLAG;

/*---------------------------------------------------------

UNICOM callbacks

---------------------------------------------------------*/
struct unicom_execution_context_s;
struct unicom_lexem_s;
struct unicom_instruction_s;
struct unicom_data_s;
struct unicom_event_data_s;
struct unicom_thread_s;

typedef bool (*unicom_instruction_step_f)( struct unicom_execution_context_s* ps_context, void *p_app_object, UNICOM_INSTRUCTION_STEP e_step, struct unicom_lexem_s *ps_lexem );
typedef bool (*unicom_specific_flow_f)( void *p_app_object, struct unicom_lexem_s *ps_lexem ); 
typedef const struct unicom_instruction_s* (*unicom_specific_instruction_f)( uint32_t ui32_id );

typedef void (*unicom_execution_event_f)( void *p_app_object, UNICOM_EXECUTION_EVENT e_event );

typedef void (*unicom_request_f)( void* p_app_object, const struct unicom_data_s* ps_data );
typedef void (*unicom_event_f)( void* p_app_object, UNICOM_EVENT e_event_id, struct unicom_event_data_s *ps_event_data  );
typedef bool (*unicom_specific_event_f)( void* p_app_object, UNICOM_SPECIFIC_EVENT e_event );

typedef bool (*unicom_response_key_pass_f)( void *p_response_key, void *p_response_object );

typedef bool (*unicom_thread_event_f)( void *p_app_object, UNICOM_THREAD_EVENT e_event, struct unicom_thread_s *ps_thread );


/*---------------------------------------------------------

UNICOM structures

---------------------------------------------------------*/
typedef struct unicom_event_data_s
{
	uint32_t ui32_active_instruction_step;
	uint32_t ui32_active_instruction;
	uint32_t ui32_active_operation;
	uint32_t ui32_active_thread;
	const char *pc_target_specific_name;
//	uint32_t ui32_size;
	uint32_t ui32_flags;
	struct unicom_lexem_s *ps_lexem;
	const char *pc_identifier;
} unicom_event_data_t, *UNICOM_EVENT_DATA;

typedef struct unicom_data_s
{
	const unsigned char *puc_data;
	uint32_t ui32_size;
} unicom_data_t, *UNICOM_DATA;

typedef struct
{
	uint32_t ui32_timeout_ms;
	bool     b_expired;
	bool     b_running;

	unitimer_t s_systimer;
} unicom_timer_t, *UNICOM_TIMER;


typedef struct unicom_lexem_s
{
	uint32_t       ui32_id;

	unsigned char *puc_data;
	uint32_t       ui32_size;
	uint32_t       ui32_position;

	void          *p_lexem_object;

} unicom_lexem_t, *UNICOM_LEXEM;



typedef struct unicom_instruction_s
{
	uint32_t       ui32_id;
	unicom_data_t  s_request;
	unicom_instruction_step_f f_step;

	uint32_t       ui32_finalizing_lexem_id;
	uint32_t       ui32_error_lexem_id;
	uint32_t       ui32_timeout_ms;
	uint32_t       ui32_error_repeats;	
} unicom_instruction_t, *UNICOM_INSTRUCTION;

typedef struct
{
	uint32_t  ui32_limit;
	uint32_t  ui32_counter;
} unicom_repeat_counter_t, *UNICOM_REPEAT_COUNTER;

typedef struct
{
	uint32_t             ui32_id;
	const uint32_t      *aui32_instructions;
	uint8_t              ui8_instructions_count;
	uint32_t             ui32_operation_wait_at_enter_ms;
	uint32_t             ui32_repeats_error;
	uint32_t             ui32_repeats_ok;
} unicom_operation_t, *UNICOM_OPERATION;


typedef struct unicom_specific_s
{
	const char                        *pc_id;                 //!< identifier of specific solution
	const unicom_operation_t          *as_operations;         //!< defined operations
	uint32_t                           ui32_operations_count; 

	const unicom_instruction_t        *as_instructions;
	uint32_t                           ui32_instructions_count;

	unicom_data_t                      s_request_begin;
	unicom_data_t                      s_request_end;

	unicom_specific_flow_f             f_flow;

	unicom_specific_event_f            f_specific_event;
} unicom_specific_t, *UNICOM_SPECIFIC;

typedef struct unicom_execution_context_s 
{
	uint32_t                     ui32_flags;

	unicom_data_t                s_request;
	bool                         b_request_use_defaults;
	bool                         b_acknowledgement_off;

	uint8_t                      ui8_operation_instructions_iterator;

	UNICOM_INSTRUCTION_STEP      e_instruction_step_last_performed;
	UNICOM_INSTRUCTION_STEP      e_instruction_step;
	UNICOM_INSTRUCTION_STATE     e_instruction_state;
	const unicom_specific_t     *ps_instruction_specific;
	const unicom_instruction_t  *ps_instruction;

	uint8_t                      ui8_instruction_repeat;
	unicom_timer_t               s_instruction_timer;

//	unicom_execution_event_f     f_event;
	uint32_t                     ui32_step_flags;
	
	unicom_timer_t               s_operation_wait_timer;
	unicom_timer_t               s_instruction_wait_timer;
	unicom_timer_t               s_continue_timer;
	uint32_t                     ui32_continue_count;
	uint32_t                     ui32_continue_counter;
	
	const uint8_t               *aui8_invoke_instructions;
	uint8_t                      ui8_invoke_instructions_count;
	uint8_t                      ui8_invoke_instructions_iterator;
	uint8_t                      ui8_simple_instruction_invoke;


	void                        *p_instruction_object;
	void                        *p_response_key;
	void                        *p_request_object;

/*	struct
	{
		UNICOM_INSTRUCTION_OPERATION_CALL_STATE e_iocs;
		UNICOM_INSTRUCTION_STEP  e_main_step;
		uint32_t                 ui32_operation_id;
	} s_call_operation;*/

	struct unicom_operation_execution_repeat_context_s
	{
		unicom_repeat_counter_t       s_repeats_ok;
		unicom_repeat_counter_t       s_repeats_error;
	} s_operation_repeat_context;

	struct unicom_instruction_repeat_s
	{
		uint32_t ui32_limit;
		uint32_t ui32_counter;
		uint32_t ui32_wait_at_return_ms;

		uint32_t ui32_flags;
	} s_instruction_return_repeat;

} unicom_execution_context_t, *UNICOM_EXECUTION_CONTEXT;


typedef struct unicom_thread_s
{
	UNICOM_THREAD_STATE e_state;

	const uint32_t *pui32_operations_list;
	uint32_t       ui32_operations_count;

	const uint32_t *pui32_operation_selected; 

	uint32_t       ui32_flags;
	uint32_t       ui32_repeat_counter;
	uint32_t       ui32_repeat_count;

	unicom_execution_event_f   f_event;
	unicom_thread_event_f      f_thread_event;
	unicom_timer_t             s_delay_timer;
	unicom_timer_t             s_instruction_processing_event_timer;

	struct
	{
		unicom_execution_context_t s_context;
	} s_context_interrupt;

	void           *p_app_object;

	uint32_t        ui32_simple_execution_operation;

} unicom_thread_t, *UNICOM_THREAD;


typedef struct 
{
	unicom_event_f             f_event;
	unicom_request_f           f_request;

	const unicom_specific_t**  aps_specifics;
	uint32_t                   ui32_specifics_count;

	unicom_thread_t            *as_thread_pool;
	uint32_t                   ui32_thread_pool_size;

	const char*                pc_identifier;

	unicom_response_key_pass_f
	                           f_response_key_pass;


	uint32_t                   ui32_flags_default;
	uint32_t                   ui32_thread_switching_time_ms;
	uint32_t                   ui32_tick_ms_default;

} unicom_settings_t, *UNICOM_SETTINGS;


typedef struct unicom_s
{
	bool                       b_lock; //!< to avoid hierarchically call
	UNICOM_STATE               e_state;

	UNICOM_SETTINGS            ps_settings;

	const unicom_specific_t**  pps_specific_pointer;
	const unicom_specific_t*   ps_specific;
	const unicom_specific_t*   ps_generic;

	unicom_execution_context_t s_execution_context;

	UNICOM_ERROR_ID            e_error_last;

	uint32_t                   ui32_flags;
	uint32_t                   ui32_flags_internal;

	UNICOM_LEXEM               ps_response_lexem;
	UNICOM_THREAD              ps_last_started_thread;
	UNICOM_THREAD              ps_active_thread;
	const unicom_operation_t  *ps_active_operation;
	const unicom_specific_t   *ps_active_operation_specific;

	void                      *p_app_object;

	unicom_timer_t             s_thread_interrupt_timer;
	unicom_timer_t             s_tick_timer;
	unicom_timer_t             s_instruction_processing_event_timer;

} unicom_t, *UNICOM;

typedef enum
{
	UNICOM_THREAD_FLAGS_NONE                     = 0x0000,
	UNICOM_THREAD_FLAG_STANDALONE                = 0x0002,    //!< stand-alone thread stops only its processing when failure instead of stopping whole unicom processing
	UNICOM_THREAD_FLAG_ERROR_REPEAT              = 0x0004,     //!< repeat thread processing on error
	UNICOM_THREAD_FLAG_INTERRUPTABLE             = 0x0008,     //!< parallel processing with other threads possible
	UNICOM_THREAD_FLAG_ATOMIC                    = 0x0010,     //!< atomic processing without interruption and without default interruption between operations

} UNICOM_THREAD_FLAG;


/*---------------------------------------------------------

BASE FUNCTIONS

---------------------------------------------------------*/
bool unicom_init( UNICOM ps, UNICOM_SETTINGS ps_settings, const unicom_specific_t* ps_generic_base, void *p_app_object );

bool unicom_start( UNICOM ps );

bool unicom_ready( UNICOM ps );
bool unicom_started( UNICOM ps );

void unicom_process( UNICOM ps );
bool unicom_execute( UNICOM ps, uint32_t ui32_thread_id, const uint32_t *pui32_operations, uint32_t ui32_operations_count, uint32_t ui32_delay_ms, uint32_t ui32_repeat_count, unicom_execution_event_f f_event, uint32_t ui32_thread_flags );
bool unicom_execute_2( UNICOM ps, uint32_t ui32_thread_id, const uint32_t *pui32_operations, uint32_t ui32_operations_count, uint32_t ui32_delay_ms, uint32_t ui32_repeat_count, unicom_execution_event_f f_event, void *p_app_object, uint32_t ui32_thread_flags, unicom_thread_event_f f_thread_event );
bool unicom_execute_simple( UNICOM ps, uint32_t ui32_thread_id, uint32_t ui32_operation, unicom_execution_event_f f_event, void *p_app_object, uint32_t ui32_flags );
bool unicom_execute_ready( UNICOM ps, uint32_t ui32_thread_id );
bool unicom_execute_running( UNICOM ps, uint32_t ui32_thread_id );
bool unicom_execute_stop( UNICOM ps, uint32_t ui32_thread_id, bool b_error );
bool unicom_execute_force_start( UNICOM ps, uint32_t ui32_thread_id );
void unicom_response( UNICOM ps, UNICOM_LEXEM ps_lexem );

void unicom_stop( UNICOM ps, bool b_immediatelly );

void unicom_tick_timer_set( UNICOM ps, uint32_t ui32_tick_time_ms );

const char *unicom_version();
const char *unicom_identifier( UNICOM ps );



/*---------------------------------------------------------

UNICOM_LEXEM

---------------------------------------------------------*/
void unicom_lexem_set( UNICOM_LEXEM ps_lexem, uint32_t ui32_id, unsigned char *puc_data, uint32_t ui32_size, uint32_t ui32_position );
uint32_t unicom_lexem_id( UNICOM_LEXEM ps_lexem );
unsigned char* unicom_lexem_content( UNICOM_LEXEM ps_lexem, uint32_t *pui32_size );
uint32_t unicom_lexem_length( UNICOM_LEXEM ps_lexem );
#define unicom_lexem_size unicom_lexem_length

uint32_t unicom_lexem_position( UNICOM_LEXEM ps_lexem );
void unicom_lexem_set_id_predefined( UNICOM_LEXEM ps_lexem, UNICOM_LEXEM_ID e_id );
bool unicom_lexem_check( UNICOM_LEXEM ps_lexem, uint32_t ui32_lexem_id, const unsigned char *puc_data, uint32_t ui32_content_size, uint32_t ui32_position, bool b_partial );
bool unicom_lexem_check_string( UNICOM_LEXEM ps_lexem, uint32_t ui32_lexem_id, const char *pc_data, uint32_t ui32_position, bool b_partial );
bool unicom_lexem_equal_string( UNICOM_LEXEM ps_lexem, const char *pc_data, bool b_partial );
char* unicom_lexem_content_string( UNICOM_LEXEM ps_lexem );
double unicom_lexem_content_double( UNICOM_LEXEM ps_lexem );
uint32_t unicom_lexem_content_uint32( UNICOM_LEXEM ps_lexem );

void *unicom_lexem_object( UNICOM_LEXEM ps_lexem );
UNICOM_LEXEM unicom_lexem_object_set( UNICOM_LEXEM ps_lexem, void *p_object );

UNICOM_LEXEM unicom_lexem_ack_set( UNICOM_LEXEM ps_lexem, void *p_object );



/*---------------------------------------------------------

UNICOM_DATA

---------------------------------------------------------*/
void unicom_data_set( UNICOM_DATA ps, const unsigned char *puc_data, uint32_t ui32_size );
const unsigned char *unicom_data( const unicom_data_t *ps, uint32_t *pui32_size );
void unicom_data_clear( UNICOM_DATA ps );
unicom_data_t unicom_data_string( const char *pc_string );
unicom_data_t unicom_data_pchar( const char *pc_string, const char *pc_string_end );
unicom_data_t unicom_data_block( const unsigned char *puc_block, uint32_t ui32_size );
void* unicom_data_object( const unicom_data_t *ps );
unicom_data_t unicom_data_object_set( void *p_object );



/*---------------------------------------------------------

UNICOM_REQUEST

---------------------------------------------------------*/
void unicom_request_begin( UNICOM ps, const struct unicom_data_s *ps_data );
void unicom_request_body( UNICOM ps, const struct unicom_data_s *ps_data );
void unicom_request_end( UNICOM ps, const struct unicom_data_s *ps_data );



/*---------------------------------------------------------

UNICOM_EXECUTION_CONTEXT

---------------------------------------------------------*/
UNICOM_INSTRUCTION_STEP unicom_instruction_step_get( UNICOM_EXECUTION_CONTEXT ps_context );
void unicom_instruction_wait_set( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_wait_ms );
bool unicom_instruction_problem( UNICOM_EXECUTION_CONTEXT ps_context );
void unicom_instruction_step_set( UNICOM_EXECUTION_CONTEXT ps_context, UNICOM_INSTRUCTION_STEP e_step );
void unicom_instruction_step_next_wait( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_wait_ms  );
void unicom_instruction_step_prev( UNICOM_EXECUTION_CONTEXT ps_context );
void unicom_instruction_step_next( UNICOM_EXECUTION_CONTEXT ps_context );
bool unicom_instruction_step_continue_count( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_count ); //!< returns false if timeout had been already set
bool unicom_instruction_step_continue( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_timeout_ms ); //!< returns false if timeout had been already set
void unicom_instruction_step_continue_stop( UNICOM_EXECUTION_CONTEXT ps_context );
void unicom_instruction_return( UNICOM_EXECUTION_CONTEXT ps_context );
void unicom_instruction_return_repeat( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_repeats_limit, uint32_t ui32_wait_before_return_ms );
void unicom_instruction_repeat_setup( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_repeats_limit,  uint32_t ui32_wait_at_return_ms, uint32_t ui32_instruction_repeat_flags );

void unicom_operation_return( UNICOM_EXECUTION_CONTEXT ps_context );

void unicom_instructions_invoke( UNICOM_EXECUTION_CONTEXT ps_context, const uint8_t *aui8_invoke, uint8_t ui8_invoke_items_count );
void unicom_instructions_invoke_simple( UNICOM_EXECUTION_CONTEXT ps_context, uint8_t ui8_instruction_id );
void unicom_instruction_timeout_reset( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_timeout_ms ); //!< is changed only during actual run, otherwise default value is used ( from instruction definition )
void unicom_instruction_timeout_restart( UNICOM_EXECUTION_CONTEXT ps_context );
void unicom_instruction_timeout_add( UNICOM_EXECUTION_CONTEXT ps_context, uint32_t ui32_timeout_ms );
void unicom_instruction_timeout_stop( UNICOM_EXECUTION_CONTEXT ps_context );

void unicom_flag_change( UNICOM_EXECUTION_CONTEXT ps_context, UNICOM_FLAG e_flag , bool b_value );
void unicom_instruction_flag_set( UNICOM_EXECUTION_CONTEXT ps_context, UNICOM_FLAG e_flag );
void unicom_instruction_stop( UNICOM_EXECUTION_CONTEXT ps_context, bool b_error );

void unicom_instruction_request( UNICOM_EXECUTION_CONTEXT ps_context, const struct unicom_data_s s_data, bool b_use_defaults, bool b_no_ack );
void unicom_instruction_request_simple( UNICOM_EXECUTION_CONTEXT ps_context, const struct unicom_data_s s_data );

void* unicom_instruction_response_key( UNICOM_EXECUTION_CONTEXT ps );
void unicom_instruction_response_key_set( UNICOM_EXECUTION_CONTEXT ps, void *p_response_key );
void unicom_instruction_response_key_clear( UNICOM_EXECUTION_CONTEXT ps );

void* unicom_instruction_object( UNICOM_EXECUTION_CONTEXT ps );
void unicom_instruction_object_set( UNICOM_EXECUTION_CONTEXT ps, void *p_object );
void unicom_instruction_object_clear( UNICOM_EXECUTION_CONTEXT ps );



/*---------------------------------------------------------

UNICOM_TIMER

---------------------------------------------------------*/
void unicom_timer_init( UNICOM_TIMER ps_timer, uint32_t ui32_timeout );
void unicom_timer_init_s( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_s );
void unicom_timer_stop( UNICOM_TIMER ps_timer );
void unicom_timer_start( UNICOM_TIMER ps_timer );
void unicom_timer_add( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_ms );
void unicom_timer_sub( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_ms );
bool unicom_timer_running( UNICOM_TIMER ps_timer );
bool unicom_timer_expired( UNICOM_TIMER ps_timer );
bool unicom_timer_expire( UNICOM_TIMER ps_timer );
void unicom_timer_restart( UNICOM_TIMER ps_timer );
void unicom_timer_reset( UNICOM_TIMER ps_timer, uint32_t ui32_timeout_ms );
uint32_t unicom_timer_time_to_expire_ms( UNICOM_TIMER ps_timer );
uint32_t unicom_timer_timeout( UNICOM_TIMER ps_timer );



/*---------------------------------------------------------

UNICOM_EVENT

---------------------------------------------------------*/
bool unicom_event_instruction_step( UNICOM_EVENT_DATA ps_data, uint32_t *pui_step );
bool unicom_event_instruction( UNICOM_EVENT_DATA ps_data, uint32_t *pui_id );
bool unicom_event_operation( UNICOM_EVENT_DATA ps_data, uint32_t *pui_id );
bool unicom_event_thread( UNICOM_EVENT_DATA ps_data, uint32_t *pui_id );
bool unicom_event_identifier( UNICOM_EVENT_DATA ps_data, const char **ppc_id );
bool unicom_event_specific_name( UNICOM_EVENT_DATA ps_data, const char **ppc_id );
UNICOM_LEXEM unicom_event_lexem( UNICOM_EVENT_DATA ps_data );
void *unicom_event_request_object( UNICOM_EVENT_DATA ps_data );


#endif

