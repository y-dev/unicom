#define UNICOM_ERROR_GOTO( e ) do { unicom_error_set( e ); MDG_ERROR(); } while ( 0 )

#define UNICOM_TIMER_TIMEOUT_NONE ((uint32_t)-1)
#define UNICOM_TIMER_TIMEOUT_IMMEDIATELLY ((uint32_t) 0 )

#define UNICOM_INSTRUCTION_ITERATOR_NEXT_STOP ((uint8_t) -1 )
#define UNICOM_INSTRUCTION_ITERATOR_INIT ((uint8_t)-2 )


typedef enum
{
	UNICOM_EVENT_FLAG_INSTRUCTION       = 0x001,
	UNICOM_EVENT_FLAG_OPERATION         = 0x002,
	UNICOM_EVENT_FLAG_THREAD            = 0x004,
	UNICOM_EVENT_FLAG_SPECIFIC          = 0x008,
	UNICOM_EVENT_FLAG_LEXEM             = 0x010,
	UNICOM_EVENT_FLAG_IDENTIFIER        = 0x020,
	UNICOM_EVENT_FLAG_INSTRUCTION_STEP  = 0x040,
	UNICOM_EVENT_FLAG_RESPONSE_OBJECT   = 0x080,
	UNICOM_EVENT_FLAGS_NONE             = 0x000,
} unicom_event_flag_e, UNICOM_EVENT_FLAG;

typedef enum
{
	UNICOM_INSTRUCTION_STEP_RESULT_NONE,
	UNICOM_INSTRUCTION_STEP_RESULT_OK,
	UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_DOWN,
	UNICOM_INSTRUCTION_STEP_RESULT_CHANGE_UP,
	UNICOM_INSTRUCTION_STEP_RESULT_ERROR,
	UNICOM_INSTRUCTION_STEP_RESULT_UNKNOWN, // unicom state changed externally!
} unicom_instruction_step_result_e, UNICOM_INSTRUCTION_STEP_RESULT;

typedef struct
{
	UNICOM_THREAD              ps_active_thread;
	const unicom_operation_t  *ps_active_operation;
	const unicom_specific_t   *ps_active_operation_specific;
	
} unicom_activity_t, *UNICOM_ACTIVITY;

struct unicom_data_s;


bool unicom_event( UNICOM ps, UNICOM_EVENT e );
void unicom_thread_init( UNICOM ps, UNICOM_THREAD ps_thread );
//void unicom_thread_done( UNICOM_THREAD ps_thread );
bool unicom_thread_start( UNICOM_THREAD ps_thread, const uint32_t *pui32_operations_list, uint32_t ui32_operations_count,  uint32_t ui32_delay_ms, uint32_t ui32_repeat_count, unicom_execution_event_f f_event, void *p_app_object, uint32_t ui32_flags, unicom_thread_event_f f_thread_event );

UNICOM_THREAD_STATE unicom_thread_state( UNICOM_THREAD ps_thread );
bool unicom_thread_operation_selected( UNICOM_THREAD ps_thread, uint32_t *pe );
bool unicom_thread_operation_next( UNICOM_THREAD ps_thread );
bool unicom_thread_runnable( UNICOM_THREAD ps_thread, bool b_forced );

bool unicom_stop_thread( UNICOM ps, UNICOM_THREAD ps_thread, bool b_error );


void unicom_thread_restart( UNICOM_THREAD ps_thread, bool b_start );

UNICOM_ERROR_ID unicom_error_get( );

void unicom_error_clear( );
void unicom_error_set( UNICOM_ERROR_ID e );


UNICOM_INSTRUCTION_STATE unicom_instruction_state_get( UNICOM_EXECUTION_CONTEXT ps_context );

bool unicom_flag_internal_get( UNICOM ps, UNICOM_FLAG_INTERNAL e_flag );

void unicom_flag_internal_set( UNICOM ps, UNICOM_FLAG_INTERNAL e_flag );

void unicom_flag_internal_clear( UNICOM ps, UNICOM_FLAG_INTERNAL e_flag );

void unicom_run( UNICOM ps );

UNICOM_INSTRUCTION_STEP_RESULT unicom_instruction_step( UNICOM ps, struct unicom_lexem_s *ps_lexem );

bool unicom_flag_get( UNICOM ps, UNICOM_FLAG e_flag );
void unicom_flag_set( UNICOM ps, UNICOM_FLAG e_flag );

bool unicom_context_flag_get( UNICOM_EXECUTION_CONTEXT ps, UNICOM_EXECUTION_CONTEXT_FLAG e_flag );
void unicom_context_flag_set( UNICOM_EXECUTION_CONTEXT ps, UNICOM_EXECUTION_CONTEXT_FLAG e_flag );
void unicom_context_flag_clear( UNICOM_EXECUTION_CONTEXT ps, UNICOM_FLAG_INTERNAL e_flag );

uint32_t unicom_data_size( const struct unicom_data_s *ps );
bool unicom_data_present( const struct unicom_data_s *ps );

bool unicom_thread_flag_get( UNICOM_THREAD ps_thread, UNICOM_THREAD_FLAG e_flag );

UNICOM_STATE unicom_state( UNICOM ps );
UNICOM_LEXEM unicom_response_top( UNICOM ps );

UNICOM_PROCESSING_RESULT unicom_instruction_done( UNICOM ps, bool b_error );

void unicom_response_pop( UNICOM ps, bool b_explicit ); //!< explicit gives user callback to control lexem reprocessing when steps changing
void unicom_flag_clear( UNICOM ps, UNICOM_FLAG e_flag );

bool unicom_specific_event( UNICOM ps, const unicom_specific_t *ps_specific, UNICOM_SPECIFIC_EVENT e_event );

void unicom_operation_stop( UNICOM ps, bool b_error, UNICOM_THREAD ps_thread );
UNICOM_PROCESSING_RESULT unicom_operation_run( UNICOM ps );
UNICOM_THREAD unicom_thread_get( UNICOM ps, uint32_t ui32_thread_id );

void unicom_response_object_clear( UNICOM ps );
void unicom_response_object_set( UNICOM ps, void *p_object );

void unicom_instruction_request_object_set( UNICOM_EXECUTION_CONTEXT ps, void *p_object );
void unicom_instruction_request_object_clear( UNICOM_EXECUTION_CONTEXT ps );
void *unicom_instruction_request_object( UNICOM_EXECUTION_CONTEXT ps );

void unicom_instruction_response_object_set( UNICOM_EXECUTION_CONTEXT ps, void *p_object );
void unicom_instruction_response_object_clear( UNICOM_EXECUTION_CONTEXT ps );
void *unicom_instruction_response_object( UNICOM_EXECUTION_CONTEXT ps );




uint32_t unicom_thread_id( UNICOM ps, UNICOM_THREAD ps_thread );

void unicom_instruction_response_key_set( UNICOM_EXECUTION_CONTEXT ps, void *p_key );

bool unicom_thread_event( UNICOM ps, UNICOM_THREAD ps_thread, UNICOM_THREAD_EVENT e_event );

bool unicom_interrupt( UNICOM ps, bool b_forced_interrupt, bool b_thread_last_operation );
bool unicom_thread_resume( UNICOM ps, UNICOM_THREAD ps_thread );
bool unicom_thread_interrupt( UNICOM ps, UNICOM_THREAD ps_thread );
bool unicom_thread_interruptable( UNICOM_THREAD ps );

void unicom_thread_interrupt_timer_start( UNICOM ps );

UNICOM_THREAD unicom_thread_previous( UNICOM ps, UNICOM_THREAD ps_thread );

void unicom_activity_store( UNICOM ps, UNICOM_ACTIVITY ps_activity );
void unicom_activity_restore( UNICOM ps, UNICOM_ACTIVITY ps_activity );
void unicom_activity_clear( UNICOM ps );

bool unicom_state_check_set( UNICOM ps, UNICOM_STATE e_check_state, UNICOM_STATE e_state );
void unicom_instruction_process_event( UNICOM ps, bool b_forced );

void unicom_repeat_init( UNICOM_REPEAT_COUNTER ps_repeat, uint32_t ui32_limit );
void unicom_repeat_reset( UNICOM_REPEAT_COUNTER ps_repeat );

bool unicom_thread_operation_selection( UNICOM ps, UNICOM_THREAD ps_thread, bool b_operation_repeat );

void unicom_instruction_return_repeat_clean( struct unicom_execution_context_s *ps_context  );

