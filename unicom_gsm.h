#ifndef __UNICOM_GSM_H__
#define __UNICOM_GSM_H__

/**
 * @brief UNICOM lexical analyser usable for GSM AT communication.
 * 
 * @remarks
 * GSM AT communication analyser.
 * Directly usable with unicom communication processor.
 * Analyses GSM AT communication and splits them into know lexical units.
 * Those lexems can be further passed directly into unicom processor for processing of
 *
 * For more details look into UNICOM documentation. (unicom.odt)
 * 
 * @author Marek Plhak 2015
 **/


#define UNICOM_GSM_UNGET_CHARACTERS_MAX 8

#include <stdint.h>
#include <stdbool.h>
#include <unicom.h>

typedef enum
{
	UNICOM_GSM_LEXEM_STRING     = 0,
	UNICOM_GSM_LEXEM_NUMBER     = 1,
	UNICOM_GSM_LEXEM_TEXT       = 2,  // [a-zA-Z0-9:+]\+
	UNICOM_GSM_LEXEM_NEW_LINE   = 3,  // new line
	UNICOM_GSM_LEXEM_ERROR      = 4, // lexem read error ( is different from returned error! )
	UNICOM_GSM_LEXEM_NEXT_POSITION = 5,
	UNICOM_GSM_LEXEM_DATA       = 6,
	UNICOM_GSM_LEXEM_OK         = UNICOM_LEXEM_OK,
} unicom_gsm_lexem_id_e, UNICOM_GSM_LEXEM_ID;

typedef enum
{
	UNICOM_GSM_INIT,
	UNICOM_GSM_STRING,
	UNICOM_GSM_NUMBER,
	UNICOM_GSM_TEXT,
	UNICOM_GSM_LFCR,
	UNICOM_GSM_CRLF,
	UNICOM_GSM_TOO_LONG_WAIT_FOR_EOL,
} unicom_gsm_state_e, UNICOM_GSM_STATE;

typedef struct
{
	UNICOM_GSM_STATE e_state;
	unicom_lexem_t   s_lexem;     //!< for standard analyse
	unicom_lexem_t   s_tmp_lexem; //!< for fast response without content ( ACK )

	unsigned char *auc_lexem_storage;
	uint32_t ui32_lexem_storage_size;
	uint32_t ui32_lexem_size;
	uint32_t ui32_lexem_line_index;
	uint32_t ui32_unget_count;
	char     ac8_unget[ UNICOM_GSM_UNGET_CHARACTERS_MAX ];
	char     c_string_quote;
	unsigned char auc16_echo_storage[16];
	uint32_t ui32_echo_size;

	bool b_lexem_found;
	bool b_echo_on;
	bool b_echo_check_active;
	bool b_echo_check_wait_for_next_line;
	bool b_echo_ack_finalized;
	bool b_echo_produce_lexem;
	uint32_t ui32_echo_check_index;

	bool     b_data_mode;
	uint32_t ui32_data_mode_size;
	uint8_t  *pui8_data;
	uint32_t ui32_data_size;
	
	unicom_timer_t s_long_line_timeout;

} unicom_gsm_t, *UNICOM_GSM;


void unicom_gsm_init( UNICOM_GSM ps, unsigned char *ac_storage, uint32_t ui32_storage_size );

void unicom_gsm_restart( UNICOM_GSM ps );

UNICOM_LEXEM unicom_gsm_process( UNICOM_GSM ps_analyser, const unsigned char **ppuc_data, uint32_t *pui32_data_size );

void unicom_gsm_analyse( UNICOM_GSM ps_gsm, UNICOM ps, const unsigned char *puc_data, uint32_t ui32_data_size );

bool unicom_gsm_echo( UNICOM_GSM ps_gsm );
void unicom_gsm_echo_on( UNICOM_GSM ps_gsm );
void unicom_gsm_echo_off( UNICOM_GSM ps_gsm );
void unicom_gsm_echo_required( UNICOM_GSM ps_gsm );
void unicom_gsm_echo_add( UNICOM_GSM ps_gsm, char *pc );

bool unicom_gsm_lexem_check( UNICOM_LEXEM ps_lexem, UNICOM_GSM_LEXEM_ID e_id, const char *pc_data, uint32_t ui32_index, bool b_partial );
bool unicom_gsm_lexem_compare( UNICOM_LEXEM ps_lexem, const char *pc_data, bool b_partial );

bool unicom_gsm_lexem_equal( UNICOM_LEXEM ps_lexem, const char *pc_data, bool b_partial );

char* unicom_gsm_lexem_content( UNICOM_LEXEM ps_lexem );

bool unicom_gsm_data_mode( UNICOM_GSM ps_gsm );
void unicom_gsm_set_data_mode( UNICOM_GSM ps_gsm, uint8_t *pui8, uint32_t ui32_size );
void unicom_gsm_set_normal_mode( UNICOM_GSM ps_gsm );

#endif

