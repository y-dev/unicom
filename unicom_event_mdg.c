#include "unicom_event_mdg.h"
#include <mdg.h>
#include <stdio.h>

#include <string.h>

#define PRINTOUT( format, ... ) do { snprintf( pc_dst, pc_dst_end - pc_dst, format, __VA_ARGS__ ); pc_dst = pc_dst + strlen( pc_dst ); /* move pointer */ } while ( 0 )

//const char *gac_unicom_event_mdg_filter = "GsmAt";

void unicom_event_mdg_debug_event( void* p, UNICOM_EVENT e_event_id, UNICOM_EVENT_DATA ps_data )
{
	char ac_tmp[ 128 ];

	char *pc_dst = ac_tmp;
	const char *pc_dst_end = pc_dst + sizeof( ac_tmp );
	const char *pc_specific_name = NULL;
	const char *pc_id = NULL;
	uint32_t ui32_id;
	UNICOM_LEXEM ps_lexem = NULL;
	MDG_LEVEL e_level = MDG_LEVEL_INFO;

	bool b_id = false;
	bool b_no_printout = false;
	//GSMAT_DEBUG( "event #%03i processing enter", e_event_id );


	switch ( e_event_id )
	{
		case UNICOM_EVENT_REQUEST_BODY:
		case UNICOM_EVENT_REQUEST_END:
		case UNICOM_EVENT_REQUEST_BEGIN:
			// prepare analyser echo mechanism
			b_no_printout = true;
			break;

		case UNICOM_EVENT_INSTRUCTION_STOP:
		case UNICOM_EVENT_THREAD_EXECUTE_BUSY:
		case UNICOM_EVENT_INSTRUCTION_STOP_ERROR:
		case UNICOM_EVENT_INSTRUCTION_ERROR:
		case UNICOM_EVENT_OPERATION_ENTER_ERROR:
		case UNICOM_EVENT_OPERATION_ERROR:
		case UNICOM_EVENT_STOPPED:
		case UNICOM_EVENT_INSTRUCTION_TIMEOUT:
			e_level = MDG_LEVEL_WARNING;
			break;

		case UNICOM_EVENT_THREAD_SELECTION:
		case UNICOM_EVENT_THREAD_SELECTED:
		case UNICOM_EVENT_INTERRUPT:
		case UNICOM_EVENT_RESUME:
			e_level = MDG_LEVEL_DEBUG;
			break;

		case UNICOM_EVENT_ERROR:
			e_level = MDG_LEVEL_ERROR;
			break;

		default:
			break;
	}

	// no processing when filtered
	if ( b_no_printout || !MDG_TRACE_CHECK( e_level ) )
		return;


	if ( unicom_event_identifier( ps_data, &pc_id ) )
		b_id = true;
	
	PRINTOUT( "event | #%03i", e_event_id );	

	if ( b_id )
		PRINTOUT( " | %.10s", pc_id );

	// some additional outputs
	switch ( e_event_id )
	{
		case UNICOM_EVENT_INSTRUCTION_STOP:
			PRINTOUT( "%s", " | stop" );
			break;

		case UNICOM_EVENT_THREAD_EXECUTE_BUSY:
		case UNICOM_EVENT_INSTRUCTION_STOP_ERROR:
		case UNICOM_EVENT_INSTRUCTION_ERROR:
		case UNICOM_EVENT_OPERATION_ENTER_ERROR:
		case UNICOM_EVENT_OPERATION_ERROR:
			PRINTOUT( "%s", " | processing error" );
			break;

		case UNICOM_EVENT_STARTED:
			PRINTOUT( " | unicom v.%s", unicom_version() );
			break;

		case UNICOM_EVENT_RESPONSE_LEXEM:
			ps_lexem = ps_data->ps_lexem;
			break;

		case UNICOM_EVENT_ERROR:
			PRINTOUT( "%s"," | failure");
			break;

		default:
			break;
	}

	if ( unicom_event_specific_name( ps_data, &pc_specific_name ) )
		PRINTOUT( " | %.10s", pc_specific_name );

	if ( unicom_event_thread( ps_data, &ui32_id ) )
		PRINTOUT( " | T#%02i ", ui32_id );

	if ( unicom_event_operation( ps_data, &ui32_id ) )
		PRINTOUT( " | OP#%03i ", ui32_id );

	if ( unicom_event_instruction( ps_data, &ui32_id ) )
		PRINTOUT( " | I#%03i ", ui32_id );
	
	if ( unicom_event_instruction_step( ps_data, &ui32_id ) )
		PRINTOUT( " | S#%03i ", ui32_id );

	if ( ps_lexem != NULL ) 
	{
		if ( unicom_lexem_size( ps_data->ps_lexem ) > 0 )
		{
			if ( unicom_lexem_id( ps_data->ps_lexem ) != UNICOM_LEXEM_ACK )
				PRINTOUT( " | RSP#%02i/%i (%i)", unicom_lexem_id( ps_lexem ), unicom_lexem_position( ps_lexem ), unicom_lexem_size( ps_lexem ) );
			else
				PRINTOUT( " | RSP#%02i/%i ACK", unicom_lexem_id( ps_lexem ), unicom_lexem_position( ps_lexem ) );
		}

	}
	  
	MDG_TRACE( e_level, "%s", ac_tmp );
}

unicom_event_f unicom_event_mdg()
{
	MDG_MODULE_REGISTER( "unicom_event_mdg", MDG_TRACING_ALL );
	return unicom_event_mdg_debug_event;

}

