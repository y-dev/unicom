#include <stdlib.h>
#include <unicom_gsm.h>
#include <stdio.h>

unicom_gsm_t s_gsm;
#define CRLF "\r\n"
#define LFCR "\n\r"

#define CR "\r"
#define LF "\n"
char pc_analyse_data[] = 
"AT+CGML = 1,2,3,4,5,6"
CRLF
"+CGML: 1,2,3,4,5,6,7,8,\"text\""
CRLF
"+URC: 1,2,3,4,5"
CR
CR
"NO CARRIER"
LF
LF
"BUSY"
LF
"THIS WILL BE NOT PRESENTED AS LEXEM BECAUSE THERE IS NO CHARACTER"
;

int main()
{
	unsigned char auc128_storage[ 128 ];
	unsigned char *puc = (unsigned char*)pc_analyse_data;
	uint32_t ui32 = sizeof( pc_analyse_data ) - 1;

	unicom_gsm_init( &s_gsm, auc128_storage, sizeof( auc128_storage ) );
	unicom_gsm_echo_on( &s_gsm );
	unicom_gsm_echo_add( &s_gsm, "AT+CGML=1,2,3,4,5" );

	while ( ui32 > 0 )
	{
		UNICOM_LEXEM ps_lexem;
		ps_lexem = unicom_gsm_process( &s_gsm, (const unsigned char**)&puc, &ui32 );
		if ( ps_lexem != NULL )
		{
			printf( "lexem type #%i with content '%s' at line position %i\n", unicom_lexem_id( ps_lexem ), unicom_gsm_lexem_content( ps_lexem ), unicom_lexem_position( ps_lexem ) );
		}
	}
	return 0;
}
